#ifndef _RAWRMAP_HPP
#define _RAWRMAP_HPP

#include "ChaloTile.hpp"
#include "Enums.hpp"
#include "Object.hpp"

#include <string>

class RawrMap
{
public:
    RawrMap();

    void Setup( std::string terrainTileset, std::string itemTileset, std::string playerTileset );
    void Clear();

    void Draw( sf::RenderTexture& window );
    void Save( const std::string& filename );
    void Load( const std::string& filename, bool mapEditor = false );
    bool HandleInput();
    void UpdateTurn();

    sf::Vector2f GetPlayerStartingPoint();
    bool CanWalkOnTile( sf::Vector2i tileIndices );
    bool CanPushTile( sf::Vector2i tileIndices );
    CollisionBehavior GetCollisionBehavior( sf::Vector2i tileIndices );
    sf::Vector2i GetTileCoordinates( sf::Vector2f position );
    sf::Vector2f GetPositionFromIndices( sf::Vector2i tileIndices );
    bool PushObject( sf::Vector2i pusherPosition, sf::Vector2i objectPosition );

    void Editor_Click( const sf::Vector2f& mousePos, BrushType brushType, ChaloTile& brush );

private:
    ChaloTile m_floor[20][12];
    ChaloTile m_terrainArray[20][12];
    ChaloTile m_itemArray[20][12];
    std::string m_name;

    // TODO: Move player into this class
    sf::Vector2f m_playerStartingPoint;
    Object              m_player;
    std::vector<Object> m_npcs;
    std::vector<Object> m_objects;

    const int MAP_WIDTH;
    const int MAP_HEIGHT;
    const int TILE_WIDTH;
    const int TILE_HEIGHT;

    Object& GetObjectAtArrayIndices( sf::Vector2i tileIndices );

    static std::string s_className;
};

#endif
