#ifndef _MAP_EDITOR_STATE
#define _MAP_EDITOR_STATE

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/UI/UIImage.hpp"
#include "../chalo-engine/UI/UIButton.hpp"
#include "../chalo-engine/UI/UILabel.hpp"

#include "../Objects/Tileset.hpp"
#include "../Objects/RawrMap.hpp"
#include "../Objects/Enums.hpp"

#include <vector>

enum class ViewState { NO_MENU, TILE_MENU };

class MapEditorState : public chalo::IState
{
public:
    MapEditorState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderTexture& window );

private:
    static std::string s_className;

    RawrMap m_map;

    ChaloTile m_brush;
    BrushType m_brushType;

    ViewState m_viewState;

    const int MAP_WIDTH;
    const int MAP_HEIGHT;
    const int TILE_WIDTH;
    const int TILE_HEIGHT;
    const int TILESET_WIDTH;
    const int TILESET_HEIGHT;

    sf::RectangleShape m_mouseCursor;
    sf::RectangleShape m_menuBackground;
    chalo::UILabel m_status;

    Tileset m_terrainTileset;
    Tileset m_itemTileset;

    std::string m_backupFilePath;
    std::string m_mainFilePath;

    void Initialize_Map();
    void Initialize_TileMenu();

    void Draw_Map( sf::RenderTexture& window );
    void Draw_TileMenu( sf::RenderTexture& window );

    void UpdateCursor( const sf::Vector2f& mousePos );
    void HandleClick_TileMenu( const sf::Vector2f& mousePos );
    void HandleClick_Map( const sf::Vector2f& mousePos );

    void btnOpenTileMenu_Clicked();
    void btnTestLevel_Clicked();
    void btnSaveLevel_Clicked();
    void btnLoadLevel_Clicked();
    void btnClearLevel_Clicked();

    void StatusUpdate( const std::string& status );
};

#endif
