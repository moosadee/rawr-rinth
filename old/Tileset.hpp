#ifndef _TILESET_HPP
#define _TILESET_HPP

#include <SFML/Graphics.hpp>

#include <string>
#include <vector>

#include "Tile.hpp"

class Tileset
{
public:
    Tileset();
    void LoadTileset( std::string tilesetSpecFile, std::string tilesetName, sf::Vector2f setDimensions, sf::Vector2f positionOffset );
    void Draw( sf::RenderTexture& window );
    bool TileClicked( const sf::Vector2f& mousePos, Tile& tileInfo );

private:
    std::vector<Tile> m_tileset;
    const int TILE_WIDTH;
    const int TILE_HEIGHT;
    sf::Vector2f m_setDimensions;
    sf::Vector2f m_positionOffset;

    static std::string s_className;
};

#endif
