#include "MapEditorState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
//#include "../chalo-engine/Managers/ConfigManager.hpp"
//#include "../chalo-engine/Managers/LanguageManager.hpp"
//#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"
#include "../chalo-engine/Utilities/CsvParser.hpp"
#include "../chalo-engine/Utilities_SFML/SFMLHelper.hpp"

#include <fstream>
#include <string>
#include <iomanip>

std::string MapEditorState::s_className = "MapEditorState";

MapEditorState::MapEditorState()
    : MAP_WIDTH( 20 ), MAP_HEIGHT( 12 ),
    TILE_WIDTH( 20 ), TILE_HEIGHT( 20 ),
    TILESET_WIDTH( 10 ), TILESET_HEIGHT( 6 )
{
}

void MapEditorState::Init( const std::string& name )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Parameters - name: " + name, s_className + "::" + __func__ );
    Logger::StackPop();
}

void MapEditorState::Setup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    IState::Setup();
    chalo::InputManager::Setup();

    // Load needed assets
    chalo::TextureManager::AddAndGet( "sheet-items", "Content/Graphics/Tilesets/sheet-items.png" );
    chalo::TextureManager::AddAndGet( "sheet-terrain", "Content/Graphics/Tilesets/sheet-terrain.png" );
    chalo::TextureManager::AddAndGet( "map-editor-buttons", "Content/Graphics/UI/map-editor-buttons.png" );
    chalo::TextureManager::AddAndGet( "menu-bar", "Content/Graphics/UI/menu-bar.png" );
    chalo::TextureManager::AddAndGet( "menu-bar-h", "Content/Graphics/UI/menu-bar-h.png" );
    chalo::TextureManager::AddAndGet( "button_bg", "Content/Graphics/UI/button_bg.png" );
    chalo::TextureManager::AddAndGet( "button_bg_20", "Content/Graphics/UI/button_bg_20.png" );

    m_mouseCursor.setFillColor( sf::Color( 0, 255, 0, 100 ) );
    m_mouseCursor.setSize( sf::Vector2f( 20, 20 ) );

    m_menuBackground.setFillColor( sf::Color( 100, 100, 100 ) );
    m_menuBackground.setSize( sf::Vector2f( chalo::Application::GetIntendedScreenDimensions().x / 2, chalo::Application::GetIntendedScreenDimensions().y ) );
    m_menuBackground.setPosition( 0, 0 );

    // Load the tileset
    // std::string tilesetSpecFile, std::string tilesetImageFile, int setWidth, int setHeight
    m_terrainTileset.LoadTileset( "Content/Graphics/Tilesets/sheet-terrain.csv", "sheet-terrain", sf::Vector2f( 200, 120 ), sf::Vector2f( 0, 0 ) );
    m_itemTileset.LoadTileset( "Content/Graphics/Tilesets/sheet-items.csv", "sheet-items", sf::Vector2f( 200, 120 ), sf::Vector2f( 0, 120 ) );

    // Load menu file
    chalo::MenuManager::LoadTextMenu( "mapeditor.chalomenu" );

    Initialize_Map();
    Initialize_TileMenu();

    m_backupFilePath = "backup_map.csv";
    m_mainFilePath = "saved_map.csv";

    m_map.Load( m_backupFilePath, true );

    Logger::StackPop();
}

void MapEditorState::Cleanup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    chalo::MenuManager::Cleanup();
    Logger::StackPop();
}

void MapEditorState::Update()
{
    chalo::MenuManager::Update();
    std::string clickedButton = chalo::MenuManager::GetClickedButton();

//if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))

    bool clickingButton = false;
    // Clicking on the open tile menu
    if ( clickedButton == "btnOpenTileMenu" || sf::Keyboard::isKeyPressed( sf::Keyboard::Space ) )
    {
        Logger::Out( "btnOpenTileMenu clicked", s_className + "::" + __func__ );
        clickingButton = true;
        btnOpenTileMenu_Clicked();
    }
    else if ( clickedButton == "btnTestLevel" || sf::Keyboard::isKeyPressed( sf::Keyboard::T ) )
    {
        Logger::Out( "btnTestLevel clicked", s_className + "::" + __func__ );
        clickingButton = true;
        btnTestLevel_Clicked();
    }
    else if ( clickedButton == "btnSaveLevel" || sf::Keyboard::isKeyPressed( sf::Keyboard::S ) )
    {
        Logger::Out( "btnSaveLevel clicked", s_className + "::" + __func__ );
        clickingButton = true;
        btnSaveLevel_Clicked();
    }
    else if ( clickedButton == "btnLoadLevel" || sf::Keyboard::isKeyPressed( sf::Keyboard::L ) )
    {
        Logger::Out( "btnLoadLevel clicked", s_className + "::" + __func__ );
        clickingButton = true;
        btnLoadLevel_Clicked();
    }
    else if ( clickedButton == "btnClearLevel" )
    {
        Logger::Out( "btnClearLevel clicked", s_className + "::" + __func__ );
        clickingButton = true;
        btnClearLevel_Clicked();
    }

    chalo::InputManager::Update();
    sf::Vector2f mousePos = chalo::InputManager::GetMousePosition();

    if ( mousePos.x >= chalo::Application::GetIntendedScreenDimensions().x / 2 )
    {
        m_viewState = ViewState::NO_MENU;
    }

    UpdateCursor( mousePos );

    // Open menu if mouse clicks on the left side
    if ( clickingButton == false && chalo::InputManager::IsLeftClick() )
    {
        // Clicking in the tile menu somewhere
        if ( m_viewState == ViewState::TILE_MENU )
        {
            HandleClick_TileMenu( mousePos );
        }
        else
        {
            HandleClick_Map( mousePos );
        }
    }
}

void MapEditorState::UpdateCursor( const sf::Vector2f& mousePos )
{
    if ( mousePos.x >= 20 && mousePos.x <= 380 && mousePos.y >= 20 && mousePos.y <= 220 )
    {
        // Highlight a specific tile
        int nearX = mousePos.x / TILE_WIDTH;
        int nearY = mousePos.y / TILE_HEIGHT;
        m_mouseCursor.setPosition( nearX * TILE_WIDTH, nearY * TILE_HEIGHT );
    //    m_mouseCursor.setPosition( mousePos );
        m_brush.SetPosition( sf::Vector2f( nearX * TILE_WIDTH, nearY * TILE_HEIGHT ) );
    }
}

void MapEditorState::Draw( sf::RenderTexture& window )
{
    sf::RectangleShape shape;
    shape.setFillColor( sf::Color::Yellow );
    shape.setPosition( 0, 0 );
    shape.setSize( sf::Vector2f( 400, 240 ) );
    window.draw( shape );
    Draw_Map( window );

    chalo::MenuManager::Draw( window );

    window.draw( m_mouseCursor );
    Draw_TileMenu( window );
    window.draw( m_status.GetText() );
    window.draw( m_brush.GetSprite() );
}

void MapEditorState::Draw_Map( sf::RenderTexture& window )
{
    m_map.Draw( window );
}

void MapEditorState::Draw_TileMenu( sf::RenderTexture& window )
{
    if ( m_viewState == ViewState::TILE_MENU )
    {
        window.draw( m_menuBackground );
        m_terrainTileset.Draw( window );
        m_itemTileset.Draw( window );
    }
}

void MapEditorState::Initialize_Map()
{
    Logger::StackPush( s_className + "::" + __func__ );

    m_map.Setup( "sheet-terrain", "sheet-items", "sheet-rawr" );
    m_map.Clear();

    Logger::StackPop();
}

void MapEditorState::Initialize_TileMenu()
{
    Logger::StackPush( s_className + "::" + __func__ );

    // Status text
    m_status.Setup( "txtStatus", "main", 10, sf::Color::Yellow, sf::Vector2f( 210, 220 ), "Hello!" );

    // Pull out menu
    m_viewState = ViewState::NO_MENU;
    sf::Vector2f texturePos( 0, 0 );
    sf::Vector2f itemPos( 0, 140 );

    Logger::StackPop();
}

void MapEditorState::HandleClick_TileMenu( const sf::Vector2f& mousePos )
{
    Logger::StackPush( s_className + "::" + __func__ );

    ChaloTile tileInfo;
    bool result = false;

    if ( m_terrainTileset.TileClicked( mousePos, tileInfo ) )
    {
        m_brushType = BrushType::TERRAIN;
        result = true;
    }

    if ( m_itemTileset.TileClicked( mousePos, tileInfo ) )
    {
        m_brushType = BrushType::ITEM;
        result = true;
    }

    if ( result )
    {
        // Setup brush
        m_brush = tileInfo;
        StatusUpdate( m_brush.GetName() );
    }

    Logger::StackPop();
}

void MapEditorState::HandleClick_Map( const sf::Vector2f& mousePos )
{
    Logger::StackPush( s_className + "::" + __func__ );

    m_map.Editor_Click( mousePos, m_brushType, m_brush );
    m_map.Save( m_backupFilePath );

    Logger::StackPop();
}

void MapEditorState::btnOpenTileMenu_Clicked()
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "btnOpenTileMenu_Clicked", s_className + "::" + __func__ );
    m_viewState = ViewState::TILE_MENU;
    Logger::StackPop();
}

void MapEditorState::btnTestLevel_Clicked()
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "btnTestLevel_Clicked", s_className + "::" + __func__ );
    SetGotoState( "gameState" );
    Logger::StackPop();
}

void MapEditorState::btnSaveLevel_Clicked()
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Map filename: \"" + m_mainFilePath + "\"", s_className + "::" + __func__ );
    m_map.Save( m_mainFilePath );
    StatusUpdate( "Save " + m_mainFilePath );

    Logger::StackPop();
}

void MapEditorState::btnLoadLevel_Clicked()
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Map filename: \"" + m_mainFilePath + "\"", s_className + "::" + __func__ );
    m_map.Load( m_mainFilePath, true );
    StatusUpdate( "Load " + m_mainFilePath );

    Logger::StackPop();
}

void MapEditorState::btnClearLevel_Clicked()
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "btnLoadLevel_Clicked", s_className + "::" + __func__ );

    Initialize_Map();

    Logger::StackPop();
}

void MapEditorState::StatusUpdate( const std::string& status )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Update status to \"" + status + "\"", s_className + "::" + __func__ );
    m_status.SetText( status );
    Logger::StackPop();
}


