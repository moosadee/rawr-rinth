#include "Tileset.hpp"

#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Utilities/CsvParser.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"
#include "../chalo-engine/Utilities/Helper.hpp"
#include "../chalo-engine/Utilities_SFML/SFMLHelper.hpp"

std::string Tileset::s_className = "Tileset";

Tileset::Tileset()
    : TILE_WIDTH( 20 ), TILE_HEIGHT( 20 )
{
}

void Tileset::LoadTileset( std::string tilesetSpecFile, std::string tilesetName, sf::Vector2f setDimensions, sf::Vector2f positionOffset )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "tilesetSpecFile: " + tilesetSpecFile
                + "<br>tilesetName: " + tilesetName
                + "<br>setDimensions: " + SFMLHelper::DimensionsToString( setDimensions )
                + "<br>positionOffset: " + SFMLHelper::CoordinateToString( positionOffset )
                , s_className + "::" + __func__ );

    m_setDimensions = setDimensions;
    m_positionOffset = positionOffset;

    CsvDocument doc = CsvParser::Parse( tilesetSpecFile );

    std::string debugFileInfo = "";
    for ( size_t row = 0; row < doc.rows.size(); row++ )
    {
        ChaloTile newTile;
        newTile.SetDimensions( sf::IntRect( 0, 0, TILE_WIDTH, TILE_HEIGHT ) );
        newTile.SetFrameRectDimensions( TILE_WIDTH, TILE_HEIGHT );
        newTile.SetTexture( chalo::TextureManager::Get( tilesetName ) );

        for ( size_t col = 0; col < doc.rows[row].size(); col++ )
        {
            std::string val = doc.rows[row][col];
//            debugFileInfo += "Row: " + Helper::ToString( row ) + ", Col: " + Helper::ToString( col ) + + ", Column name: " + doc.header[col] + ", Value: " + val + "<br>";

            if      ( doc.header[col] == "FRAMEX" )
            {
                int pos = Helper::StringToInt( doc.rows[row][col] ) * TILE_WIDTH;
                newTile.SetPositionX( pos + m_positionOffset.x );
                newTile.SetFrameRectLeft( pos );
            }
            else if ( doc.header[col] == "FRAMEY" )
            {
                int pos = Helper::StringToInt( doc.rows[row][col] ) * TILE_HEIGHT;
                newTile.SetPositionY( pos + m_positionOffset.y );
                newTile.SetFrameRectTop( pos );
            }
            else if ( doc.header[col] == "TITLE" )    { newTile.SetName( doc.rows[row][col] ); }
            else if ( doc.header[col] == "CANWALK" )  { newTile.SetCanWalkOn( Helper::StringToInt( doc.rows[row][col] ) ); }
        }

        m_tileset.push_back( newTile );
    }

//    Logger::Out( debugFileInfo, s_className + "::" + __func__ );
    Logger::Out( Helper::ToString( m_tileset.size() ) + " tiles loaded to tileset", s_className + "::" + __func__ );
    Logger::StackPop();
}

void Tileset::Draw( sf::RenderTexture& window )
{
    for ( auto& tile : m_tileset )
    {
        const sf::Sprite& sprite = tile.GetSprite();
        window.draw( sprite );
    }
}

bool Tileset::TileClicked( const sf::Vector2f& mousePos, ChaloTile& tileInfo )
{
    Logger::StackPush( s_className + "::" + __func__ );
    for ( auto& tile : m_tileset )
    {
        if ( SFMLHelper::PointInBoxCollision( mousePos, tile.GetCollisionRegion() ) )
        {
            tileInfo = tile;
            Logger::Out( "Clicked tile with name \"" + tile.GetName() + "\"", s_className + "::" + __func__ );
            Logger::StackPop();
            return true;
        }
    }
    return false;
    Logger::StackPop();
}




