#include "RawrMap.hpp"

#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Utilities_SFML/SFMLHelper.hpp"
#include "../chalo-engine/Utilities/CsvParser.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"

std::string RawrMap::s_className = "RawrMap";

RawrMap::RawrMap()
    : MAP_WIDTH( 20 ), MAP_HEIGHT( 12 ),
      TILE_WIDTH( 20 ), TILE_HEIGHT( 20 )
{
    m_playerStartingPoint = sf::Vector2f( -1, -1 );
    m_player.prop_positionable.position = m_playerStartingPoint;
}

void RawrMap::Setup( std::string terrainTileset, std::string itemTileset, std::string playerTileset )
{
    Logger::StackPush( s_className + "::" + __func__ );
    for ( int y = 0; y < MAP_HEIGHT; y++ )
    {
        for ( int x = 0; x < MAP_WIDTH; x++ )
        {
            // Set textures
            m_floor[x][y].SetTexture( chalo::TextureManager::Get( terrainTileset ) );
            m_terrainArray[x][y].SetTexture( chalo::TextureManager::Get( terrainTileset ) );
            m_itemArray[x][y].SetTexture( chalo::TextureManager::Get( itemTileset ) );

            // Set positions
            m_floor[x][y].SetPosition( sf::Vector2f( x * TILE_WIDTH, y * TILE_HEIGHT ) );
            m_terrainArray[x][y].SetPosition( sf::Vector2f( x * TILE_WIDTH, y * TILE_HEIGHT ) );
            m_itemArray[x][y].SetPosition( sf::Vector2f( x * TILE_WIDTH, y * TILE_HEIGHT ) );

            // Set dimensions
            m_floor[x][y].SetDimensions( sf::IntRect( 0, 0, TILE_WIDTH, TILE_HEIGHT ) );
            m_terrainArray[x][y].SetDimensions( sf::IntRect( 0, 0, TILE_WIDTH, TILE_HEIGHT ) );
            m_itemArray[x][y].SetDimensions( sf::IntRect( 0, 0, TILE_WIDTH, TILE_HEIGHT ) );

            // Set default texture rect
            m_floor[x][y].SetFrameRect( sf::IntRect( 60, 0, TILE_WIDTH, TILE_HEIGHT ) );
            m_floor[x][y].SetName( "floor" );
        }
    }

    m_player.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( playerTileset ) );
    m_player.prop_animatable.frameRect = sf::IntRect( 0, 0, 20, 20 );
    m_player.prop_positionable.position = m_playerStartingPoint;

    Clear();
}

void RawrMap::Clear()
{
    Logger::StackPush( s_className + "::" + __func__ );
    for ( int y = 0; y < MAP_HEIGHT; y++ )
    {
        for ( int x = 0; x < MAP_WIDTH; x++ )
        {
            m_floor[x][y].SetCanWalkOn( true );
            m_floor[x][y].SetLayerIndex( 0 );
            m_terrainArray[x][y].SetCanWalkOn( true );
            m_terrainArray[x][y].SetLayerIndex( 1 );
            m_itemArray[x][y].SetCanWalkOn( true );
            m_itemArray[x][y].SetLayerIndex( 2 );

            // Set default texture rect
            m_floor[x][y].SetFrameRect( sf::IntRect( 60, 0, TILE_WIDTH, TILE_HEIGHT ) );

            if ( x == 0 || y == 0 || x == MAP_WIDTH - 1 || y == MAP_HEIGHT - 1 )
            {
                // Can't walk boundaries
                m_floor[x][y].SetCanWalkOn( false );
                m_terrainArray[x][y].SetCanWalkOn( false );

                // Ceiling
                m_terrainArray[x][y].SetFrameRect( sf::IntRect( 0, 20, TILE_WIDTH, TILE_HEIGHT ) );
                m_terrainArray[x][y].SetName( "ceiling" );
            }
            else
            {
                // Floor; invisible
                m_terrainArray[x][y].SetFrameRect( sf::IntRect( 0, 0, TILE_WIDTH, TILE_HEIGHT ) );
                m_terrainArray[x][y].SetName( "erase" );
            }

            m_itemArray[x][y].SetFrameRect( sf::IntRect( 0, 0, TILE_WIDTH, TILE_HEIGHT ) );
            m_itemArray[x][y].SetName( "erase" );
        }
    }
    // Clear the objects
    m_npcs.clear();
    m_objects.clear();

    Logger::StackPop();
}

void RawrMap::Draw( sf::RenderTexture& window )
{
    for ( int y = 0; y < MAP_HEIGHT; y++ )
    {
        for ( int x = 0; x < MAP_WIDTH; x++ )
        {
            window.draw( m_floor[x][y].GetSprite() );
            window.draw( m_terrainArray[x][y].GetSprite() );
            window.draw( m_itemArray[x][y].GetSprite() );

            // Debug draw
            window.draw( m_floor[x][y].GetDebugRectangle() );
            window.draw( m_terrainArray[x][y].GetDebugRectangle() );
            window.draw( m_itemArray[x][y].GetDebugRectangle() );
        }
    }

    for ( auto& npc : m_npcs )
    {
        npc.Draw( window );
    }

    for ( auto& object : m_objects )
    {
        object.Draw( window );
    }

    m_player.Draw( window );
}

bool RawrMap::HandleInput()
{
    bool turnUpdate = false;

    bool playerWantsToMove = false;

    sf::Vector2i currentPosition = GetTileCoordinates( m_player.prop_positionable.position );
    sf::Vector2i desiredPosition = currentPosition;

    // Check for keyboard input
    if      (   chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::Up ) )
    {
        desiredPosition.y--;
        playerWantsToMove = true;
    }
    else if (   chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::Down ) )
    {
        desiredPosition.y++;
        playerWantsToMove = true;
    }
    else if (   chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::Left ) )
    {
        desiredPosition.x--;
        playerWantsToMove = true;
    }
    else if (   chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::Right ) )
    {
        desiredPosition.x++;
        playerWantsToMove = true;
    }

//    CollisionBehavior collisionBehavior = GetCollisionBehavior( desiredPosition );

    bool canMove = CanWalkOnTile( desiredPosition );
    bool canPush = CanPushTile( desiredPosition );

    if ( playerWantsToMove && canMove )
    {
        m_player.prop_positionable.position = GetPositionFromIndices( desiredPosition );
//        NextTurn();
        turnUpdate = true;
    }
    else if ( playerWantsToMove && canPush )
    {
        bool pushResult = PushObject( currentPosition, desiredPosition );
        if ( pushResult == true )
        {
//            m_inputCooldown = m_inputCooldownMax;
            m_player.prop_positionable.position = GetPositionFromIndices( desiredPosition );
//            NextTurn();
            turnUpdate = true;
        }
    }

    if ( turnUpdate )
    {
        UpdateTurn();
        return true;
    }
    else
    {
        return false;
    }
}

void RawrMap::UpdateTurn()
{
    for ( auto& npc : m_npcs )
    {
        npc.UpdateTurn();

        if ( npc.prop_character.type == "2dir" && npc.prop_animatable.IsUpdateFrame() )
        {
            sf::Vector2i currentPosition = GetTileCoordinates( npc.prop_positionable.position );
            sf::Vector2i desiredPosition = currentPosition;

            if      ( npc.prop_animatable.direction == Direction::NORTH )
            {
                desiredPosition.y--;
            }
            else if  ( npc.prop_animatable.direction == Direction::SOUTH )
            {
                desiredPosition.y++;
            }
            else if  ( npc.prop_animatable.direction == Direction::WEST )
            {
                desiredPosition.x--;
            }
            else if  ( npc.prop_animatable.direction == Direction::EAST )
            {
                desiredPosition.x++;
            }

            bool canMove = CanWalkOnTile( desiredPosition );

            if ( canMove )
            {
                npc.prop_positionable.position = GetPositionFromIndices( desiredPosition );
            }
            else
            {
                npc.prop_animatable.FlipDirection();
            }
        }
    }
}

void RawrMap::Save( const std::string& filename )
{
    Logger::StackPush( s_className + "::" + __func__ );

    std::vector<std::string> columns = {
        "LAYER",
        "NAME",
        "CANWALKON",
        "X",
        "Y",
        "POSITION_X",
        "POSITION_Y",
        "DIMENSIONS_WIDTH",
        "DIMENSIONS_HEIGHT",
        "FRAMERECT_LEFT",
        "FRAMERECT_TOP",
        "FRAMERECT_WIDTH",
        "FRAMERECT_HEIGHT"
    };

    CsvDocument doc;
    doc.header = columns;

    for ( int y = 0; y < MAP_HEIGHT; y++ )
    {
        for ( int x = 0; x < MAP_WIDTH; x++ )
        {
            std::vector<std::string> row;
            row.push_back( "FLOOR" );                                                           // LAYER
            row.push_back( m_floor[x][y].GetName() );                                           // NAME
            row.push_back( Helper::ToString( m_floor[x][y].GetCanWalkOn() ) );                  // CANWALKON
            row.push_back( Helper::ToString( x ) );                                             // X
            row.push_back( Helper::ToString( y ) );                                             // Y
            row.push_back( Helper::ToString( m_floor[x][y].GetPositionX() ) );                  // POSITION_X
            row.push_back( Helper::ToString( m_floor[x][y].GetPositionY() ) );                  // POSITION_Y
            row.push_back( Helper::ToString( m_floor[x][y].GetDimensionsWidth() ) );            // DIMENSIONS_WIDTH
            row.push_back( Helper::ToString( m_floor[x][y].GetDimensionsHeight() ) );           // DIMENSIONS_HEIGHT
            row.push_back( Helper::ToString( m_floor[x][y].GetFrameRectLeft() ) );              // FRAMERECT_LEFT
            row.push_back( Helper::ToString( m_floor[x][y].GetFrameRectTop() ) );               // FRAMERECT_TOP
            row.push_back( Helper::ToString( m_floor[x][y].GetFrameRectWidth() ) );             // FRAMERECT_WIDTH
            row.push_back( Helper::ToString( m_floor[x][y].GetFrameRectHeight() ) );            // FRAMERECT_HEIGHT
            doc.rows.push_back( row );

            row.clear();
            row.push_back( "TERRAIN" );                                                         // LAYER
            row.push_back( m_terrainArray[x][y].GetName() );                                    // NAME
            row.push_back( Helper::ToString( m_terrainArray[x][y].GetCanWalkOn() ) );           // CANWALKON
            row.push_back( Helper::ToString( x ) );                                             // X
            row.push_back( Helper::ToString( y ) );                                             // Y
            row.push_back( Helper::ToString( m_terrainArray[x][y].GetPositionX() ) );           // POSITION_X
            row.push_back( Helper::ToString( m_terrainArray[x][y].GetPositionY() ) );           // POSITION_Y
            row.push_back( Helper::ToString( m_terrainArray[x][y].GetDimensionsWidth() ) );     // DIMENSIONS_WIDTH
            row.push_back( Helper::ToString( m_terrainArray[x][y].GetDimensionsHeight() ) );    // DIMENSIONS_HEIGHT
            row.push_back( Helper::ToString( m_terrainArray[x][y].GetFrameRectLeft() ) );       // FRAMERECT_LEFT
            row.push_back( Helper::ToString( m_terrainArray[x][y].GetFrameRectTop() ) );        // FRAMERECT_TOP
            row.push_back( Helper::ToString( m_terrainArray[x][y].GetFrameRectWidth() ) );      // FRAMERECT_WIDTH
            row.push_back( Helper::ToString( m_terrainArray[x][y].GetFrameRectHeight() ) );     // FRAMERECT_HEIGHT
            doc.rows.push_back( row );

            row.clear();
            row.push_back( "ITEMS" );                                                           // LAYER
            row.push_back( m_itemArray[x][y].GetName() );                                       // NAME
            row.push_back( Helper::ToString( m_itemArray[x][y].GetCanWalkOn() ) );              // CANWALKON
            row.push_back( Helper::ToString( x ) );                                             // X
            row.push_back( Helper::ToString( y ) );                                             // Y
            row.push_back( Helper::ToString( m_itemArray[x][y].GetPositionX() ) );              // POSITION_X
            row.push_back( Helper::ToString( m_itemArray[x][y].GetPositionY() ) );              // POSITION_Y
            row.push_back( Helper::ToString( m_itemArray[x][y].GetDimensionsWidth() ) );        // DIMENSIONS_WIDTH
            row.push_back( Helper::ToString( m_itemArray[x][y].GetDimensionsHeight() ) );       // DIMENSIONS_HEIGHT
            row.push_back( Helper::ToString( m_itemArray[x][y].GetFrameRectLeft() ) );          // FRAMERECT_LEFT
            row.push_back( Helper::ToString( m_itemArray[x][y].GetFrameRectTop() ) );           // FRAMERECT_TOP
            row.push_back( Helper::ToString( m_itemArray[x][y].GetFrameRectWidth() ) );         // FRAMERECT_WIDTH
            row.push_back( Helper::ToString( m_itemArray[x][y].GetFrameRectHeight() ) );        // FRAMERECT_HEIGHT
            doc.rows.push_back( row );
        }
    }

    Logger::Out( "Saving map to " + filename, s_className + "::" + __func__ );
    CsvParser::Save( filename, doc );

    Logger::StackPop();
}

void RawrMap::Load( const std::string& filename, bool mapEditor /*= false*/ )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Loading map " + filename, s_className + "::" + __func__ );

    Logger::Out( "CSV Parser...", s_className + "::" + __func__ );
    CsvDocument doc = CsvParser::Parse( filename );

    int x, y;
    string layer;
    string name;
    bool canWalkOn;
    sf::Vector2f position( 0, 0 );
    sf::IntRect dimensions( 0, 0, 0, 0 );
    sf::IntRect frameRect( 0, 0, 0, 0 );

    for ( size_t row = 0; row < doc.rows.size(); row++ )
    {
        for ( size_t col = 0; col < doc.rows[row].size(); col++ )
        {
            if      ( doc.header[col] == "LAYER" )              { layer = doc.rows[row][col]; }
            else if ( doc.header[col] == "NAME" )               { name = doc.rows[row][col]; }
            else if ( doc.header[col] == "CANWALKON" )          { canWalkOn = Helper::StringToInt( doc.rows[row][col] ); }
            else if ( doc.header[col] == "X" )                  { x = Helper::StringToFloat( doc.rows[row][col] ); }
            else if ( doc.header[col] == "Y" )                  { y = Helper::StringToFloat( doc.rows[row][col] ); }
            else if ( doc.header[col] == "POSITION_X" )         { position.x = Helper::StringToFloat( doc.rows[row][col] ); }
            else if ( doc.header[col] == "POSITION_Y" )         { position.y = Helper::StringToFloat( doc.rows[row][col] ); }
            else if ( doc.header[col] == "DIMENSIONS_WIDTH" )   { dimensions.width = Helper::StringToInt( doc.rows[row][col] ); }
            else if ( doc.header[col] == "DIMENSIONS_HEIGHT" )  { dimensions.height = Helper::StringToInt( doc.rows[row][col] ); }
            else if ( doc.header[col] == "FRAMERECT_LEFT" )     { frameRect.left = Helper::StringToInt( doc.rows[row][col] ); }
            else if ( doc.header[col] == "FRAMERECT_TOP" )      { frameRect.top = Helper::StringToInt( doc.rows[row][col] ); }
            else if ( doc.header[col] == "FRAMERECT_WIDTH" )    { frameRect.width = Helper::StringToInt( doc.rows[row][col] ); }
            else if ( doc.header[col] == "FRAMERECT_HEIGHT" )   { frameRect.height = Helper::StringToInt( doc.rows[row][col] ); }
        }

        if ( layer == "FLOOR" )
        {
        }
        else if ( layer == "TERRAIN" )
        {
            m_terrainArray[x][y].SetCanWalkOn( canWalkOn );
            m_terrainArray[x][y].SetDimensions( dimensions );
            m_terrainArray[x][y].SetFrameRect( frameRect );
            m_terrainArray[x][y].SetPosition( position );
            m_terrainArray[x][y].SetName( name );
        }
        else if ( layer == "ITEMS" )
        {
            if ( mapEditor )
            {
                m_itemArray[x][y].SetCanWalkOn( canWalkOn );
                m_itemArray[x][y].SetDimensions( dimensions );
                m_itemArray[x][y].SetFrameRect( frameRect );
                m_itemArray[x][y].SetPosition( position );
                m_itemArray[x][y].SetName( name );
            }
            // Player
            else if ( name == "rawr start" )
            {
                m_playerStartingPoint = position;
                m_player.prop_positionable.position = m_playerStartingPoint;
            }
            // Enemies
            if ( Helper::Contains( name, "enemy" ) )
            {
                Direction dir = Direction::NORTH;
                if      ( Helper::Contains( name, "south" ) ) { dir = Direction::SOUTH; }
                else if ( Helper::Contains( name, "north" ) ) { dir = Direction::NORTH; }
                else if ( Helper::Contains( name, "east" ) )  { dir = Direction::EAST;  }
                else if ( Helper::Contains( name, "west" ) )  { dir = Direction::WEST;  }

                Object npc;
                npc.prop_animatable.frameRect       = frameRect;
                npc.prop_animatable.direction       = dir;
                npc.prop_positionable.position      = position;
                npc.prop_positionable.dimensions    = dimensions;

                if ( Helper::Contains( name, "static" ) )
                {
                    npc.prop_character.type = "static";
                    npc.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-static" ) );
                }
                else if ( Helper::Contains( name, "turner" ) )
                {
                    npc.prop_character.type = "turner";
                    npc.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-turner" ) );
                }
                else if ( Helper::Contains( name, "2dir" ) )
                {
                    npc.prop_character.type = "2dir";
                    npc.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-2dir" ) );
                }

                Logger::Out( "Adding NPC of type \"" + npc.prop_character.type + "\" NPC to map", s_className + "::" + __func__ );
                m_npcs.push_back( npc );
            }
            // Items
            else if ( name == "crate" )
            {
                Object object;
                object.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-items" ) );
                object.prop_animatable.frameRect = frameRect;
                object.prop_character.type = name;
                object.prop_character.collisionBehavior = CollisionBehavior::SHOVE;
                object.prop_positionable.position = position;
                object.prop_positionable.dimensions = dimensions;
                m_objects.push_back( object );
            }
            else if ( name == "bomb" )
            {
                Object object;
                object.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-items" ) );
                object.prop_animatable.frameRect = frameRect;
                object.prop_character.type = name;
                object.prop_character.collisionBehavior = CollisionBehavior::PICKUP;
                object.prop_positionable.position = position;
                object.prop_positionable.dimensions = dimensions;
                m_objects.push_back( object );
            }
            else if ( name == "key" )
            {
                Object object;
                object.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-items" ) );
                object.prop_animatable.frameRect = frameRect;
                object.prop_character.type = name;
                object.prop_character.collisionBehavior = CollisionBehavior::PICKUP;
                object.prop_positionable.position = position;
                object.prop_positionable.dimensions = dimensions;
                m_objects.push_back( object );
            }
            else if ( name == "egg" )
            {
                Object object;
                object.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-items" ) );
                object.prop_animatable.frameRect = frameRect;
                object.prop_character.type = name;
                object.prop_character.collisionBehavior = CollisionBehavior::PICKUP;
                object.prop_positionable.position = position;
                object.prop_positionable.dimensions = dimensions;
                m_objects.push_back( object );
            }
        }
    }

    Logger::Out( Helper::ToString( m_npcs.size() ) + " NPCs loaded", s_className + "::" + __func__ );

    Logger::StackPop();
}

void RawrMap::Editor_Click( const sf::Vector2f& mousePos, BrushType brushType, ChaloTile& brush )
{
    Logger::StackPush( s_className + "::" + __func__ );

    for ( int y = 1; y < MAP_HEIGHT-1; y++ )
    {
        for ( int x = 1; x < MAP_WIDTH-1; x++ )
        {
            if ( brushType == BrushType::TERRAIN )
            {
                if ( SFMLHelper::PointInBoxCollision( mousePos, m_terrainArray[x][y].GetCollisionRegion() ) )
                {
                    m_terrainArray[x][y].SetFrameRect( brush.GetFrameRect() );
                    m_terrainArray[x][y].SetName( brush.GetName() );
                    m_terrainArray[x][y].SetCanWalkOn( brush.GetCanWalkOn() );
                }
            }

            else if ( brushType == BrushType::ITEM )
            {
                if ( SFMLHelper::PointInBoxCollision( mousePos, m_itemArray[x][y].GetCollisionRegion() ) )
                {
                    m_itemArray[x][y].SetFrameRect( brush.GetFrameRect() );
                    m_itemArray[x][y].SetName( brush.GetName() );
                    m_itemArray[x][y].SetCanWalkOn( brush.GetCanWalkOn() );
                }
            }
        }
    }
    Logger::StackPop();
}

sf::Vector2f RawrMap::GetPlayerStartingPoint()
{
    return m_playerStartingPoint;
}

bool RawrMap::CanWalkOnTile( sf::Vector2i tileIndices )
{
    Logger::StackPush( s_className + "::" + __func__ );

    if ( m_terrainArray[tileIndices.x][tileIndices.y].GetCanWalkOn() == false ||
        m_itemArray[tileIndices.x][tileIndices.y].GetCanWalkOn() == false )
    {
        Logger::StackPop();
        return false;
    }

    // Check for objects that are in the way
    for ( auto& object : m_objects )
    {
        //object.Debug();
        if ( object.prop_positionable.position == GetPositionFromIndices( tileIndices )
            //&& object.GetCanWalkOn() == false )
            && object.prop_character.collisionBehavior == CollisionBehavior::BLOCK )
        {
            Logger::StackPop();
            return false;
        }
    }

    // Check for enemies in the way
    for ( auto& npc : m_npcs )
    {
        if ( npc.prop_positionable.position == GetPositionFromIndices( tileIndices ) )
        {
            Logger::StackPop();
            return false;
        }
    }

    Logger::StackPop();
    return true;
}

bool RawrMap::CanPushTile( sf::Vector2i tileIndices )
{
    bool canPush = false;

    // Check for objects that are in the way
    for ( auto& object : m_objects )
    {
        if ( object.prop_positionable.position == GetPositionFromIndices( tileIndices )
            //&& object.GetCanPush() )
            && object.prop_character.collisionBehavior == CollisionBehavior::SHOVE )
        {
            // You can push it
            canPush = true;
        }
    }

    // Check for enemies in the way
    for ( auto& npc : m_npcs )
    {
        if ( npc.prop_positionable.position == GetPositionFromIndices( tileIndices ) )
        {
            // Can't push into another object
            canPush = false;
        }
    }

    return canPush;
}

CollisionBehavior RawrMap::GetCollisionBehavior( sf::Vector2i tileIndices )
{
    CollisionBehavior behavior = CollisionBehavior::NOTHING;

    // Check for objects that are in the way
    for ( auto& object : m_objects )
    {
        if ( object.prop_positionable.position == GetPositionFromIndices( tileIndices )
            && object.prop_character.collisionBehavior != CollisionBehavior::NOTHING )
        {
            behavior = object.prop_character.collisionBehavior;
        }
    }

    // Check for enemies in the way
    for ( auto& npc : m_npcs )
    {
        if ( npc.prop_positionable.position == GetPositionFromIndices( tileIndices ) )
        {
            behavior = CollisionBehavior::BLOCK;
        }
    }

    return behavior;
}

bool RawrMap::PushObject( sf::Vector2i pusherPosition, sf::Vector2i objectPosition )
{
    // Which direction are they coming from?
//    Direction pushDirection = Direction::NONE;
    sf::Vector2i moveToLocation = objectPosition;

    if      ( pusherPosition.x < objectPosition.x ) // Pushed from the left, going to the right
    {
//        pushDirection = Direction::EAST;
        moveToLocation.x++;
    }
    else if ( pusherPosition.x > objectPosition.x ) // Pushed from the right, going to the left
    {
//        pushDirection = Direction::WEST;
        moveToLocation.x--;
    }
    else if ( pusherPosition.y < objectPosition.y ) // Pushed from the top, going down
    {
//        pushDirection = Direction::SOUTH;
        moveToLocation.y++;
    }
    else if ( pusherPosition.y > objectPosition.y ) // Pushed from the bottom, going up
    {
//        pushDirection = Direction::NORTH;
        moveToLocation.y--;
    }

    // Is there space for the object to go to the new position?
    bool canMove = CanWalkOnTile( moveToLocation );

    if ( canMove == false )
    {
        // Cannot push; fail
        return false;
    }

    try
    {
        Object& obj = GetObjectAtArrayIndices( objectPosition );
        obj.prop_positionable.position = GetPositionFromIndices( moveToLocation );
    }
    catch( ... )
    {
    }

    return true;
}

Object& RawrMap::GetObjectAtArrayIndices( sf::Vector2i tileIndices )
{
    // Check for objects that are in the way
    for ( auto& object : m_objects )
    {
        if ( object.prop_positionable.position == GetPositionFromIndices( tileIndices ) )
        {
            return object;
        }
    }

    throw runtime_error( "object not found at position" );
}

sf::Vector2i RawrMap::GetTileCoordinates( sf::Vector2f position )
{
    sf::Vector2i arrayIndices;
    arrayIndices.x = position.x / TILE_WIDTH;
    arrayIndices.y = position.y / TILE_HEIGHT;
    return arrayIndices;
}

sf::Vector2f RawrMap::GetPositionFromIndices( sf::Vector2i tileIndices )
{
    return m_floor[tileIndices.x][tileIndices.y].GetPosition();
}
