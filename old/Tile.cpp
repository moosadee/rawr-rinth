#include "Tile.hpp"

#include "../chalo-engine/Utilities/Logger.hpp"
#include "../chalo-engine/Utilities_SFML/SFMLHelper.hpp"
#include "../chalo-engine/Utilities/Helper.hpp"

#include <string>

Tile& Tile::operator=( Tile other )
{
    if ( this == &other ) { return *this; }

    m_position = other.m_position;
    m_dimensions = other.m_dimensions;
    m_frameRect = other.m_frameRect;
    m_sprite = other.m_sprite;
    m_name = other.m_name;
    m_canWalkOn = other.m_canWalkOn;

    return *this;
}

void Tile::SetTexture( const sf::Texture& texture )
{
    m_sprite.setTexture( texture );
}

void Tile::SetPosition( sf::Vector2f position )
{
    m_position = position;
    m_sprite.setPosition( position );
}

void Tile::SetPositionX( float value )
{
    m_position.x = value;
}

void Tile::SetPositionY( float value )
{
    m_position.y = value;
}

void Tile::SetFrameRect( sf::IntRect frameRect )
{
    m_frameRect = frameRect;
}

void Tile::SetFrameRectDimensions( int width, int height )
{
    m_frameRect.width = width;
    m_frameRect.height = height;
}

void Tile::SetFrameRectLeft( int value )
{
    m_frameRect.left = value;
}

void Tile::SetFrameRectTop( int value )
{
    m_frameRect.top = value;
}

void Tile::SetDimensions( sf::IntRect dimensions )
{
    m_dimensions = dimensions;
}

void Tile::SetName( std::string value )
{
    m_name = value;
}

void Tile::SetCanWalkOn( bool value )
{
    m_canWalkOn = value;
}

void Tile::SetLayerIndex( int value )
{
    m_layerIndex = value;
}

sf::Sprite Tile::GetSprite()
{
    m_sprite.setPosition( m_position );
    m_sprite.setTextureRect( m_frameRect );
    return m_sprite;
}

sf::RectangleShape Tile::GetDebugRectangle()
{
    m_debugShape.setPosition( m_position );
    m_debugShape.setSize( sf::Vector2f( m_dimensions.width, m_dimensions.height ) );

    m_debugShape.setFillColor( sf::Color( 0, 0, 0, 0 ) );
    if ( !m_canWalkOn )
    {
        if ( m_layerIndex == 0 )
        {
            m_debugShape.setFillColor( sf::Color( 255, 0, 0, 100 ) );
        }
        else if ( m_layerIndex == 1 )
        {
            m_debugShape.setFillColor( sf::Color( 0, 255, 0, 100 ) );
        }
        else if ( m_layerIndex == 2 )
        {
            m_debugShape.setFillColor( sf::Color( 0, 0, 255, 100 ) );
        }
    }

    return m_debugShape;
}

sf::IntRect Tile::GetCollisionRegion()
{
    sf::IntRect collisionRegion;
    collisionRegion.left = m_position.x;
    collisionRegion.top = m_position.y;
    collisionRegion.width = m_dimensions.width;
    collisionRegion.height = m_dimensions.height;
    return collisionRegion;
}

const sf::Texture* Tile::GetTexture()
{
    return m_sprite.getTexture();
}

sf::IntRect Tile::GetFrameRect()
{
    return m_frameRect;
}

sf::Vector2f Tile::GetPosition()
{
    return m_position;
}

std::string Tile::GetName()
{
    return m_name;
}

bool Tile::GetCanWalkOn()
{
    return m_canWalkOn;
}

float Tile::GetPositionX()
{
    return m_position.x;
}

float Tile::GetPositionY()
{
    return m_position.y;
}

int Tile::GetDimensionsWidth()
{
    return m_dimensions.width;
}

int Tile::GetDimensionsHeight()
{
    return m_dimensions.height;
}

int Tile::GetFrameRectTop()
{
    return m_frameRect.top;
}

int Tile::GetFrameRectLeft()
{
    return m_frameRect.left;
}

int Tile::GetFrameRectWidth()
{
    return m_frameRect.width;
}

int Tile::GetFrameRectHeight()
{
    return m_frameRect.height;
}

void Tile::Debug()
{
    Logger::StackPush( "Tile::Debug" );
    Logger::Out( "m_name: " + m_name
                + "<br>m_position: " + SFMLHelper::CoordinateToString( m_position )
                + "<br>m_dimensions: " + SFMLHelper::RectangleToString( m_dimensions )
                + "<br>m_frameRect: " + SFMLHelper::RectangleToString( m_frameRect )
                + "<br>m_canWalkOn: " + Helper::ToString( m_canWalkOn )
                , "Tile::Debug" );
    Logger::StackPop();
}

