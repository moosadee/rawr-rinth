#ifndef _DCLASS
#define _DCLASS

#include <string>

// TODO: Not sure if I can inherit static members in a class?

class DClass
{
public:
    static std::string s_className;
};

#endif
