// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef _TYPES_HPP
#define _TYPES_HPP

#include "../Utilities/Helper.hpp"

namespace chalo
{

enum LayerType { UNDEFINEDLAYER = 0, BELOWTILELAYER = 1, ABOVETILELAYER = 2, WARPLAYER = 3, PLACEMENTLAYER = 4, COLLISIONLAYER = 5, SHADOWLAYER = 6 };

enum Direction { SOUTH = 0, NORTH = 1, WEST = 2, EAST = 3, NONE = 4 };
enum SheetAction { WALKING = 0, ATTACKING = 1 }; // This relates to the sprite sheet
enum Action { IDLE, MOVING, BEGIN_ATTACK }; // For game logic
enum CharacterType { PLAYER, GOBLIN };

enum ActiveState { ACTIVE, WAITING_DELETION, INVISIBLE };

enum InputAction {
    INPUT_NORTH, INPUT_SOUTH, INPUT_WEST, INPUT_EAST, INPUT_ACTION1, INPUT_ACTION2, INPUT_ACTION3, INPUT_ACTION4, INPUT_ACTION5, INPUT_ACTION6
};

struct PlayerInputAction {
    PlayerInputAction() { }

    PlayerInputAction( int i, InputAction a )
    {
        action = a;
        index = i;
        name = Helper::ToString( index ) + "-" + Helper::ToString( a );
    }

    InputAction action;
    int index;
    std::string name;
};

const std::vector<PlayerInputAction> INPUT_ACTIONS = {
    PlayerInputAction( 0, INPUT_NORTH ),
    PlayerInputAction( 0, INPUT_SOUTH ),
    PlayerInputAction( 0, INPUT_WEST ),
    PlayerInputAction( 0, INPUT_EAST ),
    PlayerInputAction( 0, INPUT_ACTION1 ),
    PlayerInputAction( 0, INPUT_ACTION2 ),
    PlayerInputAction( 0, INPUT_ACTION3 ),
    PlayerInputAction( 0, INPUT_ACTION4 ),
    PlayerInputAction( 0, INPUT_ACTION5 ),
    PlayerInputAction( 0, INPUT_ACTION6 ),

    PlayerInputAction( 1, INPUT_NORTH ),
    PlayerInputAction( 1, INPUT_SOUTH ),
    PlayerInputAction( 1, INPUT_WEST ),
    PlayerInputAction( 1, INPUT_EAST ),
    PlayerInputAction( 1, INPUT_ACTION1 ),
    PlayerInputAction( 1, INPUT_ACTION2 ),
    PlayerInputAction( 1, INPUT_ACTION3 ),
    PlayerInputAction( 1, INPUT_ACTION4 ),
    PlayerInputAction( 1, INPUT_ACTION5 ),
    PlayerInputAction( 1, INPUT_ACTION6 ),

    PlayerInputAction( 2, INPUT_NORTH ),
    PlayerInputAction( 2, INPUT_SOUTH ),
    PlayerInputAction( 2, INPUT_WEST ),
    PlayerInputAction( 2, INPUT_EAST ),
    PlayerInputAction( 2, INPUT_ACTION1 ),
    PlayerInputAction( 2, INPUT_ACTION2 ),
    PlayerInputAction( 2, INPUT_ACTION3 ),
    PlayerInputAction( 2, INPUT_ACTION4 ),
    PlayerInputAction( 2, INPUT_ACTION5 ),
    PlayerInputAction( 2, INPUT_ACTION6 ),

    PlayerInputAction( 3, INPUT_NORTH ),
    PlayerInputAction( 3, INPUT_SOUTH ),
    PlayerInputAction( 3, INPUT_WEST ),
    PlayerInputAction( 3, INPUT_EAST ),
    PlayerInputAction( 3, INPUT_ACTION1 ),
    PlayerInputAction( 3, INPUT_ACTION2 ),
    PlayerInputAction( 3, INPUT_ACTION3 ),
    PlayerInputAction( 3, INPUT_ACTION4 ),
    PlayerInputAction( 3, INPUT_ACTION5 ),
    PlayerInputAction( 3, INPUT_ACTION6 )
};

}

#endif
