// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef _TILE
#define _TILE

#include <SFML/Graphics.hpp>

#include "GameObject.hpp"
#include "../../Enums/Types.hpp"

#include <string>
#include <iostream>

namespace chalo
{

namespace Map
{

class Tile : public GameObject
{
public:
    Tile();

    void SetTileType( LayerType type );

    void SetImageCode( int code );
    int GetImageCode() const;

    void Update();
    void DrawDebug();
    const sf::Sprite& GetSprite() const;

    std::string GetTileTypeName() const;
    LayerType GetTileType() const;

    std::string GetLayerName() const;
    void SetLayerName( const std::string& name );

    int GetLayerIndex() const;
    void SetLayerIndex( int index );

protected:
    LayerType m_tileType;
    std::string m_layerName;
    int m_layerIndex;
    int m_imageCode;
};





//! Decorative shadow tiles on the map, to be drawn semi-transparent
class Shadow : public Tile
{
public:
    Shadow();
    Shadow( Tile tile );

    void Update();
};

}

}

#endif
