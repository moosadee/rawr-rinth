// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef _PROJECTILE
#define _PROJECTILE

#include <SFML/Graphics.hpp>

#include "Character.hpp"

namespace chalo
{

class Projectile : public Character
{
    public:
    void Update();

    protected:
};

}

#endif
