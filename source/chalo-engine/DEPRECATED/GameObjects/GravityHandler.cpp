#include "GravityHandler.hpp"

namespace chalo
{

GravityHandler::GravityHandler()
{
    m_isFalling = false;
}

bool GravityHandler::GetIsFalling()
{
    return m_isFalling;
}

void GravityHandler::SetIsFalling( bool value )
{
    m_isFalling = value;
}

float GravityHandler::GetGravity()
{
    return m_gravity;
}

void GravityHandler::SetGravity( float value )
{
    m_gravity = value;
}

}
