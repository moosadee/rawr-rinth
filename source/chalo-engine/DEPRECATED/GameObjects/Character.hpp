// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef _CHARACTER
#define _CHARACTER

#include <SFML/Graphics.hpp>

#include "GameObject.hpp"
#include "GravityHandler.hpp"

#include "../../Utilities/Logger.hpp"
#include "../../Enums/Types.hpp"
#include "../Maps/ReadableMap.hpp"

#include <string>
#include <iostream>

namespace chalo
{

/*
TODO: Maybe change this into "animated" or "multi-frame" object?
And then take the attack specific stuff in a Character class?
Or these can be turned into has-a relationships. I don't know. :T
*/
class Character : public GameObject
{
public:
    Character();

    void Update();
    const sf::Sprite& GetSprite() const;

    void SetSpeed( int speed );
    void SetDirection( Direction direction );
    Direction GetDirection() const;
    CharacterType GetCharacterType() const;

    void SetAnimationInformation( int maxFrames, float animationSpeed );

    void Move( Direction direction, const Map::ReadableMap& gameMap );
    void Move( Direction direction );
    void Move( Direction direction, int minX, int minY, int maxX, int maxY );
    sf::IntRect GetDesiredPosition( Direction direction );

    GravityHandler gravity;

    void ForceSpriteUpdate();
    void Animate();

    void RestrictMovement();
    void SetRestrictMovement( bool value );
    sf::IntRect GetValidPositionRegion() const;
    void SetValidPositionRegion( sf::IntRect rect );


protected:
    void MoveClipping( Direction direction );
    void BeginAttack();
    void EndAttack();

    Direction m_direction;
    int m_speed;
    float m_animationFrame;
    float m_maxFrames;
    float m_animationSpeed;

    SheetAction m_sheetAction;
    float m_sheetActionTimer;

    Action m_stateAction;

    CharacterType m_characterType;

    sf::IntRect m_validPositionRegion;
    bool m_restrictMovement;

};

}

#endif
