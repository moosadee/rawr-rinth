// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef _GAME_OBJECT
#define _GAME_OBJECT

#include <SFML/Graphics.hpp>

#include "../../Managers/FontManager.hpp"
#include "../../Enums/Types.hpp"
#include "../../DEPRECATED/Camera/Camera.hpp"

#include <string>
#include <iostream>

namespace chalo
{

class GameObject
{
public:
    GameObject();

    void Setup( int initialX = 0, int initialY = 0, const std::string& name = "unnamed" );
    void SetTexture( const sf::Texture& texture, sf::IntRect& textureCoordinates );
    void SetTexture( const sf::Texture& texture, int textureLeft, int textureTop, int textureWidth, int textureHeight );
    void SetTextureRegion( sf::IntRect& textureCoordinates );
    void Update();
    void SetWindowReference( sf::RenderWindow& window );
    void SetPosition( sf::Vector2f position );
    void SetPosition( int x, int y );
    const sf::Sprite& GetSprite() const;
    sf::Sprite GetSpriteWithOffset( const Camera& camera ) const;
    bool IsOffscreen( int minX, int minY, int maxX, int maxY ) const;

    void SetMapCollisionRectangle( sf::IntRect rect );
    void SetObjectCollisionRectangle( sf::IntRect rect );
    sf::IntRect GetMapCollisionRegion() const;
    sf::IntRect GetObjectCollisionRegion() const;
    sf::IntRect GetPositionRegion() const;
    sf::Vector2f GetPosition() const;
    sf::Vector2f GetDimensions() const;
    sf::Vector2f GetCenterPosition() const;
    sf::IntRect GetTextureRegion() const;

    void DrawDebug();

    std::string GetName() const;

    ActiveState GetActiveState() const;
    void SetActiveState( ActiveState state );

    sf::Vector2f GetScale() const;
    void SetScale( sf::Vector2f scale );
    void SetScale( float x, float y );

protected:
    sf::Vector2f m_position;
    std::string m_name;
    sf::Sprite m_sprite;
    sf::IntRect m_textureCoordinates;

    sf::IntRect m_mapCollisionRectangle;
    sf::IntRect m_objectCollisionRectangle;
    sf::Color m_debugRectColor;

    sf::RenderWindow* m_ptrWindow;

    ActiveState m_activeState;

    sf::Vector2f m_scale;

};

}

#endif
