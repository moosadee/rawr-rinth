// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <SFML/Graphics.hpp>

#include "../../Enums/Types.hpp"

namespace chalo
{

class Camera
{
    public:
    Camera();

    void SetDimensions( int width, int height );
    void SetDimensions( sf::IntRect viewRectangle );
    void SetPosition( float x, float y );
    void SetPosition( sf::Vector2f position );
    void CenterOn( float x, float y );
    void CenterOn( sf::Vector2f point );
    void Pan( Direction direction );
    void SetSpeed( float speed );
    void SetRegionBound( sf::IntRect region );

    sf::Vector2f GetPosition() const;
    sf::IntRect GetCameraViewRect() const;
    float GetX() const;
    float GetY() const;

    int GetCameraLeftSide() const;
    int GetCameraRightSide() const;
    int GetCameraTopSide() const;
    int GetCameraBottomSide() const;
    void SetCameraLeftSide( int value );
    void SetCameraRightSide( int value );
    void SetCameraTopSide( int value );
    void SetCameraBottomSide( int value );

    protected:
    sf::Vector2f m_position;
    sf::IntRect m_visibleRegion;
    sf::IntRect m_regionBound;
    float m_speed;

    void AdjustToRegionBound();
};

}

#endif
