// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#include "ReadableMap.hpp"
#include "../../Utilities/Messager.hpp"
#include "../../Utilities/Helper.hpp"
#include "../../Utilities_SFML/SFMLHelper.hpp"

namespace chalo
{

namespace Map
{

ReadableMap::ReadableMap()
{
    m_mapWidth = 1280;
    m_mapHeight = 720;
    Reset();
}

void ReadableMap::Reset()
{
    m_belowTileLayers.clear();
    m_aboveTileLayers.clear();
    m_collisions.clear();
    m_warps.clear();
    m_shadows.clear();
    m_placements.clear();

    m_mapPath = "";
    m_mapFile = "";
    m_mapWidth = 0;
    m_mapHeight = 0;
    m_currentlyAtWarp = -1;
}

void ReadableMap::LoadTextMap( const std::string& mapPath, const std::string& filename )
{
    Logger::Out( "Load map from: " + mapPath + filename, "ReadableMap::LoadTextMap" );

    Reset();

    std::ifstream input( mapPath + filename );

    Map::Tile loadTile;
    Map::Shadow loadShadow;
    Map::Placement loadPlacement;
    Map::Collision loadCollision;

    // TODO: I can fix this to use polymorphism here.
    Map::TileLayer tileLayer;
    Map::ShadowLayer shadowLayer;
    LayerType layerType;
    std::string placementType;

    int tempX, tempY, tempW, tempH;
    int tempFrameX, tempFrameY, tempFrameW, tempFrameH;
    int layerIndex;

    std::string tileset = "";
    sf::Texture tilesetTexture;

    std::string buffer;
    while ( input >> buffer )
    {
        // Map information
        if ( buffer == "MAP_WIDTH" )
        {
            input >> m_mapWidth;
            if ( m_mapWidth == 0 )
            {
                m_mapWidth = 1280;
            }
        }
        else if ( buffer == "MAP_HEIGHT" )
        {
            input >> m_mapHeight;
            if ( m_mapHeight == 0 )
            {
                m_mapHeight = 720;
            }
        }
        else if ( buffer == "TILESET" )
        {
            input >> tileset;

            // TODO: Allow for setting tileset based on map file OR on args, currently going to overwrite!!
            std::string tilesetBase = Messager::Get( "tileset" );
            tileset = tilesetBase + ".png";
            std::string tilesetShadows = tilesetBase + "-shadows.png";
            std::string tilesetPlacements = tilesetBase + "-placements.png";
            std::string tilesetCollisions = tilesetBase + "-collision.png";

            m_tileset = tilesetBase;

            Logger::Out( "Tilesets: " + tilesetBase + ", " +
                tilesetShadows + ", " +
                tilesetPlacements + ", " +
                tilesetCollisions
                , "ReadableMap::LoadTextMap" );

            chalo::TextureManager::Add( "tileset", "Content/Graphics/Tilesets/" + tileset );
            chalo::TextureManager::Add( "tileset-shadows", "Content/Graphics/Tilesets/" + tilesetShadows );
            chalo::TextureManager::Add( "tileset-placements", "Content/Graphics/Tilesets/" + tilesetPlacements );
            chalo::TextureManager::Add( "tileset-collisions", "Content/Graphics/Tilesets/" + tilesetCollisions );

            loadTile.SetTexture( TextureManager::Get( "tileset" ), 0, 0, 0, 0 );
            loadShadow.SetTexture( TextureManager::Get( "tileset-shadows" ), 0, 0, 0, 0 );
            loadPlacement.SetTexture( TextureManager::Get( "tileset-placements" ), 0, 0, 0, 0 );
            loadCollision.SetTexture( TextureManager::Get( "tileset-collisions" ), 0, 0, 0, 0 );
        }

        // Layer information
        else if ( buffer == "LAYER_END" )
        {
            // Save this layer
            if ( layerType == BELOWTILELAYER )
            {
                m_belowTileLayers[ layerIndex ] = tileLayer;
            }
            else if ( layerType == ABOVETILELAYER )
            {
                m_aboveTileLayers[ layerIndex ] = tileLayer;
            }
            else if ( layerType == SHADOWLAYER )
            {
                m_shadows[ layerIndex ] = shadowLayer;
            }
        }
        else if ( buffer == "LAYER_BEGIN" )
        {
            // Reset layer
            tileLayer.ClearTiles();
            shadowLayer.ClearTiles();
        }
        else if ( buffer == "LAYER_TYPE" )
        {
            int tt;
            input >> tt;
            layerType = LayerType( tt );
        }

        else if ( buffer == "LAYER_INDEX" )
        {
            input >> layerIndex;
        }
        else if ( buffer == "LAYER_NAME" )
        {
            input >> buffer;
            if ( layerType == BELOWTILELAYER || layerType == ABOVETILELAYER )
            {
                tileLayer.SetName( buffer );
            }
            else if ( layerType == SHADOWLAYER )
            {
                shadowLayer.SetName( buffer );
            }
        }
        // Tile information
        else if ( buffer == "TILE_END" )
        {
            // Ignore tiles out of bounds
            if ( tempX < 0 || tempX >= m_mapWidth || tempY < 0 || tempY >= m_mapHeight )
            {
                continue;
            }

            // Save tile information
            if ( layerType == BELOWTILELAYER || layerType == ABOVETILELAYER )
            {
                sf::IntRect frameRect = sf::IntRect( tempFrameX, tempFrameY, tempFrameW, tempFrameH );
                loadTile.SetPosition( tempX, tempY );
                loadTile.SetTextureRegion( frameRect );
                tileLayer.AddTile( loadTile );
            }
            else if ( layerType == SHADOWLAYER )
            {
                sf::IntRect frameRect = sf::IntRect( tempFrameX, tempFrameY, tempFrameW, tempFrameH );
                loadShadow.SetPosition( tempX, tempY );
                loadShadow.SetTextureRegion( frameRect );
                shadowLayer.AddTile( loadShadow );
            }
        }
        else if ( buffer == "TILE_BEGIN" )
        {
            // Clear tile information
            tempX = tempY = tempW = tempH = 0;
            tempFrameX = tempFrameY = tempFrameW = tempFrameH = 0;
        }
        else if ( buffer == "TILE_X" )
        {
            input >> tempX;
        }
        else if ( buffer == "TILE_Y" )
        {
            input >> tempY;
        }
        else if ( buffer == "TILE_WIDTH" )
        {
            input >> tempW;
            m_tileWidth = tempW;
        }
        else if ( buffer == "TILE_HEIGHT" )
        {
            input >> tempH;
        }
        else if ( buffer == "TILE_FRAME_X" )
        {
            input >> tempFrameX;
        }
        else if ( buffer == "TILE_FRAME_Y" )
        {
            input >> tempFrameY;
        }
        else if ( buffer == "TILE_FRAME_WIDTH" )
        {
            input >> tempFrameW;
        }
        else if ( buffer == "TILE_FRAME_HEIGHT" )
        {
            input >> tempFrameH;
        }

        // Placement information
        else if ( buffer == "PLACEMENT_END" )
        {
            sf::IntRect dimensions = sf::IntRect( 0, 0, 16, 16 );
            loadPlacement.SetTextureRegion( dimensions );
            loadPlacement.SetCharacterType( placementType );
            loadPlacement.SetPosition( tempX, tempY );
            m_placements.push_back( loadPlacement );
        }
        else if ( buffer == "PLACEMENT_BEGIN" )
        {
        }
        else if ( buffer == "PLACEMENT_X" )
        {
            input >> tempX;
        }
        else if ( buffer == "PLACEMENT_Y" )
        {
            input >> tempY;
        }
        else if ( buffer == "PLACEMENT_TYPE" )
        {
            input >> placementType;
        }

        // Collision information
        else if ( buffer == "COLLISION_END" )
        {
            sf::IntRect rect = sf::IntRect( 0, 0, 16, 16 );
            loadCollision.SetPosition( tempX, tempY );
            sf::IntRect dimensions = sf::IntRect( 0, 0, 16, 16 );
            loadCollision.SetTextureRegion( dimensions );
            loadCollision.SetObjectCollisionRectangle( rect );
            m_collisions.push_back( loadCollision );
        }
        else if ( buffer == "COLLISION_BEGIN" )
        {
        }
        else if ( buffer == "COLLISION_X" )
        {
            input >> tempX;
        }
        else if ( buffer == "COLLISION_Y" )
        {
            input >> tempY;
        }
        else if ( buffer == "COLLISION_TYPE" )
        {
        }
    }

    input.close();
}

void ReadableMap::Update()
{
    for ( auto& layer : m_belowTileLayers )
    {
        for ( auto& tile : layer.second.GetTilesUpdatable() )
        {
            tile.Update();
        }
    }

    for ( auto& layer : m_aboveTileLayers )
    {
        for ( auto& tile : layer.second.GetTilesUpdatable() )
        {
            tile.Update();
        }
    }

    for ( auto& layer : m_shadows )
    {
        for ( auto& tile : layer.second.GetTilesUpdatable() )
        {
            tile.Update();
        }
    }

    for ( auto& item : m_placements )
    {
        item.Update();
    }

    for ( auto& item : m_collisions )
    {
        item.Update();
    }

    for ( auto& item : m_warps )
    {
//        item.Update();
    }
}

const std::map<int, Map::TileLayer>& ReadableMap::GetBelowTileLayers() const
{
    return m_belowTileLayers;
}

const std::map<int, Map::TileLayer>& ReadableMap::GetAboveTileLayers() const
{
    return m_aboveTileLayers;
}

const std::map<int, Map::ShadowLayer>& ReadableMap::GetShadows() const
{
    return m_shadows;
}

const std::vector<Map::Collision>& ReadableMap::GetCollisions() const
{
    return m_collisions;
}

const std::vector<Map::Warp>& ReadableMap::GetWarps() const
{
    return m_warps;
}

const std::vector<Map::Placement>& ReadableMap::GetPlacements() const
{
    return m_placements;
}

/**
   The map will check to see if there is a collision
   between a "collision tile" and the position passed in.
*/
bool ReadableMap::IsCollision( sf::IntRect desiredPosition, sf::IntRect collisionRegion ) const
{
//    Logger::Out( "..." );
//    Logger::Out( "Player position rect: " + Helper::RectangleToString( desiredPosition ) );
//    Logger::Out( "Player collision rect: " + Helper::RectangleToString( collisionRegion ) );
    desiredPosition.left += collisionRegion.left;
    desiredPosition.top += collisionRegion.top;
    desiredPosition.width = collisionRegion.width;
    desiredPosition.height = collisionRegion.height;

//    Logger::Out( "Adjusted position rect: " + Helper::RectangleToString( desiredPosition ) );

    // Bounding-box collision detection
    // Check for EVERY collision tile
    for ( auto& tile : m_collisions )
    {
        sf::IntRect tileRegion = tile.GetObjectCollisionRegion();

        if ( SFMLHelper::GetDistance( desiredPosition, tileRegion, true ) > 100 )
        {
            continue;
        }

        if ( SFMLHelper::BoundingBoxCollision( desiredPosition, tileRegion ) )
        {
            Logger::OutHighlight( " ... Tile rect: " + SFMLHelper::RectangleToString( tileRegion ),
                                  "ReadableMap::IsCollision"
                                );
            return true;
        }
    }

    // We can only definitively return false if we've checked all tiles.
    // TODO: We can actually simplify this and mathematically only check tiles near the player
    // especially if we're storing the tiles in a 2D array-like structure.
    return false;
}

/**
   Check to see if the player is at a warp point, and return the warp information
*/

bool ReadableMap::PlayerAtWarp( sf::IntRect collisionRegion )
{
    for ( unsigned int i = 0; i < m_warps.size(); i++ )
    {
        if ( SFMLHelper::BoundingBoxCollision( collisionRegion, m_warps[i].GetRegion() ) )
        {
            m_currentlyAtWarp = i;
            return true;
        }
    }
    return false;
}

Map::Warp ReadableMap::GetWarpTo( sf::IntRect collisionRegion ) const
{
    return m_warps[ m_currentlyAtWarp ];
}

sf::Vector2f ReadableMap::GetPlayerStartingPosition() const
{
    return m_playerStartingPosition;
}

std::string ReadableMap::GetTilesetName() const
{
    return m_tileset;
}

int ReadableMap::GetWidth() const
{
    return m_mapWidth;
}

int ReadableMap::GetHeight() const
{
    return m_mapHeight;
}

int ReadableMap::GetTileWidth() const
{
    return m_tileWidth;
}


}


}
