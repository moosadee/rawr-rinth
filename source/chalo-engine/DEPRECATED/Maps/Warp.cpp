// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#include "Warp.hpp"

namespace chalo
{

namespace Map
{

void Warp::Setup( const std::string& warpToMap, sf::Vector2f warpToPosition, sf::IntRect warpRectangle )
{
    m_warpToMap = warpToMap;
    m_warpToPosition = warpToPosition;
    m_warpRectangle = warpRectangle;
}

void Warp::DrawDebug()
{
    sf::RectangleShape warpRect;
    warpRect.setPosition( m_warpRectangle.left, m_warpRectangle.top );
    warpRect.setSize( sf::Vector2f( m_warpRectangle.width, m_warpRectangle.height ) );
    warpRect.setOutlineColor( sf::Color::Green );
    warpRect.setOutlineThickness( 2 );
    warpRect.setFillColor( sf::Color::Transparent );

//  DrawManager::AddDebugRectangle( warpRect );
}

sf::IntRect Warp::GetRegion() const
{
    return m_warpRectangle;
}

std::string Warp::GetWarpToMap() const
{
    return m_warpToMap;
}

sf::Vector2f Warp::GetWarpToPosition() const
{
    return m_warpToPosition;
}

}

}
