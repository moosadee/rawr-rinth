// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef _PLACEMENT_HPP
#define _PLACEMENT_HPP

#include <string>
#include <SFML/Graphics.hpp>

#include "../GameObjects/GameObject.hpp"

namespace chalo
{

namespace Map
{

//! Indicates a placement of an NPC (or item) on a map
class Placement : public GameObject
{
public:
    std::string GetCharacterType()
    {
        return m_characterType;
    }

    void SetCharacterType( std::string type )
    {
        m_characterType = type;
        int widthHeight = 16;

        sf::IntRect imageClip = sf::IntRect( 0, 0, widthHeight, widthHeight );

        // TODO: Bleh!
        if ( type == "ayda" )
        {
            imageClip = sf::IntRect( 0, 0, widthHeight, widthHeight );
            SetTextureRegion( imageClip );
        }
        else if ( type == "goblin" )
        {
            imageClip = sf::IntRect( 16, 0, widthHeight, widthHeight );
            SetTextureRegion( imageClip );
        }
        else if ( type == "skeleton" )
        {
            imageClip = sf::IntRect( 32, 0, widthHeight, widthHeight );
            SetTextureRegion( imageClip );
        }
        else if ( type == "snake" )
        {
            imageClip = sf::IntRect( 48, 0, widthHeight, widthHeight );
            SetTextureRegion( imageClip );
        }
        else if ( type == "spider" )
        {
            imageClip = sf::IntRect( 16, 16, widthHeight, widthHeight );
            SetTextureRegion( imageClip );
        }
        else if ( type == "twohead" )
        {
            imageClip = sf::IntRect( 32, 16, widthHeight, widthHeight );
            SetTextureRegion( imageClip );
        }
        else if ( type == "troll" )
        {
            imageClip = sf::IntRect( 48, 16, widthHeight, widthHeight );
            SetTextureRegion( imageClip );
        }
        else if ( type == "dragon" )
        {
            imageClip = sf::IntRect( 64, 16, widthHeight, widthHeight );
            SetTextureRegion( imageClip );
        }
        else if ( type == "cyclops" )
        {
            imageClip = sf::IntRect( 16, 32, widthHeight, widthHeight );
            SetTextureRegion( imageClip );
        }
        else if ( type == "ghost" )
        {
            imageClip = sf::IntRect( 32, 32, widthHeight, widthHeight );
            SetTextureRegion( imageClip );
        }
        else if ( type == "demon" )
        {
            imageClip = sf::IntRect( 48, 32, widthHeight, widthHeight );
            SetTextureRegion( imageClip );
        }
        else if ( type == "bat" )
        {
            imageClip = sf::IntRect( 64, 32, widthHeight, widthHeight );
            SetTextureRegion( imageClip );
        }
    }

protected:
    std::string m_characterType;
};

}

}

#endif
