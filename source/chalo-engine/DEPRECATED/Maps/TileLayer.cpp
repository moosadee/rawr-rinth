// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#include "TileLayer.hpp"

namespace chalo
{

namespace Map
{

ILayer::ILayer()
{
    m_name = "unnamed";
    m_isHidden = false;
}

ILayer::ILayer( const std::string& name )
{
    m_name = name;
    m_isHidden = false;
}

const std::string& ILayer::GetName() const
{
    return m_name;
}

void ILayer::SetName( const std::string& name )
{
    m_name = name;
}

bool ILayer::GetIsHidden() const
{
    return m_isHidden;
}

void ILayer::SetIsHidden( bool value )
{
    m_isHidden = value;
}

void ILayer::SetType( LayerType type )
{
    m_type = type;
}

LayerType ILayer::GetType() const
{
    return m_type;
}




// TODO: I mean this needs to be fixed up. Just ignore this shit for now plz.

TileLayer::TileLayer() : ILayer()
{
}

TileLayer::TileLayer( const std::string& name ) : ILayer( name )
{
}

const std::vector<Tile>& TileLayer::GetTiles() const
{
    return m_tiles;
}

std::vector<Tile>& TileLayer::GetTilesUpdatable()
{
    return m_tiles;
}

std::vector<Tile>* TileLayer::GetTilesUpdatablePtr()
{
    return &m_tiles;
}

void TileLayer::SetTileTexture( int index, sf::IntRect textureRegion )
{
    m_tiles[ index ].SetTextureRegion( textureRegion );
}

void TileLayer::ClearTiles()
{
    m_tiles.clear();
}

void TileLayer::RemoveTiles( std::list<int>& tileIndices )
{
    for ( auto& index : tileIndices )
    {
        m_tiles.erase( m_tiles.begin() + index );
    }
}

void TileLayer::AddTile( Tile tile )
{
    m_tiles.push_back( tile );
}

void TileLayer::UndoTile()
{
    m_tiles.pop_back();
}

void TileLayer::Reset()
{
    m_tiles.clear();
}

int TileLayer::GetTileCount()
{
    return m_tiles.size();
}

std::string TileLayer::GetLogString()
{
    return "TileLayer, layer type: " + Helper::ToString( GetType() ) +
           "... Layer name: " + GetName() +
           "... Total tiles: " + Helper::ToString( GetTileCount() );
}







ShadowLayer::ShadowLayer() : ILayer()
{
}

ShadowLayer::ShadowLayer( const std::string& name ) : ILayer( name )
{
}

const std::vector<Shadow>& ShadowLayer::GetTiles() const
{
    return m_tiles;
}

std::vector<Shadow>& ShadowLayer::GetTilesUpdatable()
{
    return m_tiles;
}

std::vector<Shadow>* ShadowLayer::GetTilesUpdatablePtr()
{
    return &m_tiles;
}

void ShadowLayer::SetTileTexture( int index, sf::IntRect textureRegion )
{
    m_tiles[ index ].SetTextureRegion( textureRegion );
}

void ShadowLayer::ClearTiles()
{
    m_tiles.clear();
}

void ShadowLayer::RemoveTiles( std::list<int>& tileIndices )
{
    for ( auto& index : tileIndices )
    {
        m_tiles.erase( m_tiles.begin() + index );
    }
}

void ShadowLayer::AddTile( Shadow tile )
{
    m_tiles.push_back( tile );
}

void ShadowLayer::UndoTile()
{
    m_tiles.pop_back();
}

void ShadowLayer::Reset()
{
    m_tiles.clear();
}

int ShadowLayer::GetTileCount()
{
    return m_tiles.size();
}

std::string ShadowLayer::GetLogString()
{
    std::string log;
    log = "ShadowLayer, layer type: " + Helper::ToString( GetType() ) +
          "... Layer name: " + GetName() +
          "... Total tiles: " + Helper::ToString( GetTileCount() );

    return log;
}





}

}
