// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#include "DrawManager.hpp"

#include "../../Utilities/Helper.hpp"

namespace chalo
{

std::map<int, DrawLayer> DrawManager::m_objectLayers;
std::map<int, DrawLayer> DrawManager::m_belowLayers;
std::map<int, DrawLayer> DrawManager::m_aboveLayers;

std::map<int, DrawLayer> DrawManager::m_hudSprites;
std::vector<sf::Text> DrawManager::m_hudText;

std::vector<UIImage> DrawManager::m_uiImages;
std::vector<UILabel> DrawManager::m_uiLabels;
std::vector<UIButton> DrawManager::m_uiButtons;
std::vector<UIRectangleShape> DrawManager::m_uiRectangles;
std::vector<UITextBox> DrawManager::m_uiTextBoxes;

std::vector<sf::Sprite> DrawManager::m_sprites;
std::vector<sf::Sprite> DrawManager::m_cursor;

std::vector<sf::Text> DrawManager::m_debugText;
std::vector<sf::RectangleShape> DrawManager::m_debugRectangles;
bool DrawManager::m_debugView;
sf::Color DrawManager::m_backgroundColor;

void DrawManager::Setup()
{
    m_debugView = false;
    m_backgroundColor = sf::Color( 30, 0, 52 );
}

void DrawManager::SetDebugView( bool value )
{
    m_debugView = value;
}

bool DrawManager::GetDebugView()
{
    return m_debugView;
}

void DrawManager::ToggleDebugView()
{
    m_debugView = !(m_debugView);
}

void DrawManager::Reset()
{
    // Reset the objects to be drawn.
    m_belowLayers.clear();
    m_aboveLayers.clear();
    m_objectLayers.clear();

    m_hudSprites.clear();
    m_hudText.clear();

    m_uiImages.clear();
    m_uiLabels.clear();
    m_uiButtons.clear();
    m_uiRectangles.clear();
    m_uiTextBoxes.clear();

    m_debugText.clear();
    m_debugRectangles.clear();

    m_sprites.clear();
    m_cursor.clear();
}

void DrawManager::AddGameObject( const GameObject& object )
{
    if ( object.GetActiveState() == WAITING_DELETION )
    {
        // Don't draw.
        return;
    }
    int yPosition = object.GetPosition().y;

    // Create the draw layer if there is not yet one for this yPosition.
    if ( m_objectLayers.find( yPosition ) == m_objectLayers.end() )
    {
        m_objectLayers[yPosition] = DrawLayer( "ObjectLayer" + Helper::ToString( yPosition ) );
    }

    m_objectLayers[yPosition].images.push_back( object.GetSprite() );

    if ( m_debugView )
    {
        sf::RectangleShape shape;
        sf::IntRect rect = object.GetMapCollisionRegion();
        shape.setPosition( rect.left, rect.top );
        shape.setSize( sf::Vector2f( rect.width, rect.height ) );
        shape.setFillColor( sf::Color( 255, 255, 0, 100 ) );
        shape.setOutlineColor( sf::Color( 255, 255, 0 ) );
        shape.setOutlineThickness( 1 );

        DrawManager::AddDebugRectangle( shape );

        rect = object.GetObjectCollisionRegion();
        shape.setPosition( rect.left, rect.top );
        shape.setSize( sf::Vector2f( rect.width, rect.height ) );
        shape.setFillColor( sf::Color::Transparent );
        shape.setOutlineColor( sf::Color( 100, 50, 0 ) );
        shape.setOutlineThickness( 1 );

        DrawManager::AddDebugRectangle( shape );

        sf::Text text;
        text.setFont( FontManager::Get( "debug" ) );
        text.setString(
            Helper::ToString( rect.left ) + "," +
            Helper::ToString( rect.top )
        );
        text.setFillColor( sf::Color::White );
        text.setCharacterSize( 10 );
        text.setPosition( rect.left, rect.top );

        DrawManager::AddDebugText( text );

        text.setFont( FontManager::Get( "debug" ) );
        text.setString(
            Helper::ToString( rect.width ) + "x" +
            Helper::ToString( rect.height )
        );
        text.setFillColor( sf::Color::White );
        text.setCharacterSize( 10 );
        text.setPosition( rect.left, rect.top + 10 );

        DrawManager::AddDebugText( text );
    }
}

void DrawManager::AddMenu()
{
    // Shapes
    for ( auto& layer : MenuManager::m_shapeLayers )
    {
        for ( auto& element : layer.second )
        {
            AddUIRectangle( element.second );
        }
    }

    // Buttons
    for ( auto& layer : MenuManager::m_buttonLayers )
    {
        for ( auto& element : layer.second )
        {
            if ( element.second.GetIsVisible() ) {
                AddUIButton( element.second );
            }
        }
    }

    // Images
    for ( auto& layer : MenuManager::m_imageLayers )
    {
        for ( auto& element : layer.second )
        {
            if ( element.second.GetIsVisible() ) {
                AddUIImage( element.second );
            }
        }
    }

    // Labels
    for ( auto& layer : MenuManager::m_labelLayers )
    {
        for ( auto& element : layer.second )
        {
            if ( element.second.GetIsVisible() ) {
                AddUILabel( element.second );
            }
        }
    }

    // Textboxes
    for ( auto& layer : MenuManager::m_textboxLayers )
    {
        for ( auto& element : layer.second )
        {
            AddUITextBox( element.second );
        }
    }
}

void DrawManager::AddMap( const Map::ReadableMap& map, int currentLayer /* = -1 */, LayerType layerType /* = UNDEFINEDLAYER */ )
{
    // Below tile layer
    for ( auto& layer : map.GetBelowTileLayers() )
    {
        for ( auto& tile : layer.second.GetTiles() )
        {
            AddBelowMapSprite( tile );


            // Extra: Highlight tiles if they're on the currently-editing layer, and shade if not.
            if ( currentLayer == layer.first && layerType == BELOWTILELAYER )
            {
                // Add shading
                sf::Vector2f pos = tile.GetPosition();
                sf::Vector2f dim = tile.GetDimensions();
                sf::RectangleShape dot;
                dot.setPosition( sf::Vector2f( pos.x + dim.x/2, pos.y + dim.y/2 ) );
                dot.setSize( sf::Vector2f( 1, 1 ) );
                dot.setFillColor( sf::Color( 255, 255, 255, 200 ) );
                dot.setOutlineThickness( 0 );
                //dot.setOutlineColor( sf::Color( 0, 0, 0 ) );

                AddDebugRectangle( dot );
            }
            else if ( currentLayer != layer.first && layerType == BELOWTILELAYER )
            {
                // Add shading
                sf::RectangleShape dot;
                dot.setPosition( tile.GetPosition() );
                dot.setSize( tile.GetDimensions() );
                dot.setFillColor( sf::Color( 0, 0, 0, 50 ) );
                dot.setOutlineThickness( 0 );
                //dot.setOutlineColor( sf::Color( 0, 0, 0 ) );

                AddDebugRectangle( dot );
            }
        }
    }

    // Placements
    for ( auto& item : map.GetPlacements() )
    {
        AddGameObject( item );
    }

    // Above tile layer
    for ( auto& layer : map.GetAboveTileLayers() )
    {
        for ( auto& tile : layer.second.GetTiles() )
        {
            AddAboveMapSprite( tile );

            if ( currentLayer == layer.first && layerType == ABOVETILELAYER )
            {
                // Add shading
                sf::Vector2f pos = tile.GetPosition();
                sf::Vector2f dim = tile.GetDimensions();
                sf::RectangleShape dot;
                dot.setPosition( sf::Vector2f( pos.x + dim.x/2, pos.y + dim.y/2 ) );
                dot.setSize( sf::Vector2f( 1, 1 ) );
                dot.setFillColor( sf::Color( 255, 255, 255, 200 ) );
                dot.setOutlineThickness( 0 );
                //dot.setOutlineColor( sf::Color( 0, 0, 0 ) );

                AddDebugRectangle( dot );
            }
            else if ( currentLayer != layer.first && layerType == ABOVETILELAYER )
            {
                // Add shading
                sf::RectangleShape dot;
                dot.setPosition( tile.GetPosition() );
                dot.setSize( tile.GetDimensions() );
                dot.setFillColor( sf::Color( 0, 0, 0, 50 ) );
                dot.setOutlineThickness( 0 );
                //dot.setOutlineColor( sf::Color( 0, 0, 0 ) );

                AddDebugRectangle( dot );
            }
        }
    }

    // Shadow layer
    for ( auto& layer : map.GetShadows() )
    {
        for ( auto& tile : layer.second.GetTiles() )
        {
          AddBelowMapSprite( tile );
        }
    }

    for ( auto& tile : map.GetCollisions() )
    {
        sf::IntRect tileRect = tile.GetObjectCollisionRegion();
        sf::RectangleShape tileRegion;

        tileRegion.setPosition( tileRect.left, tileRect.top );
        tileRegion.setSize( sf::Vector2f( tileRect.width, tileRect.height ) );
        tileRegion.setFillColor( sf::Color( 255, 0, 0, 100 ) );
        tileRegion.setOutlineColor( sf::Color::Red );
        tileRegion.setOutlineThickness( 1 );

        DrawManager::AddDebugRectangle( tileRegion );
    }

    if ( m_debugView )
    {

        for ( auto& warp : map.GetWarps() )
        {
            sf::RectangleShape warpRegion;
            sf::IntRect warpRect = warp.GetRegion();
            warpRegion.setPosition( warpRect.left, warpRect.top );
            warpRegion.setSize( sf::Vector2f( warpRect.width, warpRect.height ) );
            warpRegion.setFillColor( sf::Color( 0, 255, 0, 100 ) );
            warpRegion.setOutlineColor( sf::Color::Green );
            warpRegion.setOutlineThickness( 1 );

            DrawManager::AddDebugRectangle( warpRegion );

            sf::Text text;
            text.setFont( FontManager::Get( "debug" ) );
            text.setString(
                Helper::ToString( warpRect.left ) + "," +
                Helper::ToString( warpRect.top )
            );
            text.setFillColor( sf::Color::White );
            text.setCharacterSize( 10 );
            text.setPosition( warpRect.left, warpRect.top );

            DrawManager::AddDebugText( text );

            text.setFont( FontManager::Get( "debug" ) );
            text.setString(
                Helper::ToString( warpRect.width ) + "x" +
                Helper::ToString( warpRect.height )
            );
            text.setFillColor( sf::Color::White );
            text.setCharacterSize( 10 );
            text.setPosition( warpRect.left, warpRect.top + 10 );

            DrawManager::AddDebugText( text );
        }
    }
}

void DrawManager::AddBelowMapSprite( const Map::Tile& tile )
{
    if ( m_belowLayers.find( 0 ) == m_belowLayers.end() )
    {
        // Create this key in the map.
        m_belowLayers[0] = DrawLayer( "Layer0" );
        m_belowLayers[0].name = "Layer0";
    }

    m_belowLayers[0].images.push_back( tile.GetSprite() );
}

void DrawManager::AddAboveMapSprite( const Map::Tile& tile )
{
    if ( m_aboveLayers.find( 0 ) == m_aboveLayers.end() )
    {
        // Create this key in the map.
        m_aboveLayers[0] = DrawLayer( "Layer0" );
        m_aboveLayers[0].name = "Layer0";
    }

    m_aboveLayers[0].images.push_back( tile.GetSprite() );
}

void DrawManager::AddHudImage( int layer, const sf::Sprite& sprite )
{
    if ( m_hudSprites.find( layer ) == m_hudSprites.end() )
    {
        m_hudSprites[layer] = DrawLayer( "UIImageLayer" + Helper::ToString( layer ) );
    }

    m_hudSprites[layer].images.push_back( sprite );
}

void DrawManager::AddHudText( const sf::Text& text )
{
    m_hudText.push_back( text );
}

// UI

void DrawManager::AddUILabel( const UILabel& label )
{
    if ( label.GetIsVisible() )
    {
        m_uiLabels.push_back( label );
    }
}

void DrawManager::AddUIImage( const UIImage& image )
{
    if ( image.GetIsVisible() )
    {
        m_uiImages.push_back( image );
    }
}

void DrawManager::AddUIButton( const UIButton& button )
{
    if ( button.GetIsVisible() )
    {
        m_uiButtons.push_back( button );
    }
}

void DrawManager::AddUIRectangle( const UIRectangleShape& shape )
{
    if ( shape.GetIsVisible() )
    {
        m_uiRectangles.push_back( shape );
    }
}

void DrawManager::AddUITextBox( const UITextBox& textbox )
{
    if ( textbox.GetIsVisible() )
    {
        m_uiTextBoxes.push_back( textbox );
    }
}

void DrawManager::AddSprite( const sf::Sprite& sprite )
{
    m_sprites.push_back( sprite );
}

void DrawManager::AddCursor( const sf::Sprite& sprite )
{
    m_cursor.push_back( sprite );
}

void DrawManager::AddDebugText( const sf::Text& text )
{
    m_debugText.push_back( text );
}

void DrawManager::AddDebugRectangle( const sf::RectangleShape& shape )
{
    m_debugRectangles.push_back( shape );
}

void DrawManager::SetBackgroundColor( sf::Color color )
{
    m_backgroundColor = color;
}

sf::Color DrawManager::GetBackgroundColor()
{
    return m_backgroundColor;
}

void DrawManager::Draw( sf::RenderWindow& window )
{
    // Draw the ground bottom first
    for ( auto& layer : m_belowLayers )
    {
        for ( auto& image : layer.second.images )
        {
            window.draw( image );
        }
    }

    // Draw objects
    for ( auto& layer : m_objectLayers )
    {
        for ( auto& image : layer.second.images )
        {
            window.draw( image );
        }
    }

    // Draw above tiles
    for ( auto& layer : m_aboveLayers )
    {
        for ( auto& image : layer.second.images )
        {
            window.draw( image );
        }
    }

    // Draw HUD images
    for ( auto& layer : m_hudSprites )
    {
        for ( auto& image : layer.second.images )
        {
            window.draw( image );
        }
    }

    // Draw misc items
    for ( auto& sprite : m_sprites )
    {
        window.draw( sprite );
    }

    // Draw HUD text
    for ( auto& text : m_hudText )
    {
        window.draw( text );
    }

    // Draw UI stuff
    for ( auto& shape : m_uiRectangles )
    {
        window.draw( shape.GetShape() );
    }

    for ( auto& image : m_uiImages )
    {
        window.draw( image.GetSprite() );
    }

    for ( auto& button : m_uiButtons )
    {
        window.draw( button.GetBackground().GetSprite() );
        window.draw( button.GetIcon().GetSprite() );
        window.draw( button.GetLabel().GetText() );
    }

    for ( auto& label : m_uiLabels )
    {
        window.draw( label.GetText() );
    }

    for ( auto& textbox : m_uiTextBoxes )
    {
        window.draw( textbox.GetBackground().GetShape() );
        window.draw( textbox.GetCursor().GetShape() );
        window.draw( textbox.GetLabel().GetText() );
    }

    // Draw debug text
    for ( auto& text : m_debugText )
    {
        window.draw( text );
    }

    // Draw debug shapes
    for ( auto& shape : m_debugRectangles )
    {
        window.draw( shape );
    }

    // Draw debug shapes
    for ( auto& cursor : m_cursor )
    {
        window.draw( cursor );
    }
}

}
