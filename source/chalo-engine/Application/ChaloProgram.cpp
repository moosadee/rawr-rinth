// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine
#include "ChaloProgram.hpp"

#include "../Utilities/Logger.hpp"
#include "../Utilities/Helper.hpp"
//#include "../Utilities/Messager.hpp"
#include "../Application/Application.hpp"
#include "../Managers/StateManager.hpp"
#include "../Managers/MenuManager.hpp"
#include "../Managers/ConfigManager.hpp"

/**
Option keys:
- CONFIG_NAME
- TITLEBAR_TEXT
- WINDOW_WIDTH
- WINDOW_HEIGHT
- MENU_PATH
- FULLSCREEN (1/0)
*/
#include "ChaloProgram.hpp"

namespace chalo
{

std::string ChaloProgram::s_className = "ChaloProgram";

void ChaloProgram::Setup( std::map<std::string,std::string>& options )
{
    Logger::Setup();
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Chalo program begins", s_className + "::" + __func__ );

    std::string configName = "config.chaloconfig";
    std::string titlebarText = "Chalo project";
    std::string menuPath = "Content/Menus/";
    std::string platform = "";
    int windowWidth = 400;
    int windowHeight = 240;
    int gameWidth = 400;
    int gameHeight = 240;
    bool fullscreen = false;

    std::string optionsString = "Options passed to setup:\n<br>";
    optionsString += "<table><tr><th>key</th><th>value</th></tr>";
    for ( auto el : options ) {
        optionsString += "\n<tr><td>\"" + el.first + "\"</td><td>\"" + el.second + "\"</td></tr>";
    }
    optionsString += "</table>";
    Logger::Out( optionsString, s_className + "::" + __func__ );

    if ( options.find( "CONFIG_NAME" ) != options.end() )       { configName = options["CONFIG_NAME"]; }
    if ( options.find( "TITLEBAR_TEXT" ) != options.end() )     { titlebarText = options["TITLEBAR_TEXT"]; }
    if ( options.find( "MENU_PATH" ) != options.end() )         { menuPath = options["MENU_PATH"]; }
    if ( options.find( "WINDOW_WIDTH" ) != options.end() )      { windowWidth = Helper::StringToInt( options["WINDOW_WIDTH"] ); }
    if ( options.find( "WINDOW_HEIGHT" ) != options.end() )     { windowHeight = Helper::StringToInt( options["WINDOW_HEIGHT"] ); }
    if ( options.find( "GAME_WIDTH" ) != options.end() )        { gameWidth = Helper::StringToInt( options["GAME_WIDTH"] ); }
    if ( options.find( "GAME_HEIGHT" ) != options.end() )       { gameHeight = Helper::StringToInt( options["GAME_HEIGHT"] ); }
    if ( options.find( "FULLSCREEN" ) != options.end() )        { fullscreen = Helper::StringToInt( options["FULLSCREEN"] ); }
    if ( options.find( "PLATFORM" ) != options.end() )          { platform = options["PLATFORM"]; }

//    chalo::ConfigManager::Setup( configName );
    chalo::Application::Setup( titlebarText, gameWidth, gameHeight, windowWidth, windowHeight, fullscreen, platform );

    chalo::MenuManager::Setup( menuPath );
    Logger::StackPop();
}

void ChaloProgram::Teardown()
{
    chalo::Application::Teardown();
    Logger::Cleanup();
}

}

