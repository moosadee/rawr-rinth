// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#include "Application.hpp"
#include "../Managers/ConfigManager.hpp"
#include "../Utilities/Logger.hpp"
#include "../Utilities/Helper.hpp"
#include "../Utilities_SFML/SFMLHelper.hpp"

#include <string>

namespace chalo
{

std::string Application::s_className = "Application";
sf::RenderWindow Application::m_window;
sf::RenderTexture Application::m_renderTexture;
sf::Sprite Application::m_renderSprite;
bool Application::m_done;
sf::Vector2f Application::m_gameScreenDimensionsOriginal;
sf::Vector2f Application::m_gameScreenDimensionsScaled;
sf::Vector2f Application::m_windowDimensions;
sf::Vector2f Application::m_scaleRatio;
sf::Vector2f Application::m_positionOffset;
std::vector<sf::Event> Application::m_polledEvents;
std::string Application::m_windowTitle;

void Application::Setup( const std::string& title,
    int gameScreenWidth /* = 400 */,
    int gameScreenHeight /* = 240 */,
    int windowWidth /* = 1280 */,
    int windowHeight /* = 720 */,
    bool fullscreen /* = false */,
    std::string platform /* = "" */ )
{
    Logger::StackPush( s_className + "::" + __func__ );

    m_gameScreenDimensionsOriginal.x = gameScreenWidth;
    m_gameScreenDimensionsOriginal.y = gameScreenHeight;

    m_windowDimensions.x = windowWidth;
    m_windowDimensions.y = windowHeight;

    m_windowTitle = title;

    if ( fullscreen )
    {
        m_windowDimensions.x = sf::VideoMode::getDesktopMode().width;
        m_windowDimensions.y = sf::VideoMode::getDesktopMode().height;

        m_window.create(
            sf::VideoMode( m_windowDimensions.x, m_windowDimensions.y ),
            m_windowTitle,
            sf::Style::Fullscreen
        );
    }
    else if ( platform == "PLAYDATE" )
    {
        m_window.create(
            sf::VideoMode( m_windowDimensions.x, m_windowDimensions.y ),
            m_windowTitle,
            sf::Style::None
        );
        m_window.setPosition( sf::Vector2i( 0, 0 ) );
    }
    else
    {
        m_window.create(
            sf::VideoMode( m_windowDimensions.x, m_windowDimensions.y ),
            m_windowTitle
        );
    }

    AdjustRenderSprite();

    m_done = false;
    m_window.setFramerateLimit( 60 );

//    DrawManager::Setup();
    Logger::StackPop();
}

void Application::Teardown()
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::StackPop();
}

void Application::ChangeDisplay( bool fullscreen, int windowWidth /*= 1280*/, int windowHeight /*= 720*/ )
{
    if ( fullscreen )
    {
        m_windowDimensions.x = sf::VideoMode::getDesktopMode().width;
        m_windowDimensions.y = sf::VideoMode::getDesktopMode().height;

        m_window.create(
            sf::VideoMode( m_windowDimensions.x, m_windowDimensions.y ),
            m_windowTitle,
            sf::Style::Fullscreen
        );
    }
    else
    {
        m_windowDimensions.x = windowWidth;
        m_windowDimensions.y = windowHeight;

        m_window.create(
            sf::VideoMode( m_windowDimensions.x, m_windowDimensions.y ),
            m_windowTitle,
            sf::Style::Default
        );
    }

    AdjustRenderSprite();
}

void Application::AdjustRenderSprite()
{
    // Intended render size
    m_renderTexture.create( m_gameScreenDimensionsOriginal.x, m_gameScreenDimensionsOriginal.y );

    // Get ratio of screen size vs. game size
    m_scaleRatio.y = m_windowDimensions.y / m_gameScreenDimensionsOriginal.y;
    m_scaleRatio.x = m_scaleRatio.y;
    // The screen is flipped when putting it on the render texture, for some reason...
    m_scaleRatio.y *= -1;

    m_renderSprite.setScale( m_scaleRatio );
    m_renderSprite.setOrigin( m_gameScreenDimensionsOriginal.x/2, m_gameScreenDimensionsOriginal.y/2 );
    m_renderSprite.setPosition( m_window.getView().getCenter() );

    m_gameScreenDimensionsScaled.x = m_gameScreenDimensionsOriginal.x * GetScaleRatio().x;
    m_gameScreenDimensionsScaled.y = m_gameScreenDimensionsOriginal.y * GetScaleRatio().y;

    m_positionOffset.x = ( ( m_windowDimensions.x - m_gameScreenDimensionsScaled.x ) / 2 ) / GetScaleRatio().x;
    m_positionOffset.y = ( ( m_windowDimensions.y - m_gameScreenDimensionsScaled.y ) / 2 ) / GetScaleRatio().y;

    Logger::Out( "m_gameScreenDimensionsOriginal: " + SFMLHelper::CoordinateToString(m_gameScreenDimensionsOriginal) + "<br>" +
                "m_gameScreenDimensionsScaled: " + SFMLHelper::CoordinateToString(m_gameScreenDimensionsScaled) + "<br>" +
                "m_windowDimensions: " + SFMLHelper::CoordinateToString(m_windowDimensions) + "<br>" +
                "m_positionOffset: " + SFMLHelper::CoordinateToString(m_positionOffset) + "<br>" +
                "m_scaleRatio: " + SFMLHelper::CoordinateToString(m_scaleRatio) + "<br>"
                , s_className + "::" + __func__ );

}

void Application::BeginDrawing()
{
    m_window.clear( sf::Color( 75, 19, 126 ) );
    m_renderTexture.clear();
}

void Application::EndDrawing()
{
    m_renderSprite.setTexture( m_renderTexture.getTexture() );
//    m_renderSprite.setTextureRect( sf::IntRect( m_gameScreenWidth, 0, -m_gameScreenWidth, m_gameScreenHeight ) );
    m_window.draw( m_renderSprite );
    m_window.display();
}

bool Application::IsRunning()
{
    return ( m_window.isOpen() && !m_done );
}

void Application::ReadyToQuit()
{
    m_done = true;
}

void Application::Update()
{
    CheckWindowEvents();
}

sf::Vector2f Application::GetIntendedScreenDimensions()
{
    return m_gameScreenDimensionsOriginal;
}

sf::Vector2f Application::GetScaledScreenDimensions()
{
    return m_gameScreenDimensionsScaled;
}

sf::Vector2f Application::GetWindowDimensions()
{
    return m_windowDimensions;
}

sf::Vector2f Application::GetScaleRatio()
{
    // Make abs val
    sf::Vector2f absRatio;
    absRatio.x = ( m_scaleRatio.x >= 0 ) ? m_scaleRatio.x : -m_scaleRatio.x;
    absRatio.y = ( m_scaleRatio.y >= 0 ) ? m_scaleRatio.y : -m_scaleRatio.y;
    return absRatio;
}

sf::Vector2f Application::GetViewOffset()
{
    return m_positionOffset;
}

void Application::CheckWindowEvents()
{
    m_polledEvents.clear();
    sf::Event event;
    while ( m_window.pollEvent( event ) )
    {
        m_polledEvents.push_back( event );
        if ( event.type == sf::Event::Closed )
        {
            m_window.close();
        }

        if ( event.type == sf::Event::MouseWheelScrolled )
        {
            //Logger::Out( "Mouse wheel: " + chalo::Helper::FloatToString( event.mouseWheel.delta ) );
        }
    }
}

std::vector<sf::Event>& Application::GetPolledEvents()
{
    return m_polledEvents;
}

sf::RenderWindow& Application::GetWindow()
{
    return m_window;
}

sf::RenderTexture& Application::GetRenderTexture()
{
    return m_renderTexture;
}

}
