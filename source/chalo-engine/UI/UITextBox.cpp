// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#include "UITextBox.hpp"

#include "../Utilities/Logger.hpp"

#include <string>

namespace chalo
{

std::string UITextBox::s_className = "UITextBox";

void UITextBox::Setup( const std::string& key, sf::Vector2f basePosition, sf::IntRect dimensions )
{
    IWidget::Setup( key );
    m_basePosition = basePosition;
    m_dimensions = dimensions;
    m_hasFocus = false;
}

void UITextBox::SetupBackground( sf::Color borderColor, int borderThickness, sf::Color fillColor, sf::Vector2f position, sf::Vector2f size )
{
    m_unfocusedColor = fillColor;
    m_focusedColor = sf::Color::Yellow;
    m_background.Setup( m_id + "bg", borderColor, borderThickness, fillColor, m_basePosition, sf::Vector2f( m_dimensions.width, m_dimensions.height ) );

    m_cursor.Setup( m_id + "cursor", sf::Color::Transparent, 0, fillColor, sf::Vector2f( m_basePosition.x + 1, m_basePosition.y + 1 ), sf::Vector2f( 2, m_dimensions.height - 2 ) );
}

void UITextBox::SetupText( const std::string& fontName, int characterSize, sf::Color fillColor, sf::Vector2f offsetPosition, std::string defaultText /* = "" */ )
{
    offsetPosition.x += m_basePosition.x;
    offsetPosition.y += m_basePosition.y;
    m_enteredText = defaultText;
    m_label.Setup( m_id + "label", fontName, characterSize, fillColor, offsetPosition, defaultText );
}

UIRectangleShape& UITextBox::GetBackground()
{
    return m_background;
}

UIRectangleShape& UITextBox::GetCursor()
{
    return m_cursor;
}

UILabel& UITextBox::GetLabel()
{
    m_label.SetText( m_enteredText );
    return m_label;
}

void UITextBox::AddCharacter( const std::string& characters )
{
    m_enteredText += characters;

    Logger::Out( "Add character \"" + characters + "\", text string is now: \"" + m_enteredText + "\"", s_className + "::" + __func__ );
}

void UITextBox::EraseCharacter()
{
    m_enteredText = m_enteredText.substr( 0, m_enteredText.size() - 1 );
}

void UITextBox::SetFocus( bool value )
{
    m_hasFocus = value;

    if ( m_hasFocus )
    {
        m_background.SetFillColor( m_focusedColor );
        sf::Color cursorColor = sf::Color( 255 - m_focusedColor.r, 255 - m_focusedColor.g, 255 - m_focusedColor.b, 255 );
        m_cursor.SetFillColor( cursorColor );
    }
    else
    {
        m_background.SetFillColor( m_unfocusedColor );
        m_cursor.SetFillColor( m_unfocusedColor );
    }
}

bool UITextBox::HasFocus()
{
    return m_hasFocus;
}

sf::IntRect UITextBox::GetPositionRect()
{
    sf::IntRect adjustedPosition;
    adjustedPosition.left = m_basePosition.x + m_dimensions.left;
    adjustedPosition.top = m_basePosition.y + m_dimensions.top;
    adjustedPosition.width = m_dimensions.width;
    adjustedPosition.height = m_dimensions.height;
    return adjustedPosition;
}

std::string UITextBox::GetEnteredText()
{
    return m_enteredText;
}

}
