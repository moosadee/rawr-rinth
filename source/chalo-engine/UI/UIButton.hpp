// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef _UIBUTTON_HPP
#define _UIBUTTON_HPP

#include <SFML/Graphics.hpp>

#include <functional>

#include "IWidget.hpp"
#include "UIImage.hpp"
#include "UILabel.hpp"

namespace chalo
{

class IState;

class UIButton : public IWidget
{
public:
    UIButton();
    virtual ~UIButton();

    void Setup( const std::string& key, sf::Vector2f basePosition, sf::IntRect dimensions );
    void SetupBackground( const sf::Texture& texture, sf::Vector2f offsetPosition );
    void SetupBackground( const sf::Texture& texture, sf::Vector2f offsetPosition, sf::IntRect frameRect );
    void SetupIcon( const sf::Texture& texture, sf::Vector2f offsetPosition, sf::IntRect imageClip );
    void SetupText( const std::string& fontName, int characterSize, sf::Color fillColor, sf::Vector2f offsetPosition, std::string text );
    void SetupTextW( const std::string& fontName, int characterSize, sf::Color fillColor, sf::Vector2f offsetPosition, std::wstring text );
    void SetPosition( const sf::Vector2f& pos );
    void SetPosition( const sf::IntRect& pos );
    void Move( sf::Vector2f amount );
    void UpdateText( const std::string& newText );

    UIImage& GetBackground();
    UIImage& GetIcon();
    UILabel& GetLabel();

    sf::IntRect GetPositionRect();
    sf::IntRect GetIconImageClipRect();
    sf::IntRect GetFrameRect();

    void SetupBackgroundImageClipRect( sf::IntRect frameRect );
    void SetOriginalFont( const std::string& fontName );
    void ToggleDyslexiaFont();

protected:
    sf::Vector2f m_basePosition;
    sf::Vector2f m_backgroundOffset;
    sf::Vector2f m_iconOffset;
    sf::Vector2f m_textOffset;
    sf::IntRect m_dimensions;
    sf::IntRect m_frameRect;
    UIImage m_background;
    UIImage m_icon;
    UILabel m_label;

private:
    static std::string s_className;
};

}

#endif
