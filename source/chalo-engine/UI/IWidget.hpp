// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef _IWIDGET_HPP
#define _IWIDGET_HPP

#include <SFML/Graphics.hpp>

#include <string>

namespace chalo
{

class IWidget
{
public:
    IWidget()
    {
        m_visible = true;
    }

    void Setup( const std::string& key )
    {
        m_id = key;
    }

    std::string GetId()
    {
        return m_id;
    }


    void SetIsVisible( bool value )
    {
        m_visible = value;
    }

    bool GetIsVisible() const
    {
        return m_visible;
    }

protected:
    std::string m_id;
    bool m_visible;
};

}

#endif
