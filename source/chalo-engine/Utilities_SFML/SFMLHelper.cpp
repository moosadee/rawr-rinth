#include "SFMLHelper.hpp"
#include "../Utilities/Helper.hpp"

std::string SFMLHelper::CoordinateToString( sf::Vector2f coord )
{
    return Helper::CoordinateToString( coord.x, coord.y );
}

std::string SFMLHelper::CoordinateToString( sf::Vector2i coord )
{
    return Helper::CoordinateToString( coord.x, coord.y );
}

std::string SFMLHelper::CoordinateToString( sf::Vector2u coord )
{
    return Helper::CoordinateToString( coord.x, coord.y );
}

std::string SFMLHelper::CoordinateToString( sf::IntRect coord )
{
    return Helper::CoordinateToString( coord.left, coord.top );
}

std::string SFMLHelper::DimensionsToString( sf::IntRect coord )
{
    std::stringstream ss;
    ss << coord.width << "x" << coord.height;
    return ss.str();
}

std::string SFMLHelper::DimensionsToString( sf::Vector2f coord )
{
    std::stringstream ss;
    ss << coord.x << "x" << coord.y;
    return ss.str();
}

std::string SFMLHelper::RectangleToString( sf::IntRect a )
{
    std::stringstream ss;
    ss << "(" << a.left << ", " << a.top << ") to (" << a.left + a.width << ", " << a.top + a.height << ")";
    return ss.str();
}

// TODO: This doesn't work.
std::string SFMLHelper::ColorToString( sf::Color color )
{
    std::stringstream ss;
    ss << "(" << color.r << ", " << color.g << ", " << color.b << ")";
    return ss.str();
}

sf::Vector2f SFMLHelper::GetRandomCoordinate( int minX, int minY, int maxX, int maxY )
{
    sf::Vector2f position;
    position.x = Helper::GetRandom( minX, maxX );
    position.y = Helper::GetRandom( minY, maxY );
    return position;
}

sf::Vector2f SFMLHelper::ConvertVector( sf::Vector2i vec )
{
    return sf::Vector2f( vec.x, vec.y );
}

sf::Vector2i SFMLHelper::ConvertVector( sf::Vector2f vec )
{
    return sf::Vector2i( vec.x, vec.y );
}

// Math stuff
bool SFMLHelper::BoundingBoxCollision( sf::IntRect a, sf::IntRect b )
{
    return Helper::BoundingBoxCollision( a.left, a.top, a.width, a.height, b.left, b.top, b.width, b.height );
}

bool SFMLHelper::PointInBoxCollision( sf::Vector2f point, sf::IntRect box )
{
    return Helper::PointInBoxCollision( point.x, point.y, box.left, box.top, box.width, box.height );
}

bool SFMLHelper::PointInBoxCollision( sf::Vector2i point, sf::IntRect box )
{
    return Helper::PointInBoxCollision( point.x, point.y, box.left, box.top, box.width, box.height );
}

float SFMLHelper::GetDistance( sf::IntRect a, sf::IntRect b, bool fromCenter )
{
    return Helper::GetDistance( a.left, a.top, a.width, a.height, b.left, b.top, b.width, b.height, fromCenter );
}

float SFMLHelper::GetDistance( sf::Vector2f a, sf::Vector2f b )
{
    return Helper::GetDistance( a.x, a.y, b.x, b.y );
}

float SFMLHelper::DotProduct( sf::Vector2f a, sf::Vector2f b )
{
    return Helper::DotProduct( a.x, a.y, b.x, b.y );
}

float SFMLHelper::Length( sf::Vector2f vec )
{
    return Helper::Length( vec.x, vec.y );
}

float SFMLHelper::AngleBetweenTwoPointsRadians( sf::Vector2f a, sf::Vector2f b )
{
    return Helper::AngleBetweenTwoPointsRadians( a.x, a.y, b.x, b.y );
}

float SFMLHelper::AngleBetweenTwoPointsDegrees( sf::Vector2f a, sf::Vector2f b )
{
    return Helper::AngleBetweenTwoPointsDegrees( a.x, a.y, b.x, b.y );
}

