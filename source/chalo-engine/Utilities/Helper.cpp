#include "Helper.hpp"

#include "Logger.hpp"

string Helper::s_className = "Helper";

/***************************************************************/
/* User input functions ****************************************/
/***************************************************************/

bool Helper::s_needCinIgnore = false;

int Helper::GetIntInput()
{
    return StringToInt( GetStringInput() );
}

float Helper::GetFloatInput()
{
    return StringToFloat( GetStringInput() );
}

char Helper::GetCharInput()
{
    string str = GetStringInput();
    return str[0];
}

string Helper::GetStringInput()
{
    s_needCinIgnore = true;
    string strValue;
    cin >> strValue;
    return strValue;
}

string Helper::GetStringLine()
{
    if ( s_needCinIgnore )
    {
        cin.ignore();
    }

    s_needCinIgnore = false;
    string strValue;
    getline( cin, strValue );
    return strValue;
}

int Helper::GetIntInput( int min, int max )
{
    int input = GetIntInput();

    while ( input < min || input > max )
    {
        cout << "Invalid selection. Try again: " << endl;
        input = GetIntInput();
    }

    return input;
}


/***************************************************************/
/* Menu functionality ******************************************/
/***************************************************************/

void Helper::ClearScreen()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "cls" );
    #else
        system( "clear" );
    #endif
}

void Helper::Pause()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "pause" );
    #else
        cout << endl << " Press ENTER to continue..." << endl;
        if ( s_needCinIgnore )
        {
            cin.ignore( std::numeric_limits <std::streamsize> ::max(), '\n' );
        }
        else
        {
            cin.get();
        }
    #endif
}

void Helper::Header( const string& header )
{
    DrawHorizontalBar( 80 );
    string head = "| " + header + " |";
    cout << " " << head << endl << " ";
    DrawHorizontalBar( head.size() );
    cout << endl;
}

void Helper::DrawHorizontalBar( int width, char symbol /*= '-'*/ )
{
    cout << string( width, symbol ) << endl;
}

void Helper::ShowMenu( const vector<string>& options, bool vertical /*= true*/, bool startAtOne /*= true*/ )
{
    for ( size_t i = 0; i < options.size(); i++ )
    {
        int number = ( startAtOne ) ? i+1 : i;
        cout << " " << number << ".";
        if ( vertical ) { cout << "\t"; } else { cout << " "; }
        cout << options[i];
        if ( vertical ) { cout << "\n"; } else { cout << "\t"; }
    }
    cout << endl;
}

int  Helper::ShowMenuGetInt( const vector<string>& options, bool vertical /*= true*/, bool startAtOne /*= true*/ )
{
    ShowMenu( options, vertical, startAtOne );
    int minVal = ( startAtOne ) ? 1 : 0;
    int maxVal = ( startAtOne ) ? options.size() : options.size() - 1;
    int choice = GetIntInput( minVal, maxVal );
    return choice;
}


/***************************************************************/
/* System info *************************************************/
/***************************************************************/

void Helper::PrintPwd()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "echo %cd%" );
    #else
        system( "pwd" );
    #endif
}

void Helper::Sleep( int milliseconds )
{
    this_thread::sleep_for( chrono::milliseconds( milliseconds ) );
}

void Helper::DisplayDirectoryContents( string path )
{
    string command;

    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
    command = "dir ";
    #else
    command = "ls ";
    #endif

    command += path;

    system( command.c_str() );
}

string Helper::GetTime()
{
    // Code here from https://stackoverflow.com/questions/16357999/current-date-and-time-as-string/16358264
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    std::ostringstream oss;
    oss << std::put_time(&tm, "%d-%m-%Y %H-%M-%S");
    auto str = oss.str();
    return str;
}

/***************************************************************/
/* Conversion functionality ************************************/
/***************************************************************/

int      Helper::StringToInt( const string& str )
{
    int value = 0;
    try
    {
        value = stoi( str );
    }
    catch( const std::invalid_argument& ex )
    {
        std::cout << "Cannot convert \"" << str << "\" to int!" << std::endl;
        return -1;
    }

    return value;
}

float    Helper::StringToFloat( const string& str )
{
    float value = 0;
    try
    {
        value = stof( str );
    }
    catch( const std::invalid_argument& ex )
    {
        std::cout << "Cannot convert \"" << str << "\" to float!" << std::endl;
        return -1;
    }

    return value;
}

string   Helper::CurrencyToString( float currency )
{
    stringstream ss;
    ss << "$" << fixed << setprecision(2) << currency;
    return ss.str();
}

string   Helper::ToUpper( const string& val )
{
    string str = "";
    for ( unsigned int i = 0; i < val.size(); i++ )
    {
        str += toupper( val[i] );
    }
    return str;
}

string   Helper::ToLower( const string& val )
{
    string str = "";
    for ( unsigned int i = 0; i < val.size(); i++ )
    {
        str += tolower( val[i] );
    }
    return str;
}

string Helper::CoordinateToString( int x, int y )
{
    return "(" + ToString(x) + "," + ToString(y) + ")";
}

/***************************************************************/
/* String helpers **********************************************/
/***************************************************************/

bool     Helper::Contains( string haystack, string needle, bool caseSensitive /*= true*/ )
{
    string a = haystack;
    string b = needle;

    if ( !caseSensitive )
    {
        a = ToLower( a );
        b = ToLower( b );
    }

    return ( a.find( b ) != string::npos );
}

int      Helper::Find( string haystack, string needle )
{
    return haystack.find( needle );
}

string   Helper::Replace( string fulltext, string findme, string replacewith )
{
    string updated = fulltext;
    size_t index = updated.find( findme );

    while ( index != string::npos )
    {
        // Make the replacement
        updated = updated.replace( index, findme.size(), replacewith );

        // Look for the item again
        index = updated.find( findme );
    }

    return updated;
}

vector<string> Helper::Split( string str, string delim )
{
    vector<string> data;

    int begin = 0;
    int end = 0;

    while ( str.find( delim ) != string::npos )
    {
        end = str.find( delim );

        // Get substring up until delimiter
        int length = end - begin;
        string substring = str.substr( begin, length );
        data.push_back( substring );

        // Remove this chunk of string
        str = str.erase( begin, length+1 );
    }

    // Put last string in structure
    data.push_back( str );

    return data;
}

string Helper::Trim( string original )
{
    Logger::StackPush( s_className + "::" + __func__ );
    // find first non-whitespace character and last non-whitespace character
    int beginning = -1;
    int end = -1;
    for ( size_t i = 0; i < original.size(); i++ )
    {
        if ( original[i] != ' ' )
        {
            if ( beginning == -1 ) { beginning = i; }
            end = i+1;
        }
    }

    if ( beginning == -1 || end == -1 )
    {
        Logger::StackPop();
        return original;
    }

    string trimmed = original.substr( beginning, ( end - beginning ) );

    Logger::StackPop();
    return trimmed;
}

void Helper::Test_Trim()
{
    { // test begin
        string input = " BAT ";
        string expectedOutput = "BAT";
        string actualOutput = Trim( input );

        cout << "Test_Trim() Test 1: input=\"" << input << "\", expectedOutput=\"" << expectedOutput << "\", actualOutput=\"" << actualOutput << "\"... ";
        if ( actualOutput == expectedOutput )   { cout << "PASS" << endl; }
        else                                    { cout << "FAIL" << endl; }
    } // test end

    { // test begin
        string input = "DOG ";
        string expectedOutput = "DOG";
        string actualOutput = Trim( input );

        cout << "Test_Trim() Test 2: input=\"" << input << "\", expectedOutput=\"" << expectedOutput << "\", actualOutput=\"" << actualOutput << "\"... ";
        if ( actualOutput == expectedOutput )   { cout << "PASS" << endl; }
        else                                    { cout << "FAIL" << endl; }
    } // test end

    { // test begin
        string input = " CAT";
        string expectedOutput = "CAT";
        string actualOutput = Trim( input );

        cout << "Test_Trim() Test 3: input=\"" << input << "\", expectedOutput=\"" << expectedOutput << "\", actualOutput=\"" << actualOutput << "\"... ";
        if ( actualOutput == expectedOutput )   { cout << "PASS" << endl; }
        else                                    { cout << "FAIL" << endl; }
    } // test end

    { // test begin
        string input = "RAT";
        string expectedOutput = "RAT";
        string actualOutput = Trim( input );

        cout << "Test_Trim() Test 4: input=\"" << input << "\", expectedOutput=\"" << expectedOutput << "\", actualOutput=\"" << actualOutput << "\"... ";
        if ( actualOutput == expectedOutput )   { cout << "PASS" << endl; }
        else                                    { cout << "FAIL" << endl; }
    } // test end

    { // test begin
        string input = "";
        string expectedOutput = "";
        string actualOutput = Trim( input );

        cout << "Test_Trim() Test 4: input=\"" << input << "\", expectedOutput=\"" << expectedOutput << "\", actualOutput=\"" << actualOutput << "\"... ";
        if ( actualOutput == expectedOutput )   { cout << "PASS" << endl; }
        else                                    { cout << "FAIL" << endl; }
    } // test end
}

// From https://stackoverflow.com/questions/4804298/how-to-convert-wstring-into-string
std::string Helper::WStringToString( std::wstring text )
{
    using convert_type = std::codecvt_utf8<wchar_t>;
    std::wstring_convert<convert_type, wchar_t> converter;

    //use converter (.to_bytes: wstr->str, .from_bytes: str->wstr)
    std::string converted_str = converter.to_bytes( text );

    return converted_str;
}

// From https://stackoverflow.com/questions/2573834/c-convert-string-or-char-to-wstring-or-wchar-t
std::wstring Helper::StringToWString( std::string text )
{
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    std::wstring wide = converter.from_bytes( text );
    return wide;
}


/***************************************************************/
/* Timer functionality *****************************************/
/***************************************************************/

chrono::system_clock::time_point Helper::s_startTime;

void Helper::ClockStart()
{
    s_startTime = chrono::system_clock::now();
}

size_t Helper::GetElapsedSeconds()
{
    chrono::system_clock::time_point current_time = std::chrono::system_clock::now();
    return chrono::duration_cast<std::chrono::seconds>( current_time - s_startTime ).count();
}

size_t Helper::GetElapsedMilliseconds()
{
    chrono::system_clock::time_point current_time = std::chrono::system_clock::now();
    return chrono::duration_cast<std::chrono::milliseconds>( current_time - s_startTime ).count();
}


/***************************************************************/
/* Random functionality ****************************************/

void Helper::SeedRandomNumberGenerator()
{
    srand( time( NULL ) );
}

int  Helper::GetRandom( int min, int max )
{
    return rand() % ( max - min + 1 ) + min;
}


/***************************************************************/
/* Math functionality ******************************************/

bool Helper::BoundingBoxCollision( int obj1x, int obj1y, int obj1width, int obj1height, int obj2x, int obj2y, int obj2width, int obj2height )
{
    int left1 = obj1x;
    int right1 = obj1x + obj1width;
    int top1 = obj1y;
    int bottom1 = obj1y + obj1height;

    int left2 = obj2x;
    int right2 = obj2x + obj1width;
    int top2 = obj2y;
    int bottom2 = obj2y + obj1height;

    if (
        left1 < right2 &&
        right1 > left2 &&
        top1 < bottom2 &&
        bottom1 > top2
    )
    {
        return true;
    }

    return false;
}

bool Helper::PointInBoxCollision( int pointX, int pointY, int boxX, int boxY, int boxWidth, int boxHeight )
{
    return ( boxX <= pointX && pointX <= boxX + boxWidth &&
                boxY <= pointY && pointY <= boxY + boxHeight );
}

float Helper::GetDistance( int left1, int top1, int width1, int height1, int left2, int top2, int width2, int height2, bool fromCenter )
{
    return 0; // TODO: Implement
}

float Helper::GetDistance( int x1, int y1, int x2, int y2 )
{
    return 0; // TODO: Implement
}

float Helper::DotProduct( float x1, float y1, float x2, float y2 )
{
    return 0; // TODO: Implement
}

float Helper::Length( float x, float y )
{
    return 0; // TODO: Implement
}

float Helper::AngleBetweenTwoPointsRadians( float x1, float y1, float x2, float y2 )
{
    return 0; // TODO: Implement
}

float Helper::AngleBetweenTwoPointsDegrees( float x1, float y1, float x2, float y2 )
{
    return 0; // TODO: Implement
}

//float GetDistance( sf::IntRect a, sf::IntRect b, bool fromCenter )
//{
//    int x1 = a.left, y1 = a.top;
//    int x2 = b.left, y2 = b.top;
//
//    if ( fromCenter )
//    {
//        x1 += a.width / 2;
//        y1 += a.height / 2;
//
//        x2 += b.width / 2;
//        y2 += b.height / 2;
//    }
//
//    return sqrt( pow( x2 - x1, 2 ) + pow( y2 - y1, 2 ) );
//}
//
//float GetDistance( sf::Vector2f a, sf::Vector2f b )
//{
//    return sqrt( pow( b.x - a.x, 2 ) + pow( b.y - a.y, 2 ) );
//}
//
//float DotProduct( sf::Vector2f a, sf::Vector2f b )
//{
//    return ( a.x * b.x + a.y * b.y );
//}
//
//float Length( sf::Vector2f vec )
//{
//    return sqrt( vec.x * vec.x + vec.y * vec.y );
//}
//
//float AngleBetweenTwoPointsRadians( sf::Vector2f a, sf::Vector2f b )
//{
//    // Reference: https://www.wikihow.com/Find-the-Angle-Between-Two-Vectors
//    // Note that SFML uses degrees, cmath uses radians.
//
//    float lengthA = Length( a );
//    float lengthB = Length( b );
//    float dotProduct = DotProduct( a, b );
//    float cos = dotProduct / ( lengthA * lengthB );
//    // acos computes between [-1, +1]
//    float angle = acos( cos );
//    return angle;
//}
//
//float AngleBetweenTwoPointsDegrees( sf::Vector2f a, sf::Vector2f b )
//{
//    float radians = AngleBetweenTwoPointsRadians( a, b );
//    return radians * 180 / M_PI;
//}

