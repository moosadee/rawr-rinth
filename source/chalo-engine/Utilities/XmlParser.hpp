#ifndef _XML_PARSER_HPP
#define _XML_PARSER_HPP

#include "Helper.hpp"

#include <string>
#include <fstream>
#include <map>
#include <vector>
#include <stack>
using namespace std;

struct XmlElement
{
    std::map<std::string,std::string> attributes;
    std::vector<std::string> data;
    std::string path;
};

struct XmlDocument
{
    std::vector<XmlElement> elements;
    std::vector<XmlElement> FindElementsWithPath( std::string path );
    std::vector<XmlElement> FindElementsContainingPath( std::string path );
    void Debug();
};

class XmlParser
{
public:
    static XmlDocument Parse( std::string filepath );
    static std::string StackString( std::vector<std::string> pathStack );

private:
    static std::string s_className;
};

#endif
