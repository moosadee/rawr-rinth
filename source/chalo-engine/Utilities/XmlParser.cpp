#include "XmlParser.hpp"

#include "Logger.hpp"
#include <list>

std::string XmlParser::s_className = "XmlParser";

void XmlDocument::Debug()
{
    for ( size_t e = 0; e < elements.size(); e++ )
    {
        std::string elementData = "Element #" + Helper::ToString( e );
        elementData += ", path: " + elements[e].path;
        elementData += "<br>ATTRIBUTES:";
        for ( auto& keyval : elements[e].attributes )
        {
            elementData += "<br>KEY=" + keyval.first + ", VALUE=" + keyval.second;
        }
        elementData += "<br>DATA:";
        for ( auto& d : elements[e].data )
        {
            elementData += "<br>" + d;
        }

        Logger::Debug( elementData, "XmlDocument::Debug" );
    }
}

std::vector<XmlElement> XmlDocument::FindElementsWithPath( std::string path )
{
    std::vector<XmlElement> foundElements;

    for ( auto& e : elements )
    {
        if ( e.path == path )
        {
            foundElements.push_back( e );
        }
    }

    return foundElements;
}

std::vector<XmlElement> XmlDocument::FindElementsContainingPath( std::string path )
{
    std::vector<XmlElement> foundElements;

    for ( auto& e : elements )
    {
        if ( Helper::Contains( e.path, path ) )
        {
            foundElements.push_back( e );
        }
    }

    return foundElements;
}

XmlDocument XmlParser::Parse( string filepath )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Debug( "Parse XML file: \"" + filepath + "\"", s_className + "::" + __func__ );

    ifstream input( filepath );
    if ( input.fail() )
    {
        Logger::Error( "ERROR: Couldn't open \"" + filepath + "\"!", s_className + "::" + __func__ );
        Logger::StackPop();
        throw runtime_error( "File not found!" );
    }

    XmlDocument doc;

    std::vector<std::string> fieldStack;
    string pathStack = "";
    string buffer;

    XmlElement element;

    // Read one line at a time
    while ( getline( input, buffer ) )
    {
        buffer = Helper::Trim( buffer );

        if ( Helper::Contains( buffer, "<?" ) )
        {
            // ignore
            continue;
        }
        else if ( Helper::Contains( buffer, "</" ) )
        {
            // End of an element or closing </...>
            fieldStack.pop_back();
        }
        else if ( Helper::Contains( buffer, "<" ) )
        {
            // <?xml version="1.0" encoding="UTF-8"?>
            // split by spaces
            vector<string> items = Helper::Split( buffer, " " );
            // First item should be the element type
            // Find first occurrence of <
            size_t beginPos = items[0].find( "<" );
            string elementType = items[0].substr( beginPos+1, string::npos );
            fieldStack.push_back( elementType );

            element.path = StackString( fieldStack );

            // Get the rest of the attributes/values for this item
            for ( size_t i = 1; i < items.size(); i++ )
            {
                std::vector<std::string> keyval = Helper::Split( items[i], "=" );
                std::string key = keyval[0];
                std::string value = keyval[1];
                value = Helper::Replace( value, "\"", "" );
                value = Helper::Replace( value, "/>", "" );
                value = Helper::Replace( value, ">", "" );
                element.attributes[key] = value;
            }

            if ( Helper::Contains( buffer, "/>" ) )
            {
                // End of an element or closing </...>
                fieldStack.pop_back();
            }

            doc.elements.push_back( element );
        }
        else
        {
            // Raw data / not XML?
            doc.elements[doc.elements.size()-1].data.push_back( buffer );
        }
    }

    Logger::StackPop();
    return doc;
}

std::string XmlParser::StackString( std::vector<std::string> pathStack )
{
    std::string str = "";
    for ( size_t i = 0; i < pathStack.size(); i++ )
    {
        str += pathStack[i] + "/";
    }
    return str;
}
