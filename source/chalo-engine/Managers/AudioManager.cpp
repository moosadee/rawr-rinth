// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#include "AudioManager.hpp"

#include "../Utilities/Logger.hpp"
#include "../Utilities/Helper.hpp"
#include "ConfigManager.hpp"

#include <stdexcept>
#include <string>

namespace chalo
{

std::string AudioManager::s_className = "AudioManager";

std::map<std::string, sf::Music> AudioManager::m_music;
std::map<std::string, sf::SoundBuffer> AudioManager::m_soundBuffer;

void AudioManager::AddMusic( const std::string& key, const std::string& path )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Adding music with key \"" + key + "\" to path \"" + path + "\"", s_className + "::" + __func__ );
    sf::Music music;
    if ( !m_music[ key ].openFromFile( path ) )
    {
        // Error
        Logger::Error( "Unable to load music at path \"" + path + "\"", s_className + "::" + __func__ );
        Logger::StackPop();
        return;
    }
    Logger::StackPop();

}

void AudioManager::AddSoundBuffer( const std::string& key, const std::string& path )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Adding sound buffer with key \"" + key + "\" to path \"" + path + "\"", s_className + "::" + __func__ );
    sf::SoundBuffer buffer;
    if ( !buffer.loadFromFile( path ) )
    {
        // Error
        Logger::Error( "Unable to load sound bufferat path \"" + path + "\"", s_className + "::" + __func__ );
        Logger::StackPop();
        return;
    }

    m_soundBuffer[ key ] = buffer;
    Logger::StackPop();
}

void AudioManager::Clear()
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_music.clear();
    m_soundBuffer.clear();
    Logger::StackPop();
}

const sf::Music& AudioManager::GetMusic( const std::string& key )
{
    Logger::StackPush( s_className + "::" + __func__ );
    if ( m_music.find( key ) == m_music.end() )
    {
        // Not found
        Logger::Error( "Could not find music with key \"" + key + "\"", s_className + "::" + __func__ );
        Logger::StackPop();
        throw std::runtime_error( "Could not find music with key \"" + key + "\" + AudioManager::GetMusic" );
    }
    Logger::StackPop();
    return m_music[ key ];
}

const sf::SoundBuffer& AudioManager::GetSoundBuffer( const std::string& key )
{
    Logger::StackPush( s_className + "::" + __func__ );
    if ( m_soundBuffer.find( key ) == m_soundBuffer.end() )
    {
        // Not found
        Logger::Error( "Could not find sound buffer with key \"" + key + "\"", s_className + "::" + __func__ );
        Logger::StackPop();
        throw std::runtime_error( "Could not find sound buffer with key \"" + key + "\" + AudioManager::GetSoundBuffer" );
    }
    Logger::StackPop();
    return m_soundBuffer[ key ];
}

void AudioManager::PlayMusic( const std::string& key, bool repeat /* = true */ )
{
    Logger::StackPush( s_className + "::" + __func__ );
    if ( m_music.find( key ) == m_music.end() )
    {
        // Not found
        Logger::Error( "Could not find music with key \"" + key + "\"", s_className + "::" + __func__ );
        Logger::StackPop();
        return;
    }
    m_music[ key ].setVolume( Helper::StringToInt( ConfigManager::Get( "MUSIC_VOLUME" ) ) );
    m_music[ key ].setLoop( repeat );
    m_music[ key ].play();
    Logger::StackPop();
}

void AudioManager::PlaySound( const std::string& key )
{
    Logger::StackPush( s_className + "::" + __func__ );
    if ( m_soundBuffer.find( key ) == m_soundBuffer.end() )
    {
        // Not found
        Logger::Error( "Could not find sound buffer with key \"" + key + "\"", "AudioManager::PlaySound" );
        Logger::StackPop();
        return;
    }
    Logger::StackPop();
}

}
