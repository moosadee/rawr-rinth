// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#include "StateManager.hpp"
#include "InputManager.hpp"
#include "../Utilities/Logger.hpp"
#include "../Utilities/Messager.hpp"
#include "../States/DefaultState.hpp"

#include <string>

namespace chalo
{

std::map< std::string, IState* > StateManager::m_states;
IState* StateManager::m_ptrCurrentState = nullptr;
IState* StateManager::m_defaultState = nullptr;
std::string StateManager::m_currentStateName = "";

std::string StateManager::s_className = "StateManager";

void StateManager::InitManager()
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_ptrCurrentState = nullptr;
    m_defaultState = new DefaultState;
    AddState( "default", m_defaultState ); // We escape to this state if there is a fatal error
    Logger::StackPop();
}

void StateManager::Cleanup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    // std::map< std::string, IState* > m_states;
    for ( auto& state : m_states )
    {
        if ( state.second != nullptr )
        {
            delete state.second;
            state.second = nullptr;
        }
    }
    Logger::StackPop();
}

void StateManager::Debug()
{
    Logger::StackPush( s_className + "::" + __func__ );

    std::string str = "std::map< std::string, IState* > m_states:\n<br>";
    str += "<table><tr><th>key</th><th>value</th></tr>";
    for ( auto& state : m_states )
    {
        str += "\n<tr><td>\"" + state.first + "\"</td><td>\"" + Helper::ToString( state.second ) + "\"</td></tr>";
    }
    str += "</table>";
    Logger::Debug( str, "ChaloProgram::Setup" );

    Logger::Debug( "m_ptrCurrentState: " + Helper::ToString( m_ptrCurrentState ) + ", " + m_currentStateName, s_className + "::" + __func__ );
    Logger::StackPop();
}

void StateManager::FATALERROR()
{
    Logger::StackPush( s_className + "::" + __func__ );
    ChangeState( "default" );
    Logger::StackPop();
}

void StateManager::AddState( const std::string& stateName, IState* state )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Debug( "stateName: " + stateName + ", state: " + Helper::ToString( state ), s_className + "::" + __func__ );

    // Is there already a state with this key name?
    if ( m_states.find( stateName ) != m_states.end() )
    {
        Logger::Error( "ERROR: State with name \"" + stateName + "\" already exists!", s_className + "::" + __func__ );
    }

    state->Init( stateName );
    m_states[ stateName ] = state;
    Logger::StackPop();
}

void StateManager::ChangeState( const std::string& stateName )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Change state to \"" + stateName + "\"", s_className + "::" + __func__ );
    m_currentStateName = stateName;
    Debug();

    // Disable the click for now.
    chalo::InputManager::DisableClick();

    std::map< std::string, IState* >::iterator it = m_states.find( stateName );

    if ( it == m_states.end() )
    {
        std::cout << stateName << " not found!" << std::endl;
    }

    if ( m_ptrCurrentState != nullptr )
    {
        m_ptrCurrentState->Cleanup();
    }

    m_ptrCurrentState = m_states[ stateName ];

    if ( m_ptrCurrentState == nullptr )
    {
        string error = "Error! State was changed to nullptr when switching to \"" + stateName + "\"!";
        Logger::Error( error, s_className + "::" + __func__ );
        chalo::Messager::Set( "ERROR", error );
        ChangeState( "default" );
        return;
    }

    m_ptrCurrentState->Setup();
    Logger::StackPop();
}

void StateManager::SetupState()
{
    Logger::StackPush( s_className + "::" + __func__ );
    if ( m_ptrCurrentState == nullptr )
    {
        string error = "Error! Function called while m_ptrCurrentState is nullptr!";
        Logger::Error( error, s_className + "::" + __func__ );
        chalo::Messager::Set( "ERROR", error );
        ChangeState( "default" );
    }
    m_ptrCurrentState->Setup();
    Logger::StackPop();
}

void StateManager::UpdateState()
{
    Logger::StackPush( s_className + "::" + __func__ );
    if ( m_ptrCurrentState == nullptr )
    {
        Logger::Error( "Error! Function called while m_ptrCurrentState is nullptr!", s_className + "::" + __func__ );
        ChangeState( "default" );
        Logger::StackPop();
        return;
    }
    m_ptrCurrentState->Update();
    Logger::StackPop();
}

void StateManager::DrawState( sf::RenderTexture& window )
{
    Logger::StackPush( s_className + "::" + __func__ );
    if ( m_ptrCurrentState == nullptr )
    {
        Logger::Error( "Error! Function called while m_ptrCurrentState is nullptr!", s_className + "::" + __func__ );
        ChangeState( "default" );
        return;
    }
    m_ptrCurrentState->Draw( window );
    Logger::StackPop();
}

std::string StateManager::GetGotoState()
{
    if ( m_ptrCurrentState == nullptr )
    {
        Logger::Error( "Error! Function called while m_ptrCurrentState is nullptr!", s_className + "::" + __func__ );
        ChangeState( "default" );
        return "default";
    }
    return m_ptrCurrentState->GetGotoState();
}

}
