#include "MenuManager.hpp"

#include "../Application/Application.hpp"
#include "../Utilities/Logger.hpp"
#include "../Utilities/Helper.hpp"
#include "../Utilities_SFML/SFMLHelper.hpp"
#include "TextureManager.hpp"
#include "InputManager.hpp"
#include "FontManager.hpp"
#include "ConfigManager.hpp"
#include "LanguageManager.hpp"

#include <string>
#include <fstream>
#include <exception>

namespace chalo
{

std::string MenuManager::s_className = "MenuManager";

std::string MenuManager::m_menuPathBase;
std::map< std::string, std::map<std::string, UIRectangleShape> > MenuManager::m_shapeLayers;
std::map< std::string, std::map<std::string, UIButton> > MenuManager::m_buttonLayers;
std::map< std::string, std::map<std::string, UILabel> > MenuManager::m_labelLayers;
std::map< std::string, std::map<std::string, UIImage> > MenuManager::m_imageLayers;
std::map< std::string, std::map<std::string, UITextBox> > MenuManager::m_textboxLayers;
std::stack< std::string > MenuManager::m_menuStack;
UITextBox* MenuManager::m_ptrActiveTextbox;

void MenuManager::Setup( const std::string& pathBase )
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_menuPathBase = pathBase;
    m_ptrActiveTextbox = nullptr;
    Logger::StackPop();
}

void MenuManager::Cleanup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_shapeLayers.clear();
    m_buttonLayers.clear();
    m_labelLayers.clear();
    m_imageLayers.clear();
    m_textboxLayers.clear();
    Logger::StackPop();
}

void MenuManager::DyslexiaFontReload()
{
    Logger::StackPush( s_className + "::" + __func__ );
    for ( auto& layer : m_buttonLayers )
    {
        for ( auto& button : layer.second )
        {
            button.second.ToggleDyslexiaFont();
        }
    }
    for ( auto& layer : m_labelLayers )
    {
        for ( auto& label : layer.second )
        {
            label.second.ToggleDyslexiaFont();
        }
    }
    Logger::StackPop();
}

std::string MenuManager::GetClickedButton()
{
    Logger::StackPush( s_className + "::" + __func__ );

    if ( chalo::InputManager::IsLeftClickRelease() )
    {
        sf::Vector2f localPosition = chalo::InputManager::GetMousePosition();

        for ( auto& layer : m_buttonLayers )
        {
            for ( auto& element : layer.second )
            {
                bool isClickingOnButton = SFMLHelper::PointInBoxCollision( localPosition, element.second.GetPositionRect() );

                if ( isClickingOnButton )
                {
                    chalo::InputManager::DisableClick();
                    Logger::StackPop();
                    return element.second.GetId();
                }
            }
        }
    }

    Logger::StackPop();
    return "";
}

std::string MenuManager::GetHoveredButton( sf::Vector2f position )
{
    for ( auto& layer : m_buttonLayers )
    {
        for ( auto& element : layer.second )
        {
            if ( SFMLHelper::PointInBoxCollision( position, element.second.GetPositionRect() ) )
            {
                chalo::InputManager::DisableClick();
                return element.second.GetId();
            }
        }
    }

    return "";
}

std::string MenuManager::GetTextboxValue( const std::string& textboxName )
{
    for ( auto& layer : m_textboxLayers )
    {
        if ( layer.second.find( textboxName ) != layer.second.end() )
        {
            // Found
            return layer.second[ textboxName ].GetEnteredText();
        }
    }

    return "";
}

void MenuManager::Update()
{
    InputManager::Update();

    // Check to see if any textboxes have been selected
    if ( sf::Mouse::isButtonPressed( sf::Mouse::Left ) )
    {
        m_ptrActiveTextbox = nullptr;
        sf::Vector2i localPosition = sf::Mouse::getPosition( Application::GetWindow() );

        for ( auto& layer : m_textboxLayers )
        {
            for ( auto& element : layer.second )
            {
                if ( SFMLHelper::PointInBoxCollision( localPosition, element.second.GetPositionRect() ) )
                {
                    // This textbox has been selected
                    Logger::Out( "Textbox " + element.second.GetId() + " received focus", s_className + "::" + __func__ );
                    element.second.SetFocus( true );
                    m_ptrActiveTextbox = &element.second;
                }
                else
                {
                    element.second.SetFocus( false );
                }
            }
        }
    }

    // Check for keyboard presses
    if ( m_ptrActiveTextbox != nullptr )
    {
        // Input method :(
        if ( InputManager::IsKeyPressed( sf::Keyboard::BackSpace ) )
        {
            m_ptrActiveTextbox->EraseCharacter();
        }

        std::string typedCharacter = InputManager::GetTypedLetter();
        if ( typedCharacter != "" )
        {
            m_ptrActiveTextbox->AddCharacter( typedCharacter );
        }
    }
}

void MenuManager::Draw( sf::RenderTexture& window )
{
    // Shapes
    for ( auto& layer : m_shapeLayers )
    {
        for ( auto& element : layer.second )
        {
            window.draw( element.second.GetShape() );
        }
    }

    // Images
    for ( auto& layer : m_imageLayers )
    {
        for ( auto& element : layer.second )
        {
            if ( element.second.GetIsVisible() ) {
                window.draw( element.second.GetSprite() );
            }
        }
    }
    // Buttons
    for ( auto& layer : m_buttonLayers )
    {
        for ( auto& element : layer.second )
        {
            if ( element.second.GetIsVisible() ) {
                window.draw( element.second.GetBackground().GetSprite() );
                window.draw( element.second.GetIcon().GetSprite() );
                window.draw( element.second.GetLabel().GetText() );
            }
        }
    }

    // Labels
    for ( auto& layer : m_labelLayers )
    {
        for ( auto& element : layer.second )
        {
            if ( element.second.GetIsVisible() ) {
                window.draw( element.second.GetText() );
            }
        }
    }

    // Textboxes
    for ( auto& layer : m_textboxLayers )
    {
        for ( auto& element : layer.second )
        {
            window.draw( element.second.GetBackground().GetShape() );
            window.draw( element.second.GetCursor().GetShape() );
            window.draw( element.second.GetLabel().GetText() );
        }
    }
}

void MenuManager::LoadTextMenu( const std::string& filename )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Loading menu " + m_menuPathBase + filename + "... ", s_className + "::" + __func__ );
    Cleanup();

    m_menuStack.push( filename );

    std::ifstream infile( m_menuPathBase + filename );
    std::string buffer;

    std::string type;
    std::string elementName;
    std::string elementType;
    std::string layerName;
    std::string elementText;
    std::wstring elementTextW;
    std::string elementBackgroundTextureName;
    std::string fontName;
    bool visible = true;
    int x, y, width, height;
    int backgroundOffsetX, backgroundOffsetY;
    int textOffsetX, textOffsetY;
    int borderR, borderG, borderB, borderA;
    int fillR, fillG, fillB, fillA;
    int textR, textG, textB, textA;
    int imageLeft, imageTop, imageWidth, imageHeight;
    int borderThickness;
    int textSize;
    std::string iconTexture;
    int iconX, iconY;
    int iconLeft, iconTop, iconWidth, iconHeight;
    bool asciiText = true;

    while ( infile >> buffer )
    {
        if          ( buffer == "LAYER_NAME" )                  { infile >> layerName; }
        else if     ( buffer == "ELEMENT_NAME" )                { infile >> elementName; }
        else if     ( buffer == "ELEMENT_TYPE" )                { infile >> elementType; }
        else if     ( buffer == "ELEMENT_VISIBLE" )             { infile >> visible; }
        else if     ( buffer == "ELEMENT_X" )                   { infile >> x; }
        else if     ( buffer == "ELEMENT_Y" )                   { infile >> y; }
        else if     ( buffer == "ELEMENT_WIDTH" )               { infile >> width; }
        else if     ( buffer == "ELEMENT_HEIGHT" )              { infile >> height; }
        else if     ( buffer == "ELEMENT_BORDER_COLOR_R" )      { infile >> borderR; }
        else if     ( buffer == "ELEMENT_BORDER_COLOR_G" )      { infile >> borderG; }
        else if     ( buffer == "ELEMENT_BORDER_COLOR_B" )      { infile >> borderB; }
        else if     ( buffer == "ELEMENT_BORDER_COLOR_A" )      { infile >> borderA; }
        else if     ( buffer == "ELEMENT_FILL_COLOR_R" )        { infile >> fillR; }
        else if     ( buffer == "ELEMENT_FILL_COLOR_G" )        { infile >> fillG; }
        else if     ( buffer == "ELEMENT_FILL_COLOR_B" )        { infile >> fillB; }
        else if     ( buffer == "ELEMENT_FILL_COLOR_A" )        { infile >> fillA; }
        else if     ( buffer == "ELEMENT_TEXT_COLOR_R" )        { infile >> textR; }
        else if     ( buffer == "ELEMENT_TEXT_COLOR_G" )        { infile >> textG; }
        else if     ( buffer == "ELEMENT_TEXT_COLOR_B" )        { infile >> textB; }
        else if     ( buffer == "ELEMENT_TEXT_COLOR_A" )        { infile >> textA; }
        else if     ( buffer == "ELEMENT_BORDER_THICKNESS" )    { infile >> borderThickness; }
        else if     ( buffer == "ELEMENT_BACKGROUND_TEXTURE" )  { infile >> elementBackgroundTextureName; }
        else if     ( buffer == "ELEMENT_BACKGROUND_X" )        { infile >> backgroundOffsetX; }
        else if     ( buffer == "ELEMENT_BACKGROUND_Y" )        { infile >> backgroundOffsetY; }
        else if     ( buffer == "ELEMENT_BACKGROUND_FRAME_X" )          { infile >> imageLeft; }
        else if     ( buffer == "ELEMENT_BACKGROUND_FRAME_Y" )          { infile >> imageTop; }
        else if     ( buffer == "ELEMENT_BACKGROUND_FRAME_WIDTH" )      { infile >> imageWidth; }
        else if     ( buffer == "ELEMENT_BACKGROUND_FRAME_HEIGHT" )     { infile >> imageHeight; }

        else if     ( buffer == "ELEMENT_ICON_TEXTURE" )        { infile >> iconTexture; }
        else if     ( buffer == "ELEMENT_ICON_X" )              { infile >> iconX; }
        else if     ( buffer == "ELEMENT_ICON_Y" )              { infile >> iconY; }
        else if     ( buffer == "ELEMENT_ICON_FRAME_X" )        { infile >> iconLeft; }
        else if     ( buffer == "ELEMENT_ICON_FRAME_Y" )        { infile >> iconTop; }
        else if     ( buffer == "ELEMENT_ICON_FRAME_WIDTH" )    { infile >> iconWidth; }
        else if     ( buffer == "ELEMENT_ICON_FRAME_HEIGHT" )   { infile >> iconHeight; }
//        else if     ( buffer == "ELEMENT_ASCII" )               { infile >> asciiText; }

        else if     ( buffer == "ELEMENT_TEXT" )
        {
            infile.ignore();
            asciiText = false; // Just load everything to work with unicode??

            getline( infile, elementText );
            elementText = Helper::Trim( elementText );

            if ( Helper::Contains( elementText, "<<" ) )
            {
                // Replace variable values with config data
                std::string original = elementText;
                int varStart = elementText.find( "<<" ) + 2;
                int varEnd = elementText.find( ">>" );
                int length = varEnd - varStart;
                std::string variableName = elementText.substr( varStart, length );
                std::string value = ConfigManager::Get( variableName );
                elementText = Helper::Replace( elementText, "<<" + variableName + ">>", value );
                Logger::Out( "Replaced text \"" + original + "\" with \"" + elementText + "\"", s_className + "::" + __func__ );
            }
            else if ( Helper::Contains( elementText, "{{" ) )
            {
                // Replace variable values with language items
                std::string original = elementText;
                int varStart = elementText.find( "{{" ) + 2;
                int varEnd = elementText.find( "}}" );
                int length = varEnd - varStart;
                std::string variableName = elementText.substr( varStart, length );
                std::string value = LanguageManager::Text( chalo::ConfigManager::Get( "LANGUAGE_MAIN" ), variableName );
                elementText = Helper::Replace( elementText, "{{" + variableName + "}}", value );
                Logger::Out( "Replaced text \"" + original + "\" with \"" + elementText + "\"", s_className + "::" + __func__ );

                // Change font
                fontName = chalo::ConfigManager::Get( "LANGUAGE_MAIN_FONT" );
                Logger::Out( "Suggested font is \"" + fontName + "\"", s_className + "::" + __func__ );
            }
            else if ( Helper::Contains( elementText, "[[" ) )
            {
                // Replace variable values with language items
                std::string original = elementText;
                int varStart = elementText.find( "[[" ) + 2;
                int varEnd = elementText.find( "]]" );
                int length = varEnd - varStart;
                std::string variableName = elementText.substr( varStart, length );
                std::string value = LanguageManager::Text( chalo::ConfigManager::Get( "LANGUAGE_TARGET" ), variableName );
                elementText = Helper::Replace( elementText, "[[" + variableName + "]]", value );
                Logger::Out( "Replaced text \"" + original + "\" with \"" + elementText + "\"", s_className + "::" + __func__ );

                // Change font
                fontName = chalo::ConfigManager::Get( "LANGUAGE_TARGET_FONT" );
                Logger::Out( "Suggested font is \"" + fontName + "\"", s_className + "::" + __func__ );
            }
        }
        else if     ( buffer == "ELEMENT_TEXT_FONT" )           { infile >> fontName; }
        else if     ( buffer == "ELEMENT_TEXT_X" )              { infile >> textOffsetX; }
        else if     ( buffer == "ELEMENT_TEXT_Y" )              { infile >> textOffsetY; }
        else if     ( buffer == "ELEMENT_TEXT_SIZE" )           { infile >> textSize; }
        else if     ( buffer == "ELEMENT_IMAGECLIP_LEFT" )      { infile >> imageLeft; }
        else if     ( buffer == "ELEMENT_IMAGECLIP_TOP" )       { infile >> imageTop; }
        else if     ( buffer == "ELEMENT_IMAGECLIP_WIDTH" )     { infile >> imageWidth; }
        else if     ( buffer == "ELEMENT_IMAGECLIP_HEIGHT" )    { infile >> imageHeight; }

        else if ( buffer == "ELEMENT_END" )
        {
            if ( elementType == "rectangleshape" )
            {
                sf::Color borderColor = sf::Color( borderR, borderG, borderB, borderA );
                sf::Color fillColor = sf::Color( fillR, fillG, fillB, fillA );

                // AddRectangleShape( const std::string& elementName, sf::Color borderColor, int borderThickness, sf::Color fillColor, sf::Vector2f position, sf::vector2f dimensions )
                AddRectangleShape( layerName, elementName, borderColor, borderThickness, fillColor, sf::Vector2f( x, y ), sf::Vector2f( width, height ) );
            }
            else if ( elementType == "button" )
            {
                sf::Color fillColor = sf::Color( textR, textG, textB, textA );

                AddButton(
                    layerName,                                                      // const std::string& layerName
                    elementName,                                                    // const std::string& elementName
                    sf::Vector2f( x, y ),                                           // sf::Vector2f position
                    sf::IntRect( 0, 0, width, height ),                             // sf::IntRect dimensions
                    elementBackgroundTextureName,                                   // const std::string& backgroundTexture
                    sf::Vector2f( backgroundOffsetX, backgroundOffsetY ),           // sf::Vector2f bgOffset
                    sf::IntRect( imageLeft, imageTop, imageWidth, imageHeight ),    // sf::IntRect frameRect
                    fontName,                                                       // const std::string& fontName
                    textSize,                                                       // int textSize
                    fillColor,                                                      // sf::Color textColor
                    sf::Vector2f( textOffsetX, textOffsetY ),                       // sf::Vector2f textOffset
                    elementText,                                                    // const std::string& elementText
                    asciiText,
                    iconTexture,
                    iconX,
                    iconY,
                    iconLeft,
                    iconTop,
                    iconWidth,
                    iconHeight,
                    visible
                );
            }
            else if ( elementType == "label" )
            {
                sf::Color fillColor = sf::Color( textR, textG, textB, textA );

                AddLabel(
                    layerName,              // const std::string& layerName
                    elementName,            // const std::string& elementName
                    fillColor,              // sf::Color textColor
                    fontName,               // const std::string& fontName
                    textSize,               // int textSize
                    sf::Vector2f( x, y ),   // sf::Vector2f position
                    elementText,             // const std::string& elementText
                    asciiText,
                    visible
                );
            }
            else if ( elementType == "image" )
            {
                AddImage(
                    layerName,                                                      // const std::string& layerName
                    elementName,                                                    // const std::string& elementName
                    elementBackgroundTextureName,                                   // const std::string& textureName
                    sf::Vector2f( x, y ),                                           // sf::Vector2f position
                    sf::IntRect( imageLeft, imageTop, imageWidth, imageHeight ),    // sf::IntRect frameRect
                    visible
                );
            }
            else if ( elementType == "textbox" )
            {
                sf::Color borderColor = sf::Color( borderR, borderG, borderB, borderA );
                sf::Color fillColor = sf::Color( fillR, fillG, fillB, fillA );
                sf::Color textColor = sf::Color( textR, textG, textB, textA );

                // AddTextBox( const std::string& layerName, const std::string& elementName, sf::Vector2f position, sf::IntRect dimensions, sf::Color borderColor, int borderThickness, sf::Color fillColor, sf::Color textColor, int textSize, sf::Vector2f textOffset, const std::string& elementText )
                AddTextBox( layerName, elementName, sf::Vector2f( x, y ), sf::IntRect( 0, 0, width, height ), borderColor, borderThickness,
                            fillColor, textColor, fontName, textSize, sf::Vector2f( textOffsetX, textOffsetY ), elementText );
            }

            // Resets
            elementText = "";
            iconTexture = "";
            imageLeft = -1;
            imageTop = -1;
            imageWidth = -1;
            imageHeight = -1;
            visible = true;
            asciiText = true;
        }
    }

    int totalCount;

    totalCount = 0;
    for ( auto& layer : m_shapeLayers )
    {
        totalCount += layer.second.size();
    }
    Logger::Out( "Total shape layers: " + Helper::ToString( m_shapeLayers.size() ) +
            ", Total shapes: " + Helper::ToString( totalCount ), s_className + "::" + __func__, "data-stats" );

    totalCount = 0;
    for ( auto& layer : m_buttonLayers )
    {
        totalCount += layer.second.size();
    }
    Logger::Out( "Total button layers: " + Helper::ToString( m_buttonLayers.size() ) +
            ", Total buttons: " + Helper::ToString( totalCount ), s_className + "::" + __func__, "data-stats" );

    totalCount = 0;
    for ( auto& layer : m_imageLayers )
    {
        totalCount += layer.second.size();
    }
    Logger::Out( "Total image layers: " + Helper::ToString( m_imageLayers.size() ) +
            ", Total images: " + Helper::ToString( totalCount ), s_className + "::" + __func__, "data-stats" );


    totalCount = 0;
    for ( auto& layer : m_labelLayers )
    {
        totalCount += layer.second.size();
    }
    Logger::Out( "Total labels layers: " + Helper::ToString( m_labelLayers.size() ) +
            ", Total labels: " + Helper::ToString( totalCount ), s_className + "::" + __func__, "data-stats" );


    totalCount = 0;
    for ( auto& layer : m_textboxLayers )
    {
        totalCount += layer.second.size();
    }
    Logger::Out( "Total textbox layers: " + Helper::ToString( m_textboxLayers.size() ) +
            ", Total textboxes: " + Helper::ToString( totalCount ), s_className + "::" + __func__, "data-stats" );
    Logger::StackPop();
}

bool MenuManager::GoBack()
{
    if ( m_menuStack.size() > 1 )
    {
        m_menuStack.pop();
        LoadTextMenu( m_menuStack.top() );
        return true;
    }

    return false;
}

void MenuManager::AddRectangleShape( const std::string& layerName, const std::string& elementName, sf::Color borderColor, int borderThickness, sf::Color fillColor, sf::Vector2f position, sf::Vector2f dimensions )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "AddRectangleShape: Layer=\"" + layerName + "\", elementName=\"" + elementName + "\"", s_className + "::" + __func__ );
    UIRectangleShape rectangle;

    rectangle.Setup( elementName, borderColor, borderThickness, fillColor, position, dimensions );
    m_shapeLayers[layerName][elementName] = rectangle;

    Logger::Out( "Adding new RectangleShape element: [" + layerName + "][" + elementName + "]", s_className + "::" + __func__, "file-parsing" );
    Logger::StackPop();
}

void MenuManager::AddButton(
    const std::string& layerName,
    const std::string& elementName,
    sf::Vector2f position,
    sf::IntRect dimensions,
    std::string backgroundTexture,
    sf::Vector2f bgOffset,
    sf::IntRect frameRect,
    std::string fontName,
    int textSize,
    sf::Color textColor,
    sf::Vector2f textOffset,
    std::string elementText,
    bool asciiText,
    std::string iconTexture,
    int iconX,
    int iconY,
    int iconLeft,
    int iconTop,
    int iconWidth,
    int iconHeight,
    bool visible /* = true */
)
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "AddButton: Layer=\"" + layerName + "\", elementName=\"" + elementName + "\"", s_className + "::" + __func__ );
    UIButton button;

    button.Setup( elementName, position, dimensions );
    button.SetupBackground( chalo::TextureManager::Get( backgroundTexture ), bgOffset, frameRect );

    if ( elementText != "" )
    {
        button.SetOriginalFont( fontName );
        if ( asciiText )
        {
            button.SetupText( fontName, textSize, textColor, textOffset, elementText );
        }
        else
        {
            button.SetupTextW( fontName, textSize, textColor, textOffset, Helper::StringToWString( elementText ) );
        }
    }

    if ( iconTexture != "" )
    {
        sf::Vector2f offsetPosition;
        offsetPosition.x = iconX;
        offsetPosition.y = iconY;
        sf::IntRect imageClip;
        imageClip.left = iconLeft;
        imageClip.top = iconTop;
        imageClip.width = iconWidth;
        imageClip.height = iconHeight;
        button.SetupIcon( chalo::TextureManager::Get( iconTexture ), offsetPosition, imageClip );
    }

    button.SetIsVisible( visible );

    m_buttonLayers[layerName][elementName] = button;

    Logger::Out( "Adding new Button element: [" + layerName + "][" + elementName + "]", s_className + "::" + __func__, "file-parsing" );
    Logger::StackPop();
}


void MenuManager::AddButton(
    const std::string& layerName,
    const std::string& elementName,
    UIButton button
)
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "AddButton: Layer=\"" + layerName + "\", elementName=\"" + elementName + "\"", s_className + "::" + __func__ );
    m_buttonLayers[layerName][elementName] = button;
    Logger::Out( "Adding new Button element: [" + layerName + "][" + elementName + "]", s_className + "::" + __func__, "file-parsing" );
    Logger::StackPop();
}

void MenuManager::AddLabel(
    const std::string& layerName,
    const std::string& elementName,
    sf::Color textColor,
    const std::string& fontName,
    int textSize,
    sf::Vector2f position,
    const std::string& elementText,
    bool asciiText,
    bool visible
)
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "AddLabel: Layer=\"" + layerName + "\", elementName=\"" + elementName + "\"", s_className + "::" + __func__ );
    UILabel label;
    label.SetOriginalFont( fontName );

    if ( asciiText )
    {
        label.Setup( elementName, fontName, textSize, textColor, position, elementText );
    }
    else
    {
        label.SetupW( elementName, fontName, textSize, textColor, position, Helper::StringToWString( elementText ) );
    }
    label.SetIsVisible( visible );

    Logger::Out( "Adding new Label element: [" + layerName + "][" + elementName + "]", s_className + "::" + __func__, "file-parsing" );

    m_labelLayers[layerName][elementName] = label;
    Logger::StackPop();
}

void MenuManager::AddImage(
    const std::string& layerName,
    const std::string& elementName,
    const std::string& textureName,
    sf::Vector2f position,
    sf::IntRect frameRect,
    bool visible
)
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "AddImage: Layer=\"" + layerName + "\", elementName=\"" + elementName + "\"", s_className + "::" + __func__ );
    UIImage image;

    // void Setup( const std::string& key, const sf::Texture& texture, sf::Vector2f position, sf::IntRect imageClip );
    image.Setup( elementName, chalo::TextureManager::Get( textureName ), position, frameRect );
    image.SetIsVisible( visible );

    Logger::Out( "Adding new Image element: [" + layerName + "][" + elementName + "]", s_className + "::" + __func__, "file-parsing" );

    m_imageLayers[layerName][elementName] = image;
    Logger::StackPop();
}

void MenuManager::AddTextBox( const std::string& layerName, const std::string& elementName, sf::Vector2f position, sf::IntRect dimensions,
                             sf::Color borderColor, int borderThickness, sf::Color fillColor, sf::Color textColor, const std::string& fontName,
                             int textSize, sf::Vector2f textOffset, const std::string& elementText )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "AddTextBox: Layer=\"" + layerName + "\", elementName=\"" + elementName + "\"", s_className + "::" + __func__ );
    UITextBox textbox;

    textbox.Setup( elementName, position, dimensions );
    textbox.SetupBackground( borderColor, borderThickness, fillColor, position, sf::Vector2f( dimensions.width, dimensions.height ) );
    textbox.SetupText( fontName, textSize, textColor, textOffset, elementText );

    Logger::Out( "Adding new TextBox element: [" + layerName + "][" + elementName + "]", s_className + "::" + __func__, "file-parsing" );

    m_textboxLayers[layerName][elementName] = textbox;
    Logger::StackPop();
}

UILabel&             MenuManager::GetLabel( const std::string& layer, const std::string& name )
{
    if ( m_labelLayers.find( layer ) == m_labelLayers.end() )
    {
        Logger::Error( "Cannot find layer \"" + name + "\"", s_className + "::" + __func__ );
        throw std::out_of_range( "Cannot find layer key \"" + name + "\"" );
    }

    if ( m_labelLayers[layer].find( name ) == m_labelLayers[layer].end() )
    {
        Logger::Error( "Cannot find label with key \"" + name + "\"", s_className + "::" + __func__ );
        throw std::out_of_range( "Cannot find label with key \"" + name + "\"" );
    }

    return m_labelLayers[layer][name];
}

UILabel&             MenuManager::GetLabel( const std::string& name )
{
    for ( auto& layer : m_labelLayers )
    {
        if ( layer.second.find( name ) != layer.second.end() )
        {
            return layer.second[name];
        }
    }

    Logger::Error( "Cannot find label with key \"" + name + "\"", s_className + "::" + __func__ );
    throw std::out_of_range( "Cannot find label with key \"" + name + "\"" );
}

UIImage&             MenuManager::GetImage( const std::string& layer, const std::string& name )
{
    if ( m_imageLayers.find( layer ) == m_imageLayers.end() )
    {
        Logger::Error( "Cannot find layer \"" + name + "\"", s_className + "::" + __func__ );
        throw std::out_of_range( "Cannot find layer key \"" + name + "\"" );
    }

    if ( m_imageLayers[layer].find( name ) == m_imageLayers[layer].end() )
    {
        Logger::Error( "Cannot find image with key \"" + name + "\"", s_className + "::" + __func__ );
        throw std::out_of_range( "Cannot find image with key \"" + name + "\"" );
    }

    return m_imageLayers[layer][name];
}

UIImage&             MenuManager::GetImage( const std::string& name )
{
    for ( auto& layer : m_imageLayers )
    {
        if ( layer.second.find( name ) != layer.second.end() )
        {
            return layer.second[name];
        }
    }

    Logger::Error( "Cannot find image with key \"" + name + "\"", s_className + "::" + __func__ );
    throw std::out_of_range( "Cannot find image with key \"" + name + "\"" );
}

UIButton&            MenuManager::GetButton( const std::string& layer, const std::string& name )
{
    if ( m_buttonLayers.find( layer ) == m_buttonLayers.end() )
    {
        Logger::Error( "Cannot find layer \"" + name + "\"", s_className + "::" + __func__ );
        throw std::out_of_range( "Cannot find layer key \"" + name + "\"" );
    }

    if ( m_buttonLayers[layer].find( name ) == m_buttonLayers[layer].end() )
    {
        Logger::Error( "Cannot find button with key \"" + name + "\"", s_className + "::" + __func__ );
        throw std::out_of_range( "Cannot find button with key \"" + name + "\"" );
    }

    return m_buttonLayers[layer][name];
}

UIButton&            MenuManager::GetButton( const std::string& name )
{
    for ( auto& layer : m_buttonLayers )
    {
        if ( layer.second.find( name ) != layer.second.end() )
        {
            return layer.second[name];
        }
    }

    Logger::Error( "Cannot find button with key \"" + name + "\"", s_className + "::" + __func__ );
    throw std::out_of_range( "Cannot find button with key \"" + name + "\"" );
}

UIRectangleShape&    MenuManager::GetRectangleShape( const std::string& layer, const std::string& name )
{
    if ( m_shapeLayers.find( layer ) == m_shapeLayers.end() )
    {
        Logger::Error( "Cannot find layer \"" + name + "\"", s_className + "::" + __func__ );
        throw std::out_of_range( "Cannot find layer key \"" + name + "\"" );
    }

    if ( m_shapeLayers[layer].find( name ) == m_shapeLayers[layer].end() )
    {
        Logger::Error( "Cannot find shape with key \"" + name + "\"", s_className + "::" + __func__ );
        throw std::out_of_range( "Cannot find shape with key \"" + name + "\"" );
    }

    return m_shapeLayers[layer][name];
}

UIRectangleShape&    MenuManager::GetRectangleShape( const std::string& name )
{
    for ( auto& layer : m_shapeLayers )
    {
        if ( layer.second.find( name ) != layer.second.end() )
        {
            return layer.second[name];
        }
    }

    Logger::Error( "Cannot find shape with key \"" + name + "\"", s_className + "::" + __func__ );
    throw std::out_of_range( "Cannot find shape with key \"" + name + "\"" );
}

UITextBox&           MenuManager::GetTextBox( const std::string& layer, const std::string& name )
{
    if ( m_textboxLayers.find( layer ) == m_textboxLayers.end() )
    {
        Logger::Error( "Cannot find layer \"" + name + "\"", s_className + "::" + __func__ );
        throw std::out_of_range( "Cannot find layer key \"" + name + "\"" );
    }

    if ( m_textboxLayers[layer].find( name ) == m_textboxLayers[layer].end() )
    {
        Logger::Error( "Cannot find textbox with key \"" + name + "\"", s_className + "::" + __func__ );
        throw std::out_of_range( "Cannot find textbox with key \"" + name + "\"" );
    }

    return m_textboxLayers[layer][name];
}

UITextBox&           MenuManager::GetTextBox( const std::string& name )
{
    for ( auto& layer : m_textboxLayers )
    {
        if ( layer.second.find( name ) != layer.second.end() )
        {
            return layer.second[name];
        }
    }

    Logger::Error( "Cannot find textbox with key \"" + name + "\"", s_className + "::" + __func__ );
    throw std::out_of_range( "Cannot find textbox with key \"" + name + "\"" );
}


}
