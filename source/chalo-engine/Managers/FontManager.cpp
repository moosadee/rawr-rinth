// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#include "FontManager.hpp"

#include "../Utilities/Logger.hpp"

#include <stdexcept>
#include <string>

namespace chalo
{

std::string FontManager::s_className = "FontManager";

std::map<std::string, sf::Font> FontManager::m_assets;

void FontManager::Add( const std::string& key, const std::string& path )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Adding font with key \"" + key + "\" to path \"" + path + "\"", s_className + "::" + __func__ );
    sf::Font font;
    if ( !font.loadFromFile( path ) )
    {
        // Error
        Logger::Error( "Unable to load font at path \"" + path + "\"", s_className + "::" + __func__ );
        Logger::StackPop();
        return;
    }
    m_assets[ key ] = font;
    Logger::StackPop();
}

void FontManager::Clear()
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_assets.clear();
    Logger::StackPop();
}

sf::Font& FontManager::Get( const std::string& key )
{
    Logger::StackPush( s_className + "::" + __func__ );
    if ( m_assets.find( key ) == m_assets.end() )
    {
        Logger::Error( "Could not find font with key \"" + key + "\"!", s_className + "::" + __func__ );
        //throw std::runtime_error( "Could not find font with key \"" + key + "\" - FontManager::Get" );

        // Return some default so the game doesn't crash.
        for ( auto& font : m_assets )
        {
            Logger::StackPop();
            return font.second;
        }
    }
    Logger::StackPop();
    return m_assets[ key ];
}

}
