#ifndef _LANGUAGE_MANAGER_HPP
#define _LANGUAGE_MANAGER_HPP

#include <string>
#include <map>

namespace chalo
{

class LanguageSet
{
    public:
    std::string Text( const std::string& textKey );
    void Add( const std::string& key, const std::string& value );

    void SetSuggestedFont( const std::string& font );
    std::string GetSuggestedFont();

    private:
    std::map< std::string, std::string > m_textMap;
    std::string m_suggestedFont;

private:
    static std::string s_className;
};

class LanguageManager
{
    public:
    static void SetLanguageBasePath( const std::string& path );
    static void AddLanguage( const std::string& languageKey, const std::string& suggestedFont );
    static void SetMainLanguage( const std::string& languageKey );

    static std::string Text( const std::string& textKey );
    static std::string Text( const std::string& languageKey, const std::string& textKey );

    static std::string GetCurrentLanguage();
    static void SetCurrentLanguage( std::string languageKey );

    protected:
    static std::string s_basePath;
    static std::string s_currentLanguage;
    static std::map< std::string, LanguageSet > s_languageSets;

private:
    static std::string s_className;
};

}

#endif
