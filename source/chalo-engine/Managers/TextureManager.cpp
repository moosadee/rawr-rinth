// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#include "TextureManager.hpp"

#include "../Utilities/Logger.hpp"
#include "../Utilities/Helper.hpp"
#include "../Utilities/Messager.hpp"
#include "StateManager.hpp"

#include <stdexcept>
#include <string>

namespace chalo
{

std::string TextureManager::s_className = "TextureManager";

std::map<std::string, sf::Texture> TextureManager::m_assets;

void TextureManager::Setup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    Add( "notexture", "chalo-engine/defaults/notexture.png" );
    Logger::StackPop();
}

void TextureManager::Add( const std::string& key, const std::string& path )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Adding texture with key \"" + key + "\" to path \"" + path + "\"", s_className + "::" + __func__ );
    sf::Texture texture;
    if ( !texture.loadFromFile( path ) )
    {
        // Error
        Logger::Error( "Unable to load texture at path \"" + path + "\"", s_className + "::" + __func__ );
        Logger::StackPop();
        return;
    }
    m_assets[ key ] = texture;
    Logger::StackPop();
}

const sf::Texture& TextureManager::AddAndGet( const std::string& key, const std::string& path )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "key: \"" + key + "\", path: \"" + path + "\"", s_className + "::" + __func__ );
    Add( key, path );
    Logger::StackPop();
    return Get( key );
}

void TextureManager::Clear()
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_assets.clear();
    Logger::StackPop();
}

const sf::Texture& TextureManager::Get( const std::string& key )
{
    Logger::StackPush( s_className + "::" + __func__ );
    if ( m_assets.find( key ) == m_assets.end() )
    {
        // Not found
        std::string fatalError = "Could not find texture with key \"" + key + "\", use \"notexture\" instead.";
        Logger::Error( fatalError, s_className + "::" + __func__ );
        // Use the default texture instead
        Logger::StackPop();
        return m_assets[ "notexture" ];
    }
    Logger::StackPop();
    return m_assets[ key ];
}

}
