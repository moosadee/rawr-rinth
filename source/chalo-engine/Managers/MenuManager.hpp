// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef _MENU_MANAGER_HPP
#define _MENU_MANAGER_HPP

#include <SFML/Graphics.hpp>

#include <map>
#include <string>
#include <stack>

#include "../UI/UIShape.hpp"
#include "../UI/UIButton.hpp"
#include "../UI/UILabel.hpp"
#include "../UI/UIImage.hpp"
#include "../UI/UITextBox.hpp"

namespace chalo
{

class MenuManager
{
    public:
    static void Setup( const std::string& pathBase );
    static void Cleanup();

    static void LoadTextMenu( const std::string& filename );

    static std::string GetClickedButton();
    static std::string GetHoveredButton( sf::Vector2f position );
    static std::string GetTextboxValue( const std::string& textboxName );

    static void Update();
    static bool GoBack();
    static void DyslexiaFontReload();

    static void Draw( sf::RenderTexture& window );

    static void AddRectangleShape(
        const std::string& layerName,
        const std::string& elementName,
        sf::Color borderColor,
        int borderThickness,
        sf::Color fillColor,
        sf::Vector2f position,
        sf::Vector2f dimensions
    );

    static void AddButton(
        const std::string& layerName,
        const std::string& elementName,
        sf::Vector2f position,
        sf::IntRect dimensions,
        std::string backgroundTexture,
        sf::Vector2f bgOffset,
        sf::IntRect frameRect,
        std::string fontName,
        int textSize,
        sf::Color textColor,
        sf::Vector2f textOffset,
        std::string elementText,
        bool asciiText,
        std::string iconTexture,
        int iconX,
        int iconY,
        int iconLeft,
        int iconTop,
        int iconWidth,
        int iconHeight,
        bool visible = true
    );

    static void AddButton(
        const std::string& layerName,
        const std::string& elementName,
        UIButton button
    );

    static void AddLabel(
        const std::string& layerName,
        const std::string& elementName,
        sf::Color textColor,
        const std::string& fontName,
        int textSize,
        sf::Vector2f position,
        const std::string& elementText,
        bool asciiText,
        bool visible
    );

    static void AddImage(
        const std::string& layerName,
        const std::string& elementName,
        const std::string& textureName,
        sf::Vector2f position,
        sf::IntRect frameRect,
        bool visible
    );

    static void AddTextBox(
        const std::string& layerName,
        const std::string& elementName,
        sf::Vector2f position,
        sf::IntRect dimensions,
        sf::Color borderColor,
        int borderThickness,
        sf::Color fillColor,
        sf::Color textColor,
        const std::string& fontName,
        int textSize,
        sf::Vector2f textOffset,
        const std::string& elementText
    );

    static UILabel&             GetLabel( const std::string& layer, const std::string& name );
    static UILabel&             GetLabel( const std::string& name );
    static UIImage&             GetImage( const std::string& layer, const std::string& name );
    static UIImage&             GetImage( const std::string& name );
    static UIButton&            GetButton( const std::string& layer, const std::string& name );
    static UIButton&            GetButton( const std::string& name );
    static UIRectangleShape&    GetRectangleShape( const std::string& layer, const std::string& name );
    static UIRectangleShape&    GetRectangleShape( const std::string& name );
    static UITextBox&           GetTextBox( const std::string& layer, const std::string& name );
    static UITextBox&           GetTextBox( const std::string& name );

    private:
    static std::string m_menuPathBase;
    static std::stack< std::string > m_menuStack;

    // Elements TODO: use polymorphism here to have one set of layers with different types of elements
    static std::map< std::string, std::map<std::string, UIRectangleShape> > m_shapeLayers;
    static std::map< std::string, std::map<std::string, UILabel> > m_labelLayers;
    static std::map< std::string, std::map<std::string, UIImage> > m_imageLayers;
    static std::map< std::string, std::map<std::string, UIButton> > m_buttonLayers;
    static std::map< std::string, std::map<std::string, UITextBox> > m_textboxLayers;

    static UITextBox* m_ptrActiveTextbox;

private:
    static std::string s_className;
};

}

#endif
