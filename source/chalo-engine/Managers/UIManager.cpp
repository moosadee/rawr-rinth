// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#include "UIManager.hpp"

#include "../Application/Application.hpp"
#include "../Utilities/Logger.hpp"
#include "../Utilities/Helper.hpp"
#include "../Utilities_SFML/SFMLHelper.hpp"

#include <string>

namespace chalo
{

std::string UIManager::s_className = "UIManager";

std::map<std::string, UILabel> UIManager::m_labels;
std::map<std::string, UIImage> UIManager::m_images;
std::map<std::string, UIButton> UIManager::m_buttons;
std::map<std::string, UIRectangleShape> UIManager::m_rectangleShapes;

void UIManager::Clear()
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_labels.clear();
    Logger::StackPop();
}

// ------------------------------------------------------
// Labels
// ------------------------------------------------------
void UIManager::AddLabel( const std::string& key, UILabel label )
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_labels[ key ] = label;
    Logger::StackPop();
}

void UIManager::AddLabel( const std::string& key, const std::string& fontName, int characterSize, sf::Color fillColor, sf::Vector2f position, const std::string& text  )
{
    Logger::StackPush( s_className + "::" + __func__ );
    UILabel label;
    label.Setup( key, fontName, characterSize, fillColor, position, text );
    m_labels[ key ] = label;
    Logger::StackPop();
}

UILabel& UIManager::GetLabel( const std::string& key )
{
    return m_labels[ key ];
}

std::map<std::string, UILabel>& UIManager::GetLabels()
{
    return m_labels;
}


// ------------------------------------------------------
// Images
// ------------------------------------------------------
void UIManager::AddImage( const std::string& key, UIImage image )
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_images[ key ] = image;
    Logger::StackPop();
}

void UIManager::AddImage( const std::string& key, const sf::Texture& texture, sf::Vector2f position )
{
    Logger::StackPush( s_className + "::" + __func__ );
    UIImage image;
    image.Setup( key, texture, position );
    m_images[ key ] = image;
    Logger::StackPop();
}

UIImage& UIManager::GetImage( const std::string& key )
{
    return m_images[ key ];
}

std::map<std::string, UIImage>& UIManager::GetImages()
{
    return m_images;
}


// ------------------------------------------------------
// Buttons
// ------------------------------------------------------
void UIManager::AddButton( const std::string& key, UIButton button )
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_buttons[ key ] = button;
    Logger::StackPop();
}

UIButton& UIManager::GetButton( const std::string& key )
{
    return m_buttons[ key ];
}

std::map<std::string, UIButton>& UIManager::GetButtons()
{
    return m_buttons;
}

// ------------------------------------------------------
// Geometry
// ------------------------------------------------------

void UIManager::AddRectangle( const std::string& key, sf::RectangleShape shape )
{
    Logger::StackPush( s_className + "::" + __func__ );
    UIRectangleShape rectShape;
    rectShape.Setup( key, shape );
    m_rectangleShapes[ key ] = rectShape;
    Logger::StackPop();
}

void UIManager::AddRectangle( const std::string& key, sf::Color borderColor, int borderThickness, sf::Color fillColor, sf::Vector2f position, sf::Vector2f size )
{
    Logger::StackPush( s_className + "::" + __func__ );
    UIRectangleShape shape;
    shape.Setup( key, borderColor, borderThickness, fillColor, position, size );
    m_rectangleShapes[ key ] = shape;
    Logger::StackPop();
}

UIRectangleShape& UIManager::GetRectangle( const std::string& key )
{
    return m_rectangleShapes[ key ];
}

std::map<std::string, UIRectangleShape>& UIManager::GetRectangles()
{
    return m_rectangleShapes;
}

std::string UIManager::GetClickedButton()
{
    if ( sf::Mouse::isButtonPressed(sf::Mouse::Left) )
    {
        sf::Vector2i localPosition = sf::Mouse::getPosition( Application::GetWindow() );

        for ( auto& button : m_buttons )
        {
            if ( SFMLHelper::PointInBoxCollision( localPosition, button.second.GetPositionRect() ) )
            {
                return button.second.GetId();
            }
        }
    }

    return "";
}

void UIManager::RemoveElementsWith( const std::string& str )
{
    Logger::StackPush( s_className + "::" + __func__ );
    std::vector<std::string> elementNames;

    // Labels
    for ( auto& element : m_labels )
    {
        if ( Helper::Contains( element.first, str ) )
        {
            // Remove this element
            elementNames.push_back( element.first );
        }
    }

    for ( auto& key : elementNames )
    {
        m_labels.erase( key );
    }

    elementNames.clear();

    // Buttons
    for ( auto& element : m_buttons )
    {
        if ( Helper::Contains( element.first, str ) )
        {
            // Remove this element
            elementNames.push_back( element.first );
        }
    }

    for ( auto& key : elementNames )
    {
        m_buttons.erase( key );
    }

    elementNames.clear();
    Logger::StackPop();
}

}
