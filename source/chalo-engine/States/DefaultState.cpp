#include "DefaultState.hpp"

#include "../Application/Application.hpp"
#include "../Managers/InputManager.hpp"
#include "../Managers/FontManager.hpp"
#include "../Managers/MenuManager.hpp"
#include "../Managers/StateManager.hpp"
#include "../Utilities/Messager.hpp"
#include "../Utilities/Logger.hpp"

#include <string>

namespace chalo
{

std::string DefaultState::s_className = "DefaultState";

DefaultState::DefaultState()
{
}

void DefaultState::Init( const std::string& name )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Parameters - name: " + name, "DefaultState::Init", "function-trace" );
    m_gotoState = "";
    Logger::StackPop();
}

void DefaultState::Setup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    IState::Setup();
    chalo::InputManager::Setup();
    m_background.setSize( sf::Vector2f( 600, 600 ) );
    m_background.setFillColor( sf::Color::Blue );

    sf::Text errorMessage;
    errorMessage.setFont( chalo::FontManager::Get( "main" ) );
    errorMessage.setCharacterSize( 15 );
    errorMessage.setFillColor( sf::Color::White );

    int x = 0, y = 0, inc = 25;

    errorMessage.setPosition( x, y ); y += inc;
    errorMessage.setString( "Something went very wrong." );
    m_errorMessages.push_back( errorMessage );

    errorMessage.setPosition( x, y ); y += inc;
    errorMessage.setString( "Instead of crashing, you get this." );
    m_errorMessages.push_back( errorMessage );

    errorMessage.setPosition( x, y ); y += inc;
    errorMessage.setString( "Please check log.html for more info." );
    m_errorMessages.push_back( errorMessage );

    errorMessage.setPosition( x, y ); y += inc;
    errorMessage.setString( "MESSAGE:" );
    m_errorMessages.push_back( errorMessage );

    errorMessage.setPosition( x, y ); y += inc;
    errorMessage.setString( chalo::Messager::Get( "ERROR" ) );
    m_errorMessages.push_back( errorMessage );

    errorMessage.setPosition( x, y ); y += inc;
    errorMessage.setString( "Press ENTER to try to restart." );
    m_errorMessages.push_back( errorMessage );

    Logger::StackPop();
}

void DefaultState::Cleanup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    chalo::MenuManager::Cleanup();
    Logger::StackPop();
}

void DefaultState::Update()
{
    if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Enter ) )
    {
        chalo::StateManager::ChangeState( "startupState" );
    }
}

void DefaultState::Draw( sf::RenderTexture& window )
{
    window.draw( m_background );

    for ( auto& msg : m_errorMessages )
    {
        window.draw( msg );
    }
}

}

