// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef _ISTATE
#define _ISTATE

#include <SFML/Graphics.hpp>

#include <string>

namespace chalo
{

class IState
{
public:
    IState() { }
    virtual ~IState() { }
    virtual void Init( const std::string& name ) = 0;   // This is to set up the State's name, for use with the StateManager
    virtual void Setup() = 0;                           // Set up/restart the game state
    virtual void Cleanup() = 0;
    virtual void Update() = 0;
//    virtual void Draw( sf::RenderWindow& window ) = 0;
    virtual void Draw( sf::RenderTexture& window ) = 0;

    std::string GetName();
    std::string GetGotoState();
    void SetGotoState( const std::string& stateName );

protected:
    std::string m_name;
    std::string m_gotoState;

private:
    static std::string s_className;
};

}

#endif
