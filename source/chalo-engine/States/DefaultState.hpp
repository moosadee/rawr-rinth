#ifndef _DEFAULT_STATE
#define _DEFAULT_STATE

#include "IState.hpp"

#include <vector>

namespace chalo
{

class DefaultState : public chalo::IState
{
public:
    DefaultState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderTexture& window );

private:
    sf::RectangleShape m_background;
    std::vector<sf::Text> m_errorMessages;

private:
    static std::string s_className;
};

}

#endif
