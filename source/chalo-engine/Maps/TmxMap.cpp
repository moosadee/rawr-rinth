#include "TmxMap.hpp"

#include "../Utilities/Logger.hpp"

std::string TmxMap::s_className = "TmxMap";

void TmxMap::Load( std::string path, std::string mapFile )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Load TmxMap. path: \"" + path + "\", mapFile: \"" + mapFile + "\"", s_className + "::" + __func__ );

    XmlDocument mapXml = XmlParser::Parse( path + mapFile );
    mapXml.Debug();

    // <map version="1.2" tiledversion="1.3.2" orientation="orthogonal" renderorder="right-down" compressionlevel="0" width="20" height="12" tilewidth="20" tileheight="20" infinite="0" nextlayerid="5" nextobjectid="1">
    XmlElement mapData = mapXml.FindElementsWithPath( "map/" )[0];
    m_mapVersion    = mapData.attributes["version"];
    m_tiledVersion  = mapData.attributes["tiledversion"];
    m_width         = Helper::StringToInt( mapData.attributes["width"] );
    m_height        = Helper::StringToInt( mapData.attributes["height"] );
    m_tileWidth     = Helper::StringToInt( mapData.attributes["tilewidth"] );
    m_tileHeight    = Helper::StringToInt( mapData.attributes["tileheight"] );

    // <tileset firstgid="1" source="sheet-terrain.tsx"/>
    std::vector<XmlElement> tilesetData = mapXml.FindElementsWithPath( "map/tileset/" );
    for ( auto& t : tilesetData )
    {
        TmxTileset tt;
        tt.firstGid = Helper::StringToInt( t.attributes["firstgid"] );
        tt.tsxSource = t.attributes["source"];
        Logger::Out( "Tileset source: \"" + path + tt.tsxSource + "\"", s_className + "::" + __func__ );

        // Gotta load the tileset data, too. -_-
        XmlDocument tilesetDoc = XmlParser::Parse( path + tt.tsxSource );
        tilesetDoc.Debug();

        XmlElement tilesetData = tilesetDoc.FindElementsWithPath( "tileset/" )[0];
        tt.columns      = Helper::StringToInt( tilesetData.attributes["columns"] );
        tt.tileCount    = Helper::StringToInt( tilesetData.attributes["tileCount"] );
        tt.width        = Helper::StringToInt( tilesetData.attributes["width"] );
        tt.height       = Helper::StringToInt( tilesetData.attributes["height"] );
        tt.tileWidth    = Helper::StringToInt( tilesetData.attributes["columns"] );
        tt.tileHeight   = Helper::StringToInt( tilesetData.attributes["columns"] );

        XmlElement tilesetImageData = tilesetDoc.FindElementsWithPath( "tileset/image/" )[0];
        tt.imageWidth   = Helper::StringToInt( tilesetImageData.attributes["width"] );
        tt.imageHeight  = Helper::StringToInt( tilesetImageData.attributes["height"] );
        tt.imgSource    = tilesetImageData.attributes["source"];
        Logger::Out( "Tileset image source: \"" + tt.imgSource + "\"", s_className + "::" + __func__ );

        m_tilesets.push_back( tt );
    }

    // <layer id="1" name="FLOOR-LAYER" width="20" height="12">
    // <data encoding="csv">
    std::vector<XmlElement> layerData = mapXml.FindElementsWithPath( "map/layer/" );
    std::vector<XmlElement> dataData = mapXml.FindElementsWithPath( "map/layer/data/" );
    if ( layerData.size() != dataData.size() )
    {
        Logger::Error( "WARNING: Malformed data? Amount of 'layer' and 'data' elements should match.", s_className + "::" + __func__ );
    }
    for ( size_t i = 0; i < layerData.size(); i++ )
    {
        TmxMapLayer layer;
        layer.name      = layerData[i].attributes["name"];
        layer.id        = Helper::StringToInt( layerData[i].attributes["id"] );
        layer.width     = Helper::StringToInt( layerData[i].attributes["width"] );
        layer.height    = Helper::StringToInt( layerData[i].attributes["height"] );
        for ( auto& line : dataData[i].data )
        {
            std::vector<std::string> values = Helper::Split( line, "," );
            for ( auto& v : values )
            {
                if ( v == "" ) { continue; }
                layer.data.push_back( Helper::StringToInt( v ) );
            }
        }
        m_layers.push_back( layer );
    }

    Logger::StackPop();
}

void TmxMap::Debug()
{
    Logger::StackPush( s_className + "::" + __func__ );

    std::string mapInfo = "MAP INFO...";
    mapInfo += " <br> m_mapVersion: " + m_mapVersion;
    mapInfo += " <br> m_tiledVersion: " + m_tiledVersion;
    mapInfo += " <br> m_width: " + Helper::ToString( m_width );
    mapInfo += " <br> m_height: " + Helper::ToString( m_height );
    mapInfo += " <br> m_tileWidth: " + Helper::ToString( m_tileWidth );
    mapInfo += " <br> m_tileHeight: " + Helper::ToString( m_tileHeight );

    mapInfo += "<br><br> TILESET DATA...";
    for ( auto& t : m_tilesets )
    {
        mapInfo += "<br> tsxSource: \"" + t.tsxSource + "\"";
        mapInfo += "<br> imgSource: \"" + t.imgSource + "\"";
        mapInfo += "<br> firstgid: " + Helper::ToString( t.firstGid );
        mapInfo += "<br> width: " + Helper::ToString( t.width );
        mapInfo += "<br> height: " + Helper::ToString( t.height );
        mapInfo += "<br> tileWidth: " + Helper::ToString( t.tileWidth );
        mapInfo += "<br> tileHeight: " + Helper::ToString( t.tileHeight );
        mapInfo += "<br> imageWidth: " + Helper::ToString( t.imageWidth );
        mapInfo += "<br> imageHeight: " + Helper::ToString( t.imageWidth );
        mapInfo += "<br> tileCount: " + Helper::ToString( t.tileCount );
        mapInfo += "<br> columns: " + Helper::ToString( t.columns );
        mapInfo += "<br>";
    }

    mapInfo += "<br><br> LAYERS...";
    for ( auto& l : m_layers )
    {
        mapInfo += " <br> id: " + Helper::ToString( l.id );
        mapInfo += " <br> width: " + Helper::ToString( l.width );
        mapInfo += " <br> height: " + Helper::ToString( l.height );
        mapInfo += " <br> name: " + l.name;
        mapInfo += " <br> data: ";
        for ( auto& d : l.data )
        {
            mapInfo += Helper::ToString( d ) + " ";
        }
        mapInfo += "<br>";
    }

    Logger::Debug( mapInfo, s_className + "::" + __func__ );

    Logger::StackPop();
}
