#ifndef _CHALO_MAP_HPP
#define _CHALO_MAP_HPP

#include "TmxMap.hpp"
#include "ChaloTile.hpp"

#include <SFML/Graphics.hpp>

#include <vector>
#include <map>

struct ChaloMapLayer
{
    std::string name;
    std::vector<ChaloTile> tiles;
    sf::IntRect mapDimensions;
};

struct ChaloMapObjectData
{
    std::string name;
    int tmxId;
    sf::Vector2i position;
};

class ChaloMap
{
public:
    ChaloMap();

    void ConvertFrom( const TmxMap& tmxMap );
    std::string GetTilesetName();
    void Draw( sf::RenderTexture& window );
    void DrawDebug( sf::RenderTexture& window );
    void Clear();

    sf::Vector2i GetMapDimensions();

    std::vector<ChaloMapObjectData> GetObjectData();
    std::vector<ChaloTile> GetSolidWalls();
    std::vector<sf::IntRect>& GetSolidTileRegions();

    void Debug();

    // Editing
    void SetTileDimensions( sf::Vector2i dimensions );
    void SetMapDimensions( sf::Vector2i dimensions );
    void SetTilesetName( std::string name );
    void SetObjectsetName( std::string name );
    void LoadObjectTypes( std::string path );
    void SetGridLines( bool gridlines );
    void AddMapLayer( std::string name );
    void AddTileToLayer( std::string layerName, ChaloTile tile );

private:
    static std::string s_className;

    std::string m_tilesetName;
    std::string m_objectsetName;
    sf::Vector2i m_tileDimensions;
    sf::Vector2i m_mapDimensions;
    std::vector<ChaloMapLayer> m_layers;
    std::vector<ChaloMapObjectData> m_objects; // map doesn't store objects, just has object data from map
    std::map<int, std::string> m_objectTypes;
    std::vector<sf::IntRect> m_solidTileRegions;

    // Editing
    bool m_gridlinesEnabled;

    void ConvertTmxTerrain();
    void ConvertTmxObjects();
};

#endif
