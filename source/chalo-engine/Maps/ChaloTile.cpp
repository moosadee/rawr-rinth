#include "ChaloTile.hpp"

#include "../Utilities/Logger.hpp"
#include "../Utilities_SFML/SFMLHelper.hpp"
#include "../Utilities/Helper.hpp"

#include <string>

ChaloTile& ChaloTile::operator=( ChaloTile other )
{
    if ( this == &other ) { return *this; }

    m_position = other.m_position;
    m_dimensions = other.m_dimensions;
    m_frameRect = other.m_frameRect;
    m_sprite = other.m_sprite;
    m_name = other.m_name;
    m_canWalkOn = other.m_canWalkOn;

    return *this;
}

void ChaloTile::SetTexture( const sf::Texture& texture )
{
    m_sprite.setTexture( texture );
}

void ChaloTile::SetPosition( sf::Vector2f position )
{
    m_position = position;
    m_sprite.setPosition( position );
}

void ChaloTile::SetPositionX( float value )
{
    m_position.x = value;
}

void ChaloTile::SetPositionY( float value )
{
    m_position.y = value;
}

void ChaloTile::SetFrameRect( sf::IntRect frameRect )
{
    m_frameRect = frameRect;
    m_sprite.setTextureRect( m_frameRect );
}

void ChaloTile::SetFrameRectDimensions( int width, int height )
{
    m_frameRect.width = width;
    m_frameRect.height = height;
    m_sprite.setTextureRect( m_frameRect );
}

void ChaloTile::SetFrameRectLeft( int value )
{
    m_frameRect.left = value;
    m_sprite.setTextureRect( m_frameRect );
}

void ChaloTile::SetFrameRectTop( int value )
{
    m_frameRect.top = value;
    m_sprite.setTextureRect( m_frameRect );
}

void ChaloTile::SetDimensions( sf::IntRect dimensions )
{
    m_dimensions = dimensions;
}

void ChaloTile::SetName( std::string value )
{
    m_name = value;
}

void ChaloTile::SetCanWalkOn( bool value )
{
    m_canWalkOn = value;
}

void ChaloTile::SetLayerIndex( int value )
{
    m_layerIndex = value;
}

sf::Sprite ChaloTile::GetSprite()
{
    m_sprite.setPosition( m_position );
    m_sprite.setTextureRect( m_frameRect );
    return m_sprite;
}

sf::RectangleShape ChaloTile::GetDebugRectangle()
{
    m_debugShape.setPosition( m_position );
    m_debugShape.setSize( sf::Vector2f( m_dimensions.width, m_dimensions.height ) );

    m_debugShape.setFillColor( sf::Color( 0, 0, 0, 0 ) );
    if ( !m_canWalkOn )
    {
        if ( m_layerIndex == 0 )
        {
            m_debugShape.setFillColor( sf::Color( 255, 0, 0, 100 ) );
        }
        else if ( m_layerIndex == 1 )
        {
            m_debugShape.setFillColor( sf::Color( 0, 255, 0, 100 ) );
        }
        else if ( m_layerIndex == 2 )
        {
            m_debugShape.setFillColor( sf::Color( 0, 0, 255, 100 ) );
        }
    }

    return m_debugShape;
}

sf::IntRect ChaloTile::GetCollisionRegion()
{
    sf::IntRect collisionRegion;
    collisionRegion.left = m_position.x;
    collisionRegion.top = m_position.y;
    collisionRegion.width = m_dimensions.width;
    collisionRegion.height = m_dimensions.height;
    return collisionRegion;
}

const sf::Texture* ChaloTile::GetTexture()
{
    return m_sprite.getTexture();
}

sf::IntRect ChaloTile::GetFrameRect()
{
    return m_frameRect;
}

sf::Vector2f ChaloTile::GetPosition()
{
    return m_position;
}

std::string ChaloTile::GetName()
{
    return m_name;
}

bool ChaloTile::GetCanWalkOn()
{
    return m_canWalkOn;
}

float ChaloTile::GetPositionX()
{
    return m_position.x;
}

float ChaloTile::GetPositionY()
{
    return m_position.y;
}

sf::IntRect ChaloTile::GetDimensions()
{
    return m_dimensions;
}

sf::Vector2f ChaloTile::GetDimensionsVector()
{
    sf::Vector2f vec;
    vec.x = m_dimensions.width;
    vec.y = m_dimensions.height;
    return vec;
}

int ChaloTile::GetDimensionsWidth()
{
    return m_dimensions.width;
}

int ChaloTile::GetDimensionsHeight()
{
    return m_dimensions.height;
}

int ChaloTile::GetFrameRectTop()
{
    return m_frameRect.top;
}

int ChaloTile::GetFrameRectLeft()
{
    return m_frameRect.left;
}

int ChaloTile::GetFrameRectWidth()
{
    return m_frameRect.width;
}

int ChaloTile::GetFrameRectHeight()
{
    return m_frameRect.height;
}

void ChaloTile::Debug()
{
    Logger::StackPush( "Tile::Debug" );
    Logger::Out( "m_name: " + m_name
                + "<br>m_position: " + SFMLHelper::CoordinateToString( m_position )
                + "<br>m_dimensions: " + SFMLHelper::RectangleToString( m_dimensions )
                + "<br>m_frameRect: " + SFMLHelper::RectangleToString( m_frameRect )
                + "<br>m_canWalkOn: " + Helper::ToString( m_canWalkOn )
                , "Tile::Debug" );
    Logger::StackPop();
}

