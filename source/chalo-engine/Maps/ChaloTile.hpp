#ifndef _TILE_HPP
#define _TILE_HPP

#include <SFML/Graphics.hpp>
#include <string>

struct ChaloTile
{
public:
    ChaloTile& operator=( ChaloTile other );

    void SetFrameRect( sf::IntRect frameRect );
    void SetFrameRectDimensions( int width, int height );
    void SetFrameRectLeft( int value );
    void SetFrameRectTop( int value );
    void SetDimensions( sf::IntRect dimensions );
    void SetTexture( const sf::Texture& texture );
    void SetPosition( sf::Vector2f position );
    void SetPositionX( float value );
    void SetPositionY( float value );
    void SetName( std::string value );
    void SetCanWalkOn( bool value );
    void SetLayerIndex( int value );

    sf::Sprite GetSprite();
    sf::RectangleShape GetDebugRectangle();
    sf::IntRect GetCollisionRegion();
    const sf::Texture* GetTexture();
    sf::IntRect GetFrameRect();
    sf::Vector2f GetPosition();
    std::string GetName();
    bool GetCanWalkOn();
    float GetPositionX();
    float GetPositionY();
    sf::IntRect GetDimensions();
    sf::Vector2f GetDimensionsVector();
    int GetDimensionsWidth();
    int GetDimensionsHeight();
    int GetFrameRectTop();
    int GetFrameRectLeft();
    int GetFrameRectWidth();
    int GetFrameRectHeight();

    void Debug();

private:
    sf::Vector2f m_position;
    sf::IntRect m_dimensions;
    sf::IntRect m_frameRect;
    sf::Sprite m_sprite;
    sf::RectangleShape m_debugShape;
    int m_layerIndex;
    std::string m_name;
    bool m_canWalkOn;
    int m_tmxIndex;
    int m_tmxTileId;

    friend class ChaloMap;
};

#endif
