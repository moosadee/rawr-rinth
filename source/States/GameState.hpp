#ifndef _GAMESTATE
#define _GAMESTATE

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/UI/UIImage.hpp"
#include "../chalo-engine/UI/UIButton.hpp"
#include "../chalo-engine/UI/UILabel.hpp"
#include "../chalo-engine/Maps/TmxMap.hpp"
#include "../chalo-engine/Maps/ChaloMap.hpp"

#include <vector>

#include "../Objects/Enums.hpp"
#include "../Objects/Object.hpp"

class GameState : public chalo::IState
{
public:
    GameState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderTexture& window );

private:
    static std::string s_className;

    void Init_Map();
    void Init_Object( ChaloMapObjectData object );
    void Init_LevelEndMenu();
    void Draw_Map( sf::RenderTexture& window );
    void Draw_Objects( sf::RenderTexture& window );
    void HandleInput();
    void HandleMouse();
    void LevelOver();
    void GotoNextLevel();
    void RestartLevel();
    void CheckForLevelEndState();

    void PopStackGoBack();
    void PushStateToStack();

    int m_playerIndex;
    int m_exitIndex;

    int m_totalEggs;
    int m_rewindCount;

    float m_inputCooldown;
    float m_inputCooldownMax;

    ChaloMap m_map;

    std::vector<Object> m_allObjects;
    std::stack< std::vector< ObjectState > > m_turnStack;
    Object& GetPlayer();

    sf::RectangleShape m_deadScreen;
    bool m_playerDead;
    bool m_levelEnd;

    const int MAP_WIDTH;
    const int MAP_HEIGHT;
    const int TILE_WIDTH;
    const int TILE_HEIGHT;
    const int TILESET_WIDTH;
    const int TILESET_HEIGHT;
};

#endif
