#include "HelpState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Managers/AudioManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
//#include "../chalo-engine/Utilities/Platform.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"

std::string HelpState::s_className = "HelpState";

HelpState::HelpState()
{
}

void HelpState::Init( const std::string& name )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Debug( "Parameters - name: " + name, s_className + "::" + __func__ );
    Logger::StackPop();
}

void HelpState::Setup()
{
    Logger::StackPush( s_className + "::" + __func__ );

    Logger::Out( "", "HelpState::Setup", "function-trace" );
    IState::Setup();
//    CursorState::Setup();

    chalo::TextureManager::Add( "menu_bg",                 "Content/Graphics/UI/menu_bg.png" );
    chalo::TextureManager::Add( "menu_icons",              "Content/Graphics/UI/menu_icons.png" );
    chalo::TextureManager::Add( "button_bg",               "Content/Graphics/UI/button_bg_2.png" );
    chalo::TextureManager::Add( "button_bg_square",        "Content/Graphics/UI/button_bg_square_2.png" );
    chalo::TextureManager::Add( "options_menu_buttons",    "Content/Graphics/UI/options_menu_buttons.png" );
    chalo::TextureManager::Add( "scrollbar",               "Content/Graphics/UI/scrollbar.png" );

    chalo::MenuManager::LoadTextMenu( "help.chalomenu" );

    sf::Vector2f pos( 360, 700 );
    sf::IntRect dimensions( 0, 0, 300, 35 );

    chalo::InputManager::Setup();

    Logger::StackPop();
}

void HelpState::Cleanup()
{
    Logger::StackPush( s_className + "::" + __func__ );

    Logger::Out( "", "HelpState::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();

    Logger::StackPop();
}

void HelpState::Update()
{
    Logger::StackPush( s_className + "::" + __func__ );

    chalo::MenuManager::Update();

    std::string clickedButton = chalo::MenuManager::GetClickedButton();

    if ( clickedButton == "btnBack" )
    {
        SetGotoState( "startupState" );
    }

    Logger::StackPop();
}

void HelpState::Draw( sf::RenderTexture& window )
{
    chalo::MenuManager::Draw( window );
}



