#ifndef _LEVELSELECTSTATE
#define _LEVELSELECTSTATE

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/UI/UIImage.hpp"
#include "../chalo-engine/UI/UILabel.hpp"

#include <vector>

class LevelSelectState : public chalo::IState
{
public:
    LevelSelectState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderTexture& window );

    void ArrangeIcons( int world );
    void ClickLevel( std::string buttonKey );
    void GotoLevel();

private:
    static std::string s_className;
    int m_selectedWorld;
    int m_selectedLevel;
    std::vector<std::string> m_buttonKeys;
};

#endif
