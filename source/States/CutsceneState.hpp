#ifndef _CutsceneState
#define _CutsceneState

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/UI/UIImage.hpp"
#include "../chalo-engine/UI/UILabel.hpp"

#include <vector>

class CutsceneState : public chalo::IState
{
public:
    CutsceneState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderTexture& window );

    void NextPage();
    void RestartCutscene();

private:
    static std::string s_className;

    std::vector<sf::Sprite> m_sprites;
    std::vector<sf::Text> m_text;

    int m_slide;
    int m_cutsceneId;
    float m_clickCooldown;
    float m_titleAnimationCounter;
};

#endif
