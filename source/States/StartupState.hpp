#ifndef _STARTUPSTATE
#define _STARTUPSTATE

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/UI/UIImage.hpp"
#include "../chalo-engine/UI/UILabel.hpp"

#include <vector>

class StartupState : public chalo::IState
{
public:
    StartupState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderTexture& window );

private:
    static std::string s_className;

    sf::Sprite m_titleText;
    float m_titleAnimationCounter;

    sf::Sprite m_portal1;
    sf::Sprite m_portal2;
};

#endif
