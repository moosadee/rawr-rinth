#include "PlaydateStartupState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Managers/LanguageManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
//#include "../chalo-engine/Utilities/Platform.hpp"

PlaydateStartupState::PlaydateStartupState()
{
}

void PlaydateStartupState::Init( const std::string& name )
{
    Logger::Out( "Parameters - name: " + name, "PlaydateStartupState::Init", "function-trace" );
}

void PlaydateStartupState::Setup()
{
    Logger::Out( "", "PlaydateStartupState::Setup", "function-trace" );
    IState::Setup();

    chalo::TextureManager::Add( "bg",                      "Content/Graphics/Playdate_UI/menubg.png" );
    chalo::TextureManager::Add( "logo",                    "Content/Graphics/Playdate_UI/logo-moosadee-small.png" );
    chalo::TextureManager::Add( "button-long",             "Content/Graphics/Playdate_UI/button-long.png" );
    chalo::TextureManager::Add( "button-square",           "Content/Graphics/Playdate_UI/button-square.png" );
    chalo::TextureManager::Add( "menu-icons",              "Content/Graphics/Playdate_UI/menu-icons.png" );
    chalo::TextureManager::Add( "cursor",                  "Content/Graphics/Playdate_UI/cursor.png" );

    m_cursor.setTexture( chalo::TextureManager::Get( "cursor" ) );
    m_cursor.setPosition( 33, 55 );
    m_cursor.setTextureRect( sf::IntRect( 0, 0, 40, 40 ) );

    chalo::MenuManager::LoadTextMenu( "pdstartup.chalomenu" );

    chalo::InputManager::Setup();
}

void PlaydateStartupState::Cleanup()
{
    Logger::Out( "", "PlaydateStartupState::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
}

void PlaydateStartupState::Update()
{
//    CursorState::Update();

    std::string clickedButton = chalo::MenuManager::GetClickedButton();
//    std::string gamepadButton = chalo::MenuManager::GetHoveredButton( m_cursors[0].GetPosition() );

    // Main menu
    if ( clickedButton == "btnPlay" )//||
//        ( gamepadButton == "btnPlay" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        SetGotoState( "rawrstate" );

    }
    else if ( clickedButton == "btnOptions" )//||
//        ( gamepadButton == "btnOptions" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        SetGotoState( "optionsstate" );
    }
    else if ( clickedButton == "btnHelp" )//||
//        ( gamepadButton == "btnHelp" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        SetGotoState( "helpstate" );
    }
    else if ( clickedButton == "btnLanguage" )//||
//        ( gamepadButton == "btnLanguage" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        SetGotoState( "languagestate" );
    }
    else if ( clickedButton == "btnQuit" )//||
//        ( gamepadButton == "btnQuit" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        chalo::Application::ReadyToQuit();
    }
}

void PlaydateStartupState::Draw( sf::RenderWindow& window )
{
//    CursorState::Draw( window );
}



