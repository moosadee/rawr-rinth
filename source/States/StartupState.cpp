#include "StartupState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/AudioManager.hpp"
//#include "../chalo-engine/Managers/ConfigManager.hpp"
//#include "../chalo-engine/Managers/LanguageManager.hpp"
//#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"

std::string StartupState::s_className = "StartupState";

StartupState::StartupState()
{
}

void StartupState::Init( const std::string& name )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Debug( "Parameters - name: " + name, s_className + "::" + __func__ );
    Logger::StackPop();
}

void StartupState::Setup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    IState::Setup();
    chalo::InputManager::Setup();

    // Load needed assets
    chalo::TextureManager::AddAndGet( "title_art", "Content/Graphics/StartupState/titlebg.png" );
    chalo::TextureManager::AddAndGet( "title_art", "Content/Graphics/UI/menu_bg_title.png" );
    chalo::TextureManager::AddAndGet( "button_bg", "Content/Graphics/UI/button_bg.png" );
    chalo::TextureManager::AddAndGet( "menu_icons", "Content/Graphics/UI/menu_icons.png" );
    chalo::TextureManager::AddAndGet( "desktop_icons", "Content/Graphics/UI/desktop_icons.png" );

    m_titleText.setTexture( chalo::TextureManager::AddAndGet( "title_text", "Content/Graphics/UI/title_text.png" ) );
    m_portal1.setTexture( chalo::TextureManager::AddAndGet( "portal1", "Content/Graphics/UI/portal1.png" ) );
    m_portal2.setTexture( chalo::TextureManager::AddAndGet( "portal2", "Content/Graphics/UI/portal2.png" ) );

    m_titleText.setOrigin( sf::Vector2f( 335/2, 42/2 ) );
    m_titleText.setPosition( sf::Vector2f( 200, 25 ) );
    m_titleAnimationCounter = 1;

    m_portal1.setOrigin( sf::Vector2f( 75/2, 75/2 ) );
    m_portal1.setPosition( sf::Vector2f( 200, 100 ) );

    m_portal2.setOrigin( sf::Vector2f( 75/2, 75/2 ) );
    m_portal2.setPosition( sf::Vector2f( 200, 100 ) );

    // Load menu file
    chalo::MenuManager::LoadTextMenu( "startup.chalomenu" );

    // Load theme song
    chalo::AudioManager::AddMusic( "title_screen", "Content/Audio/SuburbanDinosaur_Moosader.ogg" );
    chalo::AudioManager::PlayMusic( "title_screen" );

    Logger::StackPop();
}

void StartupState::Cleanup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    chalo::MenuManager::Cleanup();
    Logger::StackPop();
}

void StartupState::Update()
{
    chalo::MenuManager::Update();
    std::string clickedButton = chalo::MenuManager::GetClickedButton();

    if ( clickedButton == "btnPlay" )
    {
        SetGotoState( "worldSelectState" );
    }
    else if ( clickedButton == "btnMapEditor" )
    {
        SetGotoState( "mapEditorState" );
    }
    else if ( clickedButton == "btnOptions" )
    {
        SetGotoState( "optionsState" );
    }
    else if ( clickedButton == "btnHelp" )
    {
        SetGotoState( "helpState" );
    }
    else if ( clickedButton == "btnEditor" )
    {
        SetGotoState( "levelEditorState" );
    }
    else if ( clickedButton == "btnExit" )
    {
        chalo::Application::ReadyToQuit();
    }

    sf::Vector2f pos = m_titleText.getPosition();
    float modifyPos = pos.y;
    float modifyRotation = m_portal2.getRotation();
    float modifyScale = m_portal1.getScale().x;

    float posRate = 0.01;
    float rotationRate = 4;
    float scaleRate = 0.001;

    m_titleAnimationCounter += 0.5;
    if ( m_titleAnimationCounter >= 360 )
    {
        m_titleAnimationCounter = 0;
    }

    if      ( m_titleAnimationCounter < 90 )  { modifyScale += scaleRate; modifyRotation += rotationRate; modifyPos += posRate; }
    else if ( m_titleAnimationCounter < 180 ) { modifyScale -= scaleRate; modifyRotation += rotationRate; modifyPos -= posRate; }
    else if ( m_titleAnimationCounter < 270 ) { modifyScale -= scaleRate; modifyRotation += rotationRate; modifyPos -= posRate; }
    else if ( m_titleAnimationCounter < 360 ) { modifyScale += scaleRate; modifyRotation += rotationRate; modifyPos += posRate; }

    m_titleText.setPosition( pos.x, modifyPos );
    m_portal2.setRotation( modifyRotation );
    m_portal1.setScale( modifyScale, modifyScale );
}

void StartupState::Draw( sf::RenderTexture& window )
{
    chalo::MenuManager::Draw( window );

    window.draw( m_portal1 );
    window.draw( m_portal2 );
    window.draw( m_titleText );
}



