#ifndef _WORLDSELECTSTATE
#define _WORLDSELECTSTATE

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/UI/UIImage.hpp"
#include "../chalo-engine/UI/UILabel.hpp"

#include <vector>

class WorldSelectState : public chalo::IState
{
public:
    WorldSelectState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderTexture& window );

    void SelectWorld( int world );
    void GotoWorld();

private:
    static std::string s_className;
    int m_selectedWorld;
};

#endif
