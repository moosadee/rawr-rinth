#include "LanguageSelectState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Managers/LanguageManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"

LanguageSelectState::LanguageSelectState()
{
}

void LanguageSelectState::Init( const std::string& name )
{
    Logger::Out( "Parameters - name: " + name, "LanguageSelectState::Init", "function-trace" );
//    CursorState::Init( name, true );
}

void LanguageSelectState::Setup()
{
    Logger::Out( "", "LanguageSelectState::Setup", "function-trace" );
    IState::Setup();
//    CursorState::Setup();

    chalo::TextureManager::Add( "bg",                      "Content/Graphics/UI/menubg3.png" );
    chalo::TextureManager::Add( "button-long",             "Content/Graphics/UI/button-long.png" );
    chalo::TextureManager::Add( "button-long-selected",    "Content/Graphics/UI/button-long-selected.png" );
    chalo::TextureManager::Add( "button-square",           "Content/Graphics/UI/button-square.png" );
    chalo::TextureManager::Add( "button-square-selected",  "Content/Graphics/UI/button-square-selected.png" );

    chalo::MenuManager::LoadTextMenu( "language.chalomenu" );

    sf::Vector2f pos( 360, 700 );
    sf::IntRect dimensions( 0, 0, 300, 35 );

    chalo::InputManager::Setup();
}

void LanguageSelectState::Cleanup()
{
    Logger::Out( "", "LanguageSelectState::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
}

void LanguageSelectState::Update()
{
//    CursorState::Update();

    std::string clickedButton = chalo::MenuManager::GetClickedButton();

    // Helper language select
    bool selectedHelperLanguage = true;
    if ( clickedButton == "btnLanguageEnglish" )//||
//        ( gamepadButton == "btnLanguageEnglish" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        chalo::ConfigManager::Set( "LANGUAGE_MAIN", "en" );
        chalo::ConfigManager::Set( "LANGUAGE_MAIN_FONT", "main" );
        chalo::LanguageManager::AddLanguage( chalo::ConfigManager::Get( "LANGUAGE_MAIN" ), chalo::ConfigManager::Get( "LANGUAGE_MAIN_FONT" ) );
        //SetGotoState( "startupstate" );
    }
    else if ( clickedButton == "btnLanguageEsperanto" )//||
//        ( gamepadButton == "btnLanguageEsperanto" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        chalo::ConfigManager::Set( "LANGUAGE_MAIN", "eo" );
        chalo::ConfigManager::Set( "LANGUAGE_MAIN_FONT", "main" );
        chalo::LanguageManager::AddLanguage( chalo::ConfigManager::Get( "LANGUAGE_MAIN" ), chalo::ConfigManager::Get( "LANGUAGE_MAIN_FONT" ) );
        //SetGotoState( "startupstate" );
    }
    else if ( clickedButton == "btnLanguageHindi" )//||
//        ( gamepadButton == "btnLanguageHindi" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        chalo::ConfigManager::Set( "LANGUAGE_MAIN", "hi" );
        chalo::ConfigManager::Set( "LANGUAGE_MAIN_FONT", "devanagari" );
        chalo::LanguageManager::AddLanguage( chalo::ConfigManager::Get( "LANGUAGE_MAIN" ), chalo::ConfigManager::Get( "LANGUAGE_MAIN_FONT" ) );
        //SetGotoState( "startupstate" );
    }
    else
    {
        selectedHelperLanguage = false;
    }

    if ( selectedHelperLanguage )
    {
        SetGotoState( "startupstate" );
    }
}

void LanguageSelectState::Draw( sf::RenderWindow& window )
{
//    CursorState::Draw( window );
}



