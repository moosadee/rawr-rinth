#ifndef _LANGUAGESELECTSTATE
#define _LANGUAGESELECTSTATE

#include "../chalo-engine/States/IState.hpp"
//#include "../chalo-engine/GameObjects/GameObject.hpp"
//#include "../chalo-engine/Maps/WritableMap.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
//#include "../chalo-engine/Managers/DrawManager.hpp"
#include "../chalo-engine/UI/UILabel.hpp"

#include <vector>

class LanguageSelectState : public chalo::IState
{
public:
    LanguageSelectState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

};

#endif
