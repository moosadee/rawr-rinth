#ifndef _HELPSTATE
#define _HELPSSTATE


#include "../chalo-engine/States/IState.hpp"
//#include "../chalo-engine/GameObjects/GameObject.hpp"
//#include "../chalo-engine/Maps/WritableMap.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
//#include "../chalo-engine/Managers/DrawManager.hpp"

#include <SFML/Audio.hpp>

#include <vector>

class HelpState : public chalo::IState
{
public:
    HelpState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderTexture& window );

    private:
    static std::string s_className;
};

#endif
