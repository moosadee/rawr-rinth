#ifndef _LEVELEDITORSTATE
#define _LEVELEDITORSTATE

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/UI/UIImage.hpp"
#include "../chalo-engine/UI/UILabel.hpp"
#include "../chalo-engine/Maps/ChaloMap.hpp"

#include <vector>

class LevelEditorState : public chalo::IState
{
public:
    LevelEditorState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderTexture& window );

private:
    static std::string s_className;
    ChaloMap m_map;

    void ClearMap();
};

#endif
