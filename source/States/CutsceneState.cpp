#include "CutsceneState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
//#include "../chalo-engine/Managers/ConfigManager.hpp"
//#include "../chalo-engine/Managers/LanguageManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"

std::string CutsceneState::s_className = "CutsceneState";

CutsceneState::CutsceneState()
{
}

void CutsceneState::Init( const std::string& name )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Debug( "Parameters - name: " + name, s_className + "::" + __func__ );
    Logger::StackPop();
}

void CutsceneState::Setup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    IState::Setup();
    chalo::InputManager::Setup();

    RestartCutscene();
    NextPage();

    Logger::StackPop();
}

void CutsceneState::Cleanup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    chalo::MenuManager::Cleanup();
    Logger::StackPop();
}

void CutsceneState::RestartCutscene()
{
    m_slide = 0;
    m_text.clear();
    m_sprites.clear();

    sf::Text pageNum;
    pageNum.setFont( chalo::FontManager::Get( "main" ) );
    pageNum.setFillColor( sf::Color::White );
    pageNum.setOutlineColor( sf::Color::Black );
    pageNum.setOutlineThickness( 2 );
    pageNum.setCharacterSize( 15 );
    pageNum.setString( "0" );
    pageNum.setPosition( sf::Vector2f( 400-20, 240-20 ) );
    m_text.push_back( pageNum );


    m_slide = 0;
    m_cutsceneId = 1;
    m_clickCooldown = 0;
    m_titleAnimationCounter = 0;
}

void CutsceneState::Update()
{
    chalo::MenuManager::Update();

    if ( m_clickCooldown > 0 )
    {
        m_clickCooldown--;
    }

    if ( ( chalo::InputManager::IsLeftClick() || chalo::InputManager::IsKeyPressed( sf::Keyboard::Return ) )
        && m_clickCooldown <= 0 )
    {
        NextPage();
        m_clickCooldown = 20;
    }

    if ( chalo::InputManager::IsKeyPressed( sf::Keyboard::R ) )
    {
        RestartCutscene();
    }

    m_titleAnimationCounter += 0.5;
    if ( m_titleAnimationCounter >= 360 )
    {
        m_titleAnimationCounter = 0;
    }

    // CUTSCENE 1
    if ( m_cutsceneId == 1 && m_slide == 3 )
    {
        sf::Color color = m_sprites[1].getColor();
        if ( color.a < 255 )
        {
            color.a += 5;
            m_sprites[1].setColor( color );
        }
    }

    if ( m_cutsceneId == 1 && ( m_slide == 5 || m_slide == 6 ) )
    {
        sf::Color color = m_sprites[1].getColor();
        if ( color.a < 255 )
        {
            color.a += 5;
            m_sprites[1].setColor( color );
        }


        float modifyRotation = m_sprites[5].getRotation();
        float rotationRate = 4;
        modifyRotation += rotationRate;
        m_sprites[5].setRotation( modifyRotation );

        if ( m_sprites[5].getScale().x < 1 )
        {
            float modifyScale = m_sprites[5].getScale().x;
            modifyScale += 0.01;
            m_sprites[5].setScale( modifyScale, modifyScale );
        }
    }

    if ( m_cutsceneId == 1 && m_slide == 6 )
    {
        sf::Color color = m_sprites[6].getColor();
        if ( color.a < 255 )
        {
            color.a += 5;
            m_sprites[6].setColor( color );
        }
    }
}

void CutsceneState::Draw( sf::RenderTexture& window )
{
    //chalo::MenuManager::Draw( window );

    for ( auto& sprite : m_sprites )
    {
        window.draw( sprite );
    }

    for ( auto& text : m_text )
    {
        window.draw( text );
    }
}

void CutsceneState::NextPage()
{
    m_slide++;

    m_text[0].setString( Helper::ToString( m_slide ) );

    if ( m_cutsceneId == 1 && m_slide == 1 )
    {
        m_sprites.clear();

        sf::Sprite sprBg;
        chalo::TextureManager::Add( "slide_bg", "Content/Graphics/Cutscenes/act1-slide1.png" );
        sprBg.setTexture( chalo::TextureManager::Get( "slide_bg" ) );
        sprBg.setPosition( sf::Vector2f( 0, 0 ) );
        m_sprites.push_back( sprBg );

        sf::Text txtMain;
        txtMain.setFont( chalo::FontManager::Get( "main" ) );
        txtMain.setFillColor( sf::Color::White );
        txtMain.setOutlineColor( sf::Color::Black );
        txtMain.setOutlineThickness( 2 );
        txtMain.setCharacterSize( 20 );
        txtMain.setString( "Long ago in prehistory..." );
        txtMain.setPosition( sf::Vector2f( 10, 10 ) );
        m_text.push_back( txtMain );
    }
    else if ( m_cutsceneId == 1 && m_slide == 2 )
    {
        m_text[1].setString( "Rawr and Roar were caring for \ntheir eggs..." );

    }
    else if ( m_cutsceneId == 1 && m_slide == 3 )
    {
        sf::Sprite sprBg;
        chalo::TextureManager::Add( "slide_bg2", "Content/Graphics/Cutscenes/act1-slide2.png" );
        sprBg.setTexture( chalo::TextureManager::Get( "slide_bg2" ) );
        sprBg.setPosition( sf::Vector2f( 0, 0 ) );
        sprBg.setColor( sf::Color( 255, 255, 255, 0 ) );
        m_sprites.push_back( sprBg );

        m_text[1].setString( "It was peaceful." );
    }
    else if ( m_cutsceneId == 1 && m_slide == 4 )
    {
        m_sprites.clear();

        chalo::TextureManager::Add( "slide_bg", "Content/Graphics/Cutscenes/act1-slide3_background1.png" );
        chalo::TextureManager::Add( "slide_bg2", "Content/Graphics/Cutscenes/act1-slide3_background2.png" );

        chalo::TextureManager::Add( "eggs", "Content/Graphics/Cutscenes/act1-slide3_eggs1.png" );
        chalo::TextureManager::Add( "eggs2", "Content/Graphics/Cutscenes/act1-slide3_eggs2.png" );

        chalo::TextureManager::Add( "shadow", "Content/Graphics/Cutscenes/act1-slide3_shadow.png" );
        chalo::TextureManager::Add( "portal1", "Content/Graphics/Cutscenes/act1-slide3_portal1.png" );
        chalo::TextureManager::Add( "portal2", "Content/Graphics/Cutscenes/act1-slide3_portal2.png" );

        m_sprites.push_back( sf::Sprite( chalo::TextureManager::Get( "slide_bg" ) ) );      // 0
        m_sprites.push_back( sf::Sprite( chalo::TextureManager::Get( "slide_bg2" ) ) );     // 1
        m_sprites.push_back( sf::Sprite( chalo::TextureManager::Get( "eggs" ) ) );          // 2
        m_sprites.push_back( sf::Sprite( chalo::TextureManager::Get( "eggs2" ) ) );         // 3
        m_sprites.push_back( sf::Sprite( chalo::TextureManager::Get( "portal1" ) ) );       // 4
        m_sprites.push_back( sf::Sprite( chalo::TextureManager::Get( "portal2" ) ) );       // 5
        m_sprites.push_back( sf::Sprite( chalo::TextureManager::Get( "shadow" ) ) );        // 6

        m_sprites[1].setColor( sf::Color( 255, 255, 255, 0 ) );
        m_sprites[3].setColor( sf::Color( 255, 255, 255, 0 ) );
        m_sprites[4].setColor( sf::Color( 255, 255, 255, 0 ) );
        m_sprites[5].setColor( sf::Color( 255, 255, 255, 0 ) );
        m_sprites[6].setColor( sf::Color( 255, 255, 255, 0 ) );

        m_text[1].setString( "But one night..." );
    }
    else if ( m_cutsceneId == 1 && m_slide == 5 )
    {
        m_sprites[4].setColor( sf::Color::White );
        m_sprites[4].setPosition( 200, 50 );
        m_sprites[4].setOrigin( 75/2, 75/2 );
        m_sprites[5].setColor( sf::Color::White );
        m_sprites[5].setPosition( 200, 50 );
        m_sprites[5].setOrigin( 75/2, 75/2 );
        m_sprites[5].setScale( 0.1, 0.1 );

        m_text[1].setString( "..." );
    }
    else if ( m_cutsceneId == 1 && m_slide == 6 )
    {
        m_sprites[4].setScale( 1, 1 );
        m_sprites[5].setScale( 1, 1 );

        m_sprites[6].setOrigin( 42/2, 69/2 );
        m_sprites[6].setPosition( 200, 50 );

        m_text[1].setString( "...They came..." );
    }
    else if ( m_cutsceneId == 1 && m_slide == 7 )
    {
        m_sprites[6].setColor( sf::Color::White );
    }
}



