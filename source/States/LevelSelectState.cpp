#include "LevelSelectState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/StateManager.hpp"
//#include "../chalo-engine/Managers/LanguageManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"

std::string LevelSelectState::s_className = "LevelSelectState";

LevelSelectState::LevelSelectState()
{
}

void LevelSelectState::Init( const std::string& name )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::StackPop();
}

void LevelSelectState::Setup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    IState::Setup();
    chalo::InputManager::Setup();

    // Load needed assets
//    chalo::TextureManager::AddAndGet( "title_art", "Content/Graphics/LevelSelectState/titlebg.png" );

    chalo::TextureManager::Add( "menu_bg",                 "Content/Graphics/UI/menu_bg.png" );
    chalo::TextureManager::Add( "level_select_backgrounds","Content/Graphics/UI/level_select_backgrounds.png" );
    chalo::TextureManager::Add( "menu_icons",              "Content/Graphics/UI/menu_icons.png" );
    chalo::TextureManager::Add( "button_bg",               "Content/Graphics/UI/button_bg.png" );
    chalo::TextureManager::Add( "button_bg_square",        "Content/Graphics/UI/button_bg_square.png" );
    chalo::TextureManager::Add( "options_menu_buttons",    "Content/Graphics/UI/options_menu_buttons.png" );
    chalo::TextureManager::Add( "scrollbar",               "Content/Graphics/UI/scrollbar.png" );
    chalo::TextureManager::Add( "button_level_select",     "Content/Graphics/UI/button_level_select.png" );

    // Load menu file
    chalo::MenuManager::LoadTextMenu( "levelselect.chalomenu" );

    m_selectedWorld = Helper::StringToInt( chalo::Messager::Get( "WORLD" ) );
    Logger::OutValue( "World", m_selectedWorld );

    m_selectedLevel = -1;

    ArrangeIcons( m_selectedWorld );

    m_buttonKeys = std::vector<std::string>( {
        "btnCutscene1",
        "btnLevel1",
        "btnLevel2",
        "btnLevel3",
        "btnLevel4",
        "btnLevel5",
        "btnCutscene2",
    } );

    Logger::StackPop();
}

void LevelSelectState::ArrangeIcons( int world )
{
    Logger::StackPush( s_className + "::" + __func__ );

    if ( world == 1 )
    {
        try
        {
//            chalo::MenuManager::GetButton( "main", "btnCutscene1" ).SetPosition( sf::Vector2f( 50, 60 ) );
//            chalo::MenuManager::GetButton( "main", "btnLevel1" ).SetPosition( sf::Vector2f( 150, 60 ) );
//            chalo::MenuManager::GetButton( "main", "btnLevel2" ).SetPosition( sf::Vector2f( 250, 60 ) );
//            chalo::MenuManager::GetButton( "main", "btnLevel3" ).SetPosition( sf::Vector2f( 300, 120 ) );
//            chalo::MenuManager::GetButton( "main", "btnLevel4" ).SetPosition( sf::Vector2f( 160, 130 ) );
//            chalo::MenuManager::GetButton( "main", "btnLevel5" ).SetPosition( sf::Vector2f( 50, 170 ) );
//            chalo::MenuManager::GetButton( "main", "btnCutscene2" ).SetPosition( sf::Vector2f( 50, 60 ) );
        }
        catch( const out_of_range& ex )
        {
            chalo::Messager::Set( "ERROR", ex.what() );
            chalo::StateManager::ChangeState( "default" );
        }
    }

    Logger::StackPop();
}

void LevelSelectState::Cleanup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    chalo::MenuManager::Cleanup();
    Logger::StackPop();
}

void LevelSelectState::Update()
{
    chalo::MenuManager::Update();
    std::string clickedButton = chalo::MenuManager::GetClickedButton();

    if ( clickedButton == "btnForward" )
    {
        GotoLevel();
    }
    else if ( clickedButton == "btnBack" )
    {
        SetGotoState( "worldSelectState" );
    }
    else
    {
        ClickLevel( clickedButton );
    }
}

void LevelSelectState::ClickLevel( string buttonKey )
{
    if ( buttonKey == "" ) { return; }

    // Make all buttons not selected
    for ( auto& key : m_buttonKeys )
    {
        sf::IntRect clipRect = chalo::MenuManager::GetButton( "main", key ).GetFrameRect();
        clipRect.top = 0;
        chalo::MenuManager::GetButton( "main", key ).SetupBackgroundImageClipRect( clipRect );
    }

    // Make the one button selected
    sf::IntRect clipRect = chalo::MenuManager::GetButton( "main", buttonKey ).GetFrameRect();
    clipRect.top = 45;
    chalo::MenuManager::GetButton( "main", buttonKey ).SetupBackgroundImageClipRect( clipRect );

    // TODO: This could be stored in a data file somewhere.
    std::string levelName = "";
    int chosenLevel = -1;
    switch( m_selectedWorld )
    {
        case 1:
        if ( buttonKey == "btnCutscene1" )
        {
            levelName = "Introduction";
            chosenLevel = 0;
        }
        else if ( buttonKey == "btnLevel1" )
        {
            levelName = "Where am I?";
            chosenLevel = 1;
        }
        else if ( buttonKey == "btnLevel2" )
        {
            levelName = "Storage room";
            chosenLevel = 2;
        }
        else if ( buttonKey == "btnLevel3" )
        {
            levelName = "Science lab";
            chosenLevel = 3;
        }
        else if ( buttonKey == "btnLevel4" )
        {
            levelName = "Open floor-plan office";
            chosenLevel = 4;
        }
        else if ( buttonKey == "btnLevel5" )
        {
            levelName = "Conference room";
            chosenLevel = 5;
        }
        else if ( buttonKey == "btnCutscene2" )
        {
            levelName = "Introduction";
            chosenLevel = 6;
        }
        break;
        break;

        case 2:
        break;

        case 3:
        break;

        case 4:
        break;

        case 5:
        break;
    }

    if ( chosenLevel == m_selectedLevel )
    {
        GotoLevel();
        return;
    }
    m_selectedLevel = chosenLevel;

    // Set the level label
    chalo::MenuManager::GetLabel( "text", "lblLevelName" ).SetText( levelName );
}

void LevelSelectState::GotoLevel()
{
    Logger::OutValue( "Go to level", m_selectedLevel );
    if ( m_selectedLevel != -1 )
    {
        chalo::Messager::Set( "LEVEL", m_selectedLevel );
        SetGotoState( "gameState" );
    }
}

void LevelSelectState::Draw( sf::RenderTexture& window )
{
    chalo::MenuManager::Draw( window );
}



