#ifndef _PD_STARTUPSTATE
#define _PD_STARTUPSTATE


#include "../chalo-engine/States/IState.hpp"
//#include "../chalo-engine/GameObjects/GameObject.hpp"
//#include "../chalo-engine/Maps/WritableMap.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
//#include "../chalo-engine/Managers/DrawManager.hpp"
#include "../chalo-engine/UI/UILabel.hpp"

#include <vector>

class PlaydateStartupState : public chalo::IState
{
public:
    PlaydateStartupState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

private:
    chalo::UILabel m_helperLanguage;
    chalo::UILabel m_targetLanguage;
    sf::Sprite m_cursor;
};

#endif
