#include "OptionsState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Managers/AudioManager.hpp"
#include "../chalo-engine/Managers/LanguageManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
//#include "../chalo-engine/Utilities/Platform.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"
#include "../chalo-engine/Utilities_SFML/SFMLHelper.hpp"

std::string OptionsState::s_className = "OptionsState";

OptionsState::OptionsState()
{
}

void OptionsState::Init( const std::string& name )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Debug( "Parameters - name: " + name, s_className + "::" + __func__ );
    Logger::StackPop();
}

void OptionsState::Setup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    IState::Setup();

    chalo::TextureManager::Add( "menu_bg",                 "Content/Graphics/UI/menu_bg.png" );
    chalo::TextureManager::Add( "menu_icons",              "Content/Graphics/UI/menu_icons.png" );
    chalo::TextureManager::Add( "button_bg",               "Content/Graphics/UI/button_bg_2.png" );
    chalo::TextureManager::Add( "button_bg_square",        "Content/Graphics/UI/button_bg_square_2.png" );
    chalo::TextureManager::Add( "options_menu_buttons",    "Content/Graphics/UI/options_menu_buttons.png" );
    chalo::TextureManager::Add( "scrollbar",               "Content/Graphics/UI/scrollbar.png" );

    chalo::AudioManager::AddMusic( "music-test",           "Content/Audio/music-test.ogg" );
    chalo::AudioManager::AddSoundBuffer( "sound-test",           "Content/Audio/sound-test.ogg" );

    chalo::MenuManager::LoadTextMenu( "options.chalomenu" );

    sf::Vector2f pos( 360, 700 );
    sf::IntRect dimensions( 0, 0, 300, 35 );

    chalo::InputManager::Setup();

    LoadSavedOptions();

    Logger::StackPop();
}

void OptionsState::Cleanup()
{
    Logger::StackPush( s_className + "::" + __func__ );

    chalo::MenuManager::Cleanup();

    Logger::StackPop();
}

void OptionsState::LoadSavedOptions()
{
    Logger::StackPush( s_className + "::" + __func__ );

    // PUT SOUND SLIDER AT APPROPRIATE SPOT
    int soundVolume = Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) );

    chalo::UIButton& soundScrollBg = chalo::MenuManager::GetButton( "main", "soundScrollBg" );

    int minval = soundScrollBg.GetPositionRect().left;
    int maxval = soundScrollBg.GetPositionRect().left + soundScrollBg.GetPositionRect().width - 10;
    int length = maxval - minval;

    float volumeDec = float( soundVolume ) / 100;
    float ratioX = length * volumeDec + minval;

    chalo::UIButton& soundScrollButton = chalo::MenuManager::GetButton( "main", "soundScrollButton" );
    int y = soundScrollButton.GetPositionRect().top;
    soundScrollButton.SetPosition( sf::Vector2f( ratioX, y ) );


    // PUT MUSIC SLIDER AT APPROPRIATE SPOT
    chalo::UIButton& musicScrollBg = chalo::MenuManager::GetButton( "main", "musicScrollBg" );

    int musicVolume = Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) );

    minval = musicScrollBg.GetPositionRect().left;
    maxval = musicScrollBg.GetPositionRect().left + musicScrollBg.GetPositionRect().width - 10;
    length = maxval - minval;

    volumeDec = float( musicVolume ) / 100;
    ratioX = length * volumeDec + minval;

    chalo::UIButton& musicScrollButton = chalo::MenuManager::GetButton( "main", "musicScrollButton" );
    y = musicScrollButton.GetPositionRect().top;
    musicScrollButton.SetPosition( sf::Vector2f( ratioX, y ) );

    if ( chalo::ConfigManager::Get( "USE_OPEN_DYSLEXIC" ) == "1" )
    {
        chalo::MenuManager::GetButton( "btnToggleOpenDyslexic" ).SetupBackgroundImageClipRect( sf::IntRect( 130, 0, 26, 26 ) );
    }
    else
    {
        chalo::MenuManager::GetButton( "btnToggleOpenDyslexic" ).SetupBackgroundImageClipRect( sf::IntRect( 104, 0, 26, 26 ) );
    }

    // SET APPROPRIATE RESOLUTION CHECK

    int gameWidth = Helper::StringToInt( chalo::ConfigManager::Get( "GAME_WIDTH" ) );
    int gameHeight = Helper::StringToInt( chalo::ConfigManager::Get( "GAME_HEIGHT" ) );

    int windowWidth = Helper::StringToInt( chalo::ConfigManager::Get( "WINDOW_WIDTH" ) );
    int windowHeight = Helper::StringToInt( chalo::ConfigManager::Get( "WINDOW_HEIGHT" ) );

    int ratio = windowWidth / gameWidth;
    Logger::OutValue( "Window ratio", Helper::ToString( ratio ) , s_className + "::" + __func__ );

    // Set all the display buttons to blank for a moment
    chalo::MenuManager::GetButton( "resolution", "btnToggle1x" ).SetupBackgroundImageClipRect( sf::IntRect( 104, 0, 26, 26 ) );
    chalo::MenuManager::GetButton( "resolution", "btnToggle2x" ).SetupBackgroundImageClipRect( sf::IntRect( 104, 0, 26, 26 ) );
    chalo::MenuManager::GetButton( "resolution", "btnToggle4x" ).SetupBackgroundImageClipRect( sf::IntRect( 104, 0, 26, 26 ) );
    chalo::MenuManager::GetButton( "resolution", "btnToggleFullscreen" ).SetupBackgroundImageClipRect( sf::IntRect( 104, 0, 26, 26 ) );

    // Set only the one clicked to have checkmark
    std::string key = "btnToggle" + Helper::ToString( ratio ) + "x";
    chalo::MenuManager::GetButton( "resolution", key ).SetupBackgroundImageClipRect( sf::IntRect( 130, 0, 26, 26 ) );


    Logger::StackPop();
}

void OptionsState::ScrollSound()
{
    Logger::StackPush( s_className + "::" + __func__ );

    Logger::StackPop();
}

void OptionsState::ScrollMusic()
{
    Logger::StackPush( s_className + "::" + __func__ );

    chalo::UIButton& musicScrollBg = chalo::MenuManager::GetButton( "main", "musicScrollBg" );
    chalo::UIButton& musicScrollButton = chalo::MenuManager::GetButton( "main", "musicScrollButton" );
    sf::Vector2f mousePos = chalo::InputManager::GetMousePosition();
    Logger::OutValue( "MousePos", SFMLHelper::CoordinateToString( mousePos ) );

    int minval = musicScrollBg.GetPositionRect().left;
    int maxval = musicScrollBg.GetPositionRect().left + musicScrollBg.GetPositionRect().width - 10;
    int length = maxval - minval;

    sf::IntRect scrollPos = musicScrollButton.GetPositionRect();
    scrollPos.left = mousePos.x;
    if      ( scrollPos.left < minval ) { scrollPos.left = minval; }
    else if ( scrollPos.left > maxval ) { scrollPos.left = maxval; }
    musicScrollButton.SetPosition( scrollPos );

    Logger::StackPop();
}

void OptionsState::IncreaseSoundVolume()
{
    Logger::StackPush( s_className + "::" + __func__ );

    int soundVolume = Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) );
    soundVolume += 25;
    if ( soundVolume > 100 ) { soundVolume = 100; }
    chalo::ConfigManager::Set( "SOUND_VOLUME", Helper::ToString( soundVolume ) );
    LoadSavedOptions();

    m_soundTest.setBuffer( chalo::AudioManager::GetSoundBuffer( "sound-test" ) );
    m_soundTest.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );
    m_soundTest.play();

    Logger::StackPop();
}

void OptionsState::DecreaseSoundVolume()
{
    Logger::StackPush( s_className + "::" + __func__ );

    int soundVolume = Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) );
    soundVolume -= 25;
    if ( soundVolume < 0 ) { soundVolume = 0; }
    chalo::ConfigManager::Set( "SOUND_VOLUME", Helper::ToString( soundVolume ) );
    LoadSavedOptions();

    m_soundTest.setBuffer( chalo::AudioManager::GetSoundBuffer( "sound-test" ) );
    m_soundTest.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );
    m_soundTest.play();

    Logger::StackPop();
}

void OptionsState::IncreaseMusicVolume()
{
    Logger::StackPush( s_className + "::" + __func__ );

    int musicVolume = Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) );
    musicVolume += 25;
    if ( musicVolume > 100 ) { musicVolume = 100; }
    chalo::ConfigManager::Set( "MUSIC_VOLUME", Helper::ToString( musicVolume ) );
    LoadSavedOptions();
    chalo::AudioManager::PlayMusic( "music-test", false );

    Logger::StackPop();
}

void OptionsState::DecreaseMusicVolume()
{
    Logger::StackPush( s_className + "::" + __func__ );

    int musicVolume = Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) );
    musicVolume -= 25;
    if ( musicVolume < 0 ) { musicVolume = 0; }
    chalo::ConfigManager::Set( "MUSIC_VOLUME", Helper::ToString( musicVolume ) );
    LoadSavedOptions();
    chalo::AudioManager::PlayMusic( "music-test", false );

    Logger::StackPop();
}

void OptionsState::ToggleOpenDyslexic()
{
    Logger::StackPush( s_className + "::" + __func__ );

    if ( chalo::ConfigManager::Get( "USE_OPEN_DYSLEXIC" ) == "1" )
    {
        // Turn it off
        chalo::MenuManager::GetButton( "btnToggleOpenDyslexic" ).SetupBackgroundImageClipRect( sf::IntRect( 104, 0, 26, 26 ) );
        chalo::MenuManager::GetLabel( "lblUseOpenDyslexic" ).SetText( chalo::LanguageManager::Text( "options_use_open_dyslexic" ) );
        chalo::ConfigManager::Set( "USE_OPEN_DYSLEXIC", "0" );
        chalo::MenuManager::DyslexiaFontReload();
    }
    else
    {
        // Turn it on
        chalo::MenuManager::GetButton( "btnToggleOpenDyslexic" ).SetupBackgroundImageClipRect( sf::IntRect( 130, 0, 26, 26 ) );
        chalo::MenuManager::GetLabel( "lblUseOpenDyslexic" ).SetText( chalo::LanguageManager::Text( "options_dont_use_open_dyslexic" )  );
        chalo::ConfigManager::Set( "USE_OPEN_DYSLEXIC", "1" );
        chalo::MenuManager::DyslexiaFontReload();
    }

    Logger::StackPop();
}

void OptionsState::ChangeDisplay( int multiplier )
{
    Logger::StackPush( s_className + "::" + __func__ );

    int gameWidth = Helper::StringToInt( chalo::ConfigManager::Get( "GAME_WIDTH" ) );
    int gameHeight = Helper::StringToInt( chalo::ConfigManager::Get( "GAME_HEIGHT" ) );

    int adjustedWidth = gameWidth * multiplier;
    int adjustedHeight = gameHeight * multiplier;

    chalo::ConfigManager::Set( "WINDOW_WIDTH", Helper::ToString( adjustedWidth ) );
    chalo::ConfigManager::Set( "WINDOW_HEIGHT", Helper::ToString( adjustedHeight ) );

    // Set all the display buttons to blank for a moment
    chalo::MenuManager::GetButton( "resolution", "btnToggle1x" ).SetupBackgroundImageClipRect( sf::IntRect( 104, 0, 26, 26 ) );
    chalo::MenuManager::GetButton( "resolution", "btnToggle2x" ).SetupBackgroundImageClipRect( sf::IntRect( 104, 0, 26, 26 ) );
    chalo::MenuManager::GetButton( "resolution", "btnToggle4x" ).SetupBackgroundImageClipRect( sf::IntRect( 104, 0, 26, 26 ) );
    chalo::MenuManager::GetButton( "resolution", "btnToggleFullscreen" ).SetupBackgroundImageClipRect( sf::IntRect( 104, 0, 26, 26 ) );

    // Set only the one clicked to have checkmark
    std::string key = "btnToggle" + Helper::ToString( multiplier ) + "x";
    chalo::MenuManager::GetButton( "resolution", key ).SetupBackgroundImageClipRect( sf::IntRect( 130, 0, 26, 26 ) );

    chalo::Application::ChangeDisplay( false, adjustedWidth, adjustedHeight );

    Logger::StackPop();
}

void OptionsState::SetFullscreen()
{
    Logger::StackPush( s_className + "::" + __func__ );

    chalo::MenuManager::GetButton( "resolution", "btnToggle1x" ).SetupBackgroundImageClipRect( sf::IntRect( 104, 0, 26, 26 ) );
    chalo::MenuManager::GetButton( "resolution", "btnToggle2x" ).SetupBackgroundImageClipRect( sf::IntRect( 104, 0, 26, 26 ) );
    chalo::MenuManager::GetButton( "resolution", "btnToggle4x" ).SetupBackgroundImageClipRect( sf::IntRect( 104, 0, 26, 26 ) );
    chalo::MenuManager::GetButton( "resolution", "btnToggleFullscreen" ).SetupBackgroundImageClipRect( sf::IntRect( 130, 0, 26, 26 ) );

    chalo::Application::ChangeDisplay( true );

    Logger::StackPop();
}

void OptionsState::Update()
{
    Logger::StackPush( s_className + "::" + __func__ );

    chalo::MenuManager::Update();

    std::string clickedButton = chalo::MenuManager::GetClickedButton();
    if ( clickedButton != "" )
    {
        Logger::OutValue( "Clicked button", clickedButton, s_className + "::" + __func__ );
    }

    if ( clickedButton == "musicScrollButton" )
    {
        ScrollMusic();
    }
    else if ( clickedButton == "soundScrollButton" )
    {
        ScrollSound();
    }
    else if ( clickedButton == "btnSoundEffectsDecrease" )
    {
        DecreaseSoundVolume();
    }
    else if ( clickedButton == "btnSoundEffectsIncrease" )
    {
        IncreaseSoundVolume();
    }
    else if ( clickedButton == "btnMusicDecrease" )
    {
        DecreaseMusicVolume();
    }
    else if ( clickedButton == "btnMusicIncrease" )
    {
        IncreaseMusicVolume();
    }
    else if ( clickedButton == "btnToggleOpenDyslexic" )
    {
        ToggleOpenDyslexic();
    }
    else if ( clickedButton == "btnToggle1x" )
    {
        ChangeDisplay( 1 );
    }
    else if ( clickedButton == "btnToggle2x" )
    {
        ChangeDisplay( 2 );
    }
    else if ( clickedButton == "btnToggle4x" )
    {
        ChangeDisplay( 4 );
    }
    else if ( clickedButton == "btnToggleFullscreen" )
    {
        SetFullscreen();
    }
    else if ( clickedButton == "btnBack" )
    {
        SetGotoState( "startupState" );
    }

    Logger::StackPop();
}

void OptionsState::Draw( sf::RenderTexture& window )
{
    chalo::MenuManager::Draw( window );
}



