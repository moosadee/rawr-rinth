#include "WorldSelectState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
//#include "../chalo-engine/Managers/ConfigManager.hpp"
//#include "../chalo-engine/Managers/LanguageManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"

std::string WorldSelectState::s_className = "WorldSelectState";

WorldSelectState::WorldSelectState()
{
}

void WorldSelectState::Init( const std::string& name )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Debug( "Parameters - name: " + name, s_className + "::" + __func__ );
    Logger::StackPop();
}

void WorldSelectState::Setup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    IState::Setup();
    chalo::InputManager::Setup();

    // Load needed assets
//    chalo::TextureManager::AddAndGet( "title_art", "Content/Graphics/WorldSelectState/titlebg.png" );

    chalo::TextureManager::Add( "menu_bg",                 "Content/Graphics/UI/menu_bg.png" );
    chalo::TextureManager::Add( "menu_icons",              "Content/Graphics/UI/menu_icons.png" );
    chalo::TextureManager::Add( "button_bg",               "Content/Graphics/UI/button_bg.png" );
    chalo::TextureManager::Add( "button_bg_square",        "Content/Graphics/UI/button_bg_square.png" );
    chalo::TextureManager::Add( "options_menu_buttons",    "Content/Graphics/UI/options_menu_buttons.png" );
    chalo::TextureManager::Add( "scrollbar",               "Content/Graphics/UI/scrollbar.png" );
    chalo::TextureManager::Add( "world_select_buttons",    "Content/Graphics/UI/buttons_world_select.png" );
    chalo::TextureManager::Add( "world_select",            "Content/Graphics/UI/world_select.png" );
    chalo::TextureManager::Add( "button_level_select",    "Content/Graphics/UI/button_level_select.png" );

    // Load menu file
    chalo::MenuManager::LoadTextMenu( "worldselect.chalomenu" );

    m_selectedWorld = 1;

    Logger::StackPop();
}

void WorldSelectState::Cleanup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    chalo::MenuManager::Cleanup();
    Logger::StackPop();
}

void WorldSelectState::Update()
{
    chalo::MenuManager::Update();
    std::string clickedButton = chalo::MenuManager::GetClickedButton();

    if ( clickedButton == "btnWorldSelect" )
    {
//        GotoWorld();
    }
    else if ( clickedButton == "btnForward" )
    {
        GotoWorld();
    }
    else if ( clickedButton == "btnBack" )
    {
        SetGotoState( "startupState" );
    }
    else if ( clickedButton == "btnWorld1" )
    {
        SelectWorld( 1 );
    }
    else if ( clickedButton == "btnWorld2" )
    {
//        SelectWorld( 2 );
    }
    else if ( clickedButton == "btnWorld3" )
    {
//        SelectWorld( 3 );
    }
    else if ( clickedButton == "btnWorld4" )
    {
//        SelectWorld( 4 );
    }
    else if ( clickedButton == "btnWorld5" )
    {
//        SelectWorld( 5 );
    }
}

void WorldSelectState::Draw( sf::RenderTexture& window )
{
    chalo::MenuManager::Draw( window );
}

void WorldSelectState::SelectWorld( int world )
{
    if ( m_selectedWorld == world )
    {
        // If you click the world button a second time
        // it will count as a confirmation that you want
        // to go to that world.
        GotoWorld();
        return;
    }

    m_selectedWorld = world;
    sf::IntRect rect;
    rect.width = 300;
    rect.height = 175;
    rect.left = 0;
    rect.top = (world - 1) * rect.height;

    chalo::UIButton& worldSelector = chalo::MenuManager::GetButton( "main", "btnWorldSelect" );
    worldSelector.GetBackground().SetImageClipRect( rect );

    // Only world 1 is selected
    chalo::MenuManager::GetButton( "main", "btnWorld1" ).GetBackground().SetImageClipRect( sf::IntRect( 0, 0, 0, 40 ) );

    // These worlds are locked for the demo
    chalo::MenuManager::GetButton( "main", "btnWorld2" ).GetBackground().SetImageClipRect( sf::IntRect( 0, 0, 120, 40 ) );
    chalo::MenuManager::GetButton( "main", "btnWorld3" ).GetBackground().SetImageClipRect( sf::IntRect( 0, 0, 120, 40 ) );
    chalo::MenuManager::GetButton( "main", "btnWorld4" ).GetBackground().SetImageClipRect( sf::IntRect( 0, 0, 120, 40 ) );
    chalo::MenuManager::GetButton( "main", "btnWorld5" ).GetBackground().SetImageClipRect( sf::IntRect( 0, 0, 120, 40 ) );

    // Set just one to selected
    string key = "btnWorld" + Helper::ToString( world );
    chalo::MenuManager::GetButton( "main", key ).GetBackground().SetImageClipRect( sf::IntRect( 60, 0, 60, 40 ) );
}

void WorldSelectState::GotoWorld()
{
    if ( m_selectedWorld != -1 )
    {
        chalo::Messager::Set( "WORLD", m_selectedWorld );
        SetGotoState( "levelSelectState" );
    }
}




