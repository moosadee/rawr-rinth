#include "LevelEditorState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/AudioManager.hpp"
//#include "../chalo-engine/Managers/ConfigManager.hpp"
//#include "../chalo-engine/Managers/LanguageManager.hpp"
//#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"

std::string LevelEditorState::s_className = "LevelEditorState";

LevelEditorState::LevelEditorState()
{
}

void LevelEditorState::Init( const std::string& name )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Debug( "Parameters - name: " + name, s_className + "::" + __func__ );
    Logger::StackPop();
}

void LevelEditorState::Setup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    IState::Setup();
    chalo::InputManager::Setup();

    // Load needed assets
    chalo::TextureManager::Add( "level_editor_art", "Content/Graphics/UI/editor_art.png" );
    chalo::TextureManager::Add( "sheet_objects", "Content/Graphics/Tilesets/sheet-objects.png" );
    chalo::TextureManager::Add( "sheet_terrain", "Content/Graphics/Tilesets/sheet-terrain.png" );

    // Load menu file
    chalo::MenuManager::LoadTextMenu( "level_editor.chalomenu" );

    ClearMap();

    Logger::StackPop();
}

void LevelEditorState::ClearMap()
{
    Logger::StackPush( s_className + "::" + __func__ );

    m_map.Clear();

    m_map.SetTileDimensions( sf::Vector2i( 20, 20 ) );
    m_map.SetMapDimensions( sf::Vector2i( 20, 12 ) );

    m_map.SetTilesetName( "sheet_terrain" );
    m_map.SetObjectsetName( "sheet_objects" );

    m_map.LoadObjectTypes( "Content/Maps/sheet-objecttypes.csv" );
    m_map.SetGridLines( true );

    m_map.AddMapLayer( "FLOOR" );
    m_map.AddMapLayer( "WALLS" );
    m_map.AddMapLayer( "DECOR" );
    m_map.AddMapLayer( "OBJECTS" );

    // Set up bottom layer
    for ( int y = 0; y < 12; y++ )
    {
        for ( int x = 0; x < 20; x++ )
        {
            ChaloTile tile;
            tile.SetTexture( chalo::TextureManager::Get( "sheet_terrain" ) );
            tile.SetPosition( sf::Vector2f( x*20, y*20 ) );
            tile.SetFrameRect( sf::IntRect( 0, 20, 20, 20 ) );
            tile.SetDimensions( sf::IntRect( 0, 0, 20, 20 ) );
            tile.SetLayerIndex( 0 );
            tile.SetCanWalkOn( true );
            m_map.AddTileToLayer( "FLOOR", tile );
        }
    }

    Logger::StackPop();
}

void LevelEditorState::Cleanup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    chalo::MenuManager::Cleanup();
    Logger::StackPop();
}

void LevelEditorState::Update()
{
    chalo::MenuManager::Update();
    std::string clickedButton = chalo::MenuManager::GetClickedButton();

    if ( clickedButton == "btnPlay" )
    {
        SetGotoState( "worldSelectState" );
    }
    else if ( clickedButton == "btnMapEditor" )
    {
        SetGotoState( "mapEditorState" );
    }
    else if ( clickedButton == "btnOptions" )
    {
        SetGotoState( "optionsState" );
    }
    else if ( clickedButton == "btnHelp" )
    {
        SetGotoState( "helpState" );
    }
    else if ( clickedButton == "btnExit" )
    {
        chalo::Application::ReadyToQuit();
    }
}

void LevelEditorState::Draw( sf::RenderTexture& window )
{
    chalo::MenuManager::Draw( window );
    m_map.Draw( window );
}



