#include "GameState.hpp"

#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/LanguageManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"
#include "../chalo-engine/Utilities_SFML/SFMLHelper.hpp"
#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Utilities/XmlParser.hpp"

std::string GameState::s_className = "GameState";

GameState::GameState()
    : MAP_WIDTH( 20 ), MAP_HEIGHT( 12 ),
    TILE_WIDTH( 20 ), TILE_HEIGHT( 20 ),
    TILESET_WIDTH( 10 ), TILESET_HEIGHT( 6 )
{
}

void GameState::Init( const std::string& name )
{
    Logger::StackPush( s_className + "::" + __func__ );
    IState::Init( name );
    Logger::StackPop();
}

void GameState::Setup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    IState::Setup();

    m_allObjects.clear();
    m_playerDead = false;
    m_levelEnd = false;

    m_totalEggs = 0;
    m_rewindCount = 0;

    // Load textures for game
    for ( std::string filename : std::vector<std::string>{
        "sheet-rawr", "sheet-patrol-robot", "sheet-static-robot", "sheet-rotating-robot", "sheet-alldirection-robot",
        "sheet-object-bomb", "sheet-object-button", "sheet-object-crate", "sheet-object-door", "sheet-object-jaildoor", "sheet-object-exitdoor", "sheet-object-egg", "sheet-object-key"
        } )
    {
        std::string path = "Content/Graphics/Objects/" + filename + ".png";
        chalo::TextureManager::Add( filename, path );
    }

    chalo::TextureManager::Add( "hud-icons", "Content/Graphics/UI/ui-hud-icons.png" );
    chalo::TextureManager::Add( "level_end_bg", "Content/Graphics/UI/level_end_bg.png" );
    chalo::TextureManager::Add( "stars", "Content/Graphics/UI/stars.png" );
    chalo::TextureManager::Add( "button_bg",               "Content/Graphics/UI/button_bg.png" );
    chalo::TextureManager::Add( "menu_icons",              "Content/Graphics/UI/menu_icons.png" );
    chalo::MenuManager::LoadTextMenu( "game.chalomenu" );
    Init_LevelEndMenu();

    Init_Map();

    m_inputCooldownMax = 10;
    m_inputCooldown = 0;

    m_deadScreen.setFillColor( sf::Color( 255, 0, 0, 100 ) );
    m_deadScreen.setPosition( sf::Vector2f( 0, 0 ) );
    m_deadScreen.setSize( sf::Vector2f( MAP_WIDTH*TILE_WIDTH, MAP_HEIGHT*TILE_HEIGHT ) );

    chalo::InputManager::Setup();

    while ( !m_turnStack.empty() )
    {
        m_turnStack.pop();
    }
    PushStateToStack();

    Logger::StackPop();
}

void GameState::Init_Map()
{
    Logger::StackPush( s_className + "::" + __func__ );

    std::string world = chalo::Messager::Get( "WORLD" );
    std::string level = chalo::Messager::Get( "LEVEL" );
    std::string levelFilename = "LEVEL_" + Helper::ToString( world + "-" + level ) + ".tmx";
    Logger::OutValue( "levelFilename", levelFilename );

    TmxMap tmxMap;
    tmxMap.Load( "Content/Maps/", levelFilename );
    tmxMap.Debug();

    m_map.ConvertFrom( tmxMap );
    m_map.Debug();

    // Create objects
    std::vector<ChaloMapObjectData> objects = m_map.GetObjectData();
    for ( auto& obj : objects )
    {
        bool validObject = false;

        Object newObject;
        newObject.prop_movable.dimensions = sf::IntRect( 0, 0, 20, 20 );
        newObject.prop_movable.speed = 2;
        newObject.prop_animatable.frame = 0;
        newObject.prop_animatable.frameRect = sf::IntRect( 0, 0, 20, 20 );
        newObject.prop_character.objectType = ObjectType::NOTHING;

        // RAWR STARTING SPOT
        if ( Helper::Contains( obj.name, "rawr starting point" ) )
        {
            validObject = true;
            newObject.name = "ENTITY[rawr]";
            newObject.prop_character.type = "rawr";
            newObject.prop_character.collisionBehavior = CollisionBehavior::NOTHING;
            newObject.prop_movable.position = SFMLHelper::ConvertVector( obj.position );
            newObject.prop_movable.speed = 1;
            newObject.prop_movable.movementDirection = Direction::NONE;
            newObject.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-rawr" ) );
            newObject.prop_animatable.frameMax = 2;
            newObject.prop_animatable.facingDirection = Direction::SOUTH;
            newObject.prop_animatable.animates = true;
            newObject.prop_character.objectType = ObjectType::PLAYER;
            newObject.prop_character.sightType = SightType::AHEAD;
        }
        // ENEMIES
        else if ( Helper::Contains( obj.name, "enemy" ) )
        {
            newObject.name = "ENTITY[" + Helper::ToString( m_allObjects.size() ) + "][ENEMY][" + obj.name + "]";
            newObject.prop_character.collisionBehavior = CollisionBehavior::DAMAGE;
            newObject.prop_movable.position = SFMLHelper::ConvertVector( obj.position );
            newObject.prop_movable.movementDirection = Direction::NONE;
            newObject.prop_animatable.animates = true;
            newObject.prop_character.objectType = ObjectType::ENEMY;

            if      ( Helper::Contains( obj.name, "north" ) ) { newObject.prop_animatable.facingDirection = Direction::NORTH; }
            else if ( Helper::Contains( obj.name, "south" ) ) { newObject.prop_animatable.facingDirection = Direction::SOUTH; }
            else if ( Helper::Contains( obj.name, "west" ) )  { newObject.prop_animatable.facingDirection = Direction::WEST; }
            else if ( Helper::Contains( obj.name, "east" ) )  { newObject.prop_animatable.facingDirection = Direction::EAST; }
            else { newObject.prop_animatable.facingDirection = Direction::ALL; }

            if ( Helper::Contains( obj.name, "static robot" ) )
            {
                validObject = true;
                newObject.prop_character.type = "enemy static robot";
                newObject.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-static-robot" ) );
                newObject.prop_animatable.frameMax = 2;
                newObject.prop_character.sightType = SightType::AHEAD;
            }
            else if ( Helper::Contains( obj.name, "rotating robot" ) )
            {
                validObject = true;
                newObject.prop_character.type = "enemy rotating robot";
                newObject.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-rotating-robot" ) );
                newObject.prop_animatable.frameMax = 2;
                newObject.prop_character.sightType = SightType::AHEAD;
            }
            else if ( Helper::Contains( obj.name, "patrol robot" ) )
            {
                validObject = true;
                newObject.prop_character.type = "enemy patrol robot";
                newObject.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-patrol-robot" ) );
                newObject.prop_animatable.frameMax = 2;
                newObject.prop_character.sightType = SightType::AHEAD;
            }
            else if ( Helper::Contains( obj.name, "all direction robot" ) )
            {
                validObject = true;
                newObject.prop_character.type = "all direction robot";
                newObject.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-alldirection-robot" ) );
                newObject.prop_animatable.frameMax = 1;
                newObject.prop_character.sightType = SightType::FOUR_DIRECTION;
            }
        }
//        else if ( Helper::Contains( obj.name, "wall", false ) )
//        {
//            validObject = true;
//            newObject.prop_character.type = "wall";
//            newObject.prop_animatable.animates = false;
//            newObject.prop_animatable.frameMax = 1;
//            newObject.prop_character.sightType = SightType::NONE;
//            newObject.prop_character.objectType = ObjectType::WALL;
//        }
        else
        {
            newObject.name = "ENTITY[" + Helper::ToString( m_allObjects.size() ) + "][OBJECT][" + obj.name + "]";
            newObject.prop_movable.position = SFMLHelper::ConvertVector( obj.position );
            newObject.prop_movable.movementDirection = Direction::NONE;
            newObject.prop_animatable.animates = false;
            newObject.prop_animatable.frameMax = 1;
            newObject.prop_animatable.frameRect = sf::IntRect( 0, 20, 20, 20 );
            newObject.prop_character.objectType = ObjectType::OBJECT;
            newObject.prop_character.sightType = SightType::NONE;

            // DOORS/SWITCHES
            if ( Helper::Contains( obj.name, "exit toggle switch" ) )
            {
                validObject = true;
                newObject.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-object-key" ) );
                newObject.prop_character.type = "exit switch";
                newObject.prop_character.collisionBehavior = CollisionBehavior::TOGGLE;
                newObject.prop_character.specialId = Helper::StringToInt( obj.name.substr( 0, 1 ) );
            }
            else if ( Helper::Contains( obj.name, "exit toggle door" ) )
            {
                validObject = true;
                newObject.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-object-exitdoor" ) );
                newObject.prop_character.type = "exit door";
                newObject.prop_character.collisionBehavior = CollisionBehavior::BLOCK;
                newObject.prop_character.specialId = Helper::StringToInt( obj.name.substr( 0, 1 ) );
            }
            else if ( Helper::Contains( obj.name, "toggle" ) && Helper::Contains( obj.name, "switch" ) )
            {
                validObject = true;
                newObject.prop_character.type = "switch";
                newObject.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-object-button" ) );
                newObject.prop_character.collisionBehavior = CollisionBehavior::TOGGLE;
                newObject.prop_character.objectType = ObjectType::TOGGLE_SWITCH;
                // "toggle3 switch up"
                newObject.prop_character.specialId = Helper::StringToInt( obj.name.substr( 0, 1 ) );
            }
            else if ( Helper::Contains( obj.name, "toggle" ) && Helper::Contains( obj.name, "door" ) )
            {
                validObject = true;
                newObject.prop_character.type = "door";
                newObject.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-object-door" ) );
                newObject.prop_character.collisionBehavior = CollisionBehavior::BLOCK;
                newObject.prop_character.objectType = ObjectType::TOGGLE_GATE;
                // "toggle3 door closed"
                newObject.prop_character.specialId = Helper::StringToInt( obj.name.substr( 0, 1 ) );
            }
            else if ( Helper::Contains( obj.name, "toggle" ) && Helper::Contains( obj.name, "cage" ) )
            {
                validObject = true;
                newObject.prop_character.type = "door";
                newObject.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-object-jaildoor" ) );
                newObject.prop_character.collisionBehavior = CollisionBehavior::BLOCK;
                newObject.prop_character.objectType = ObjectType::TOGGLE_GATE;
                // "toggle5 cage closed"
                newObject.prop_character.specialId = Helper::StringToInt( obj.name.substr( 0, 1 ) );
            }
            // OTHER OBJECTS
            else if ( Helper::Contains( obj.name, "crate" ) )
            {
                validObject = true;
                newObject.prop_character.type = "crate";
                newObject.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-object-crate" ) );
                newObject.prop_character.collisionBehavior = CollisionBehavior::SHOVE;
            }
            else if ( Helper::Contains( obj.name, "egg" ) )
            {
                validObject = true;
                newObject.prop_character.type = "egg";
                newObject.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-object-egg" ) );
                newObject.prop_character.collisionBehavior = CollisionBehavior::PICKUP;
                m_totalEggs++;
            }
            else if ( Helper::Contains( obj.name, "bomb" ) )
            {
                validObject = true;
                newObject.prop_character.type = "bomb";
                newObject.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-object-bomb" ) );
                newObject.prop_character.collisionBehavior = CollisionBehavior::PICKUP;
            }
            else if ( Helper::Contains( obj.name, "key" ) )
            {
                validObject = true;
                newObject.prop_character.type = "key";
                newObject.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "sheet-object-key" ) );
                newObject.prop_character.collisionBehavior = CollisionBehavior::PICKUP;
            }
        }

        if ( validObject )
        {
            m_allObjects.push_back( newObject );
        }
    }

    // Get the wall tiles and create objects based on them.
    std::vector<ChaloTile> wallTiles = m_map.GetSolidWalls();
    for ( auto& tile : wallTiles )
    {
        Object newObject;
        newObject.prop_movable.position = tile.GetPosition();
        newObject.prop_movable.dimensions = sf::IntRect( 0, 0, 20, 20 );
        newObject.prop_movable.speed = 0;
        newObject.prop_animatable.frame = 0;
        newObject.prop_animatable.frameRect = sf::IntRect( 0, 0, 20, 20 );
        newObject.prop_character.objectType = ObjectType::WALL;
        newObject.prop_character.sightType = SightType::NONE;
        newObject.prop_character.collisionBehavior = CollisionBehavior::BLOCK;
        newObject.prop_character.type = "wall";
        newObject.name = "ENTITY[" + Helper::ToString( m_allObjects.size() ) + "][WALL][" + tile.GetName() + "]";
        newObject.prop_animatable.sprite.setTexture( chalo::TextureManager::Get( "tileset" ) );
        m_allObjects.push_back( newObject );
    }


    m_playerIndex = -1;
    m_exitIndex = -1;
    for ( size_t i = 0; i < m_allObjects.size(); i++ )
    {
        if ( Helper::Contains( m_allObjects[i].name, "rawr", false ) )
        {
            m_playerIndex = i;
        }
        else if ( Helper::Contains( m_allObjects[i].name , "exit toggle door", false ) )
        {
            m_exitIndex = i;
        }

        if ( m_playerIndex != -1 && m_exitIndex != -1 ) { break; }
    }

    Logger::StackPop();
}

void GameState::Init_Object( ChaloMapObjectData object )
{
    Logger::StackPush( s_className + "::" + __func__ );

    Logger::StackPop();
}

void GameState::Init_LevelEndMenu()
{
    Logger::StackPush( s_className + "::" + __func__ );

    // TODO: Maybe add a "per layer" translation.
    int moveAmount = -1000;
    chalo::MenuManager::GetImage( "levelend", "backgroundframe" ).Move( sf::Vector2f( moveAmount, 0 ) );
    chalo::MenuManager::GetLabel( "levelend", "lblGrats" ).Move( sf::Vector2f( moveAmount, 0 ) );
    chalo::MenuManager::GetImage( "levelend", "starScore1" ).Move( sf::Vector2f( moveAmount, 0 ) );
    chalo::MenuManager::GetImage( "levelend", "starScore2" ).Move( sf::Vector2f( moveAmount, 0 ) );
    chalo::MenuManager::GetImage( "levelend", "starScore3" ).Move( sf::Vector2f( moveAmount, 0 ) );
    chalo::MenuManager::GetButton( "levelend", "btnNextLevel" ).Move( sf::Vector2f( moveAmount, 0 ) );
    chalo::MenuManager::GetButton( "levelend", "btnRestartLevel" ).Move( sf::Vector2f( moveAmount, 0 ) );
    chalo::MenuManager::GetButton( "levelend", "btnBackToLevelSelect" ).Move( sf::Vector2f( moveAmount, 0 ) );
    chalo::MenuManager::GetLabel( "levelend", "lblRank1" ).Move( sf::Vector2f( moveAmount, 0 ) );
    chalo::MenuManager::GetLabel( "levelend", "lblRank2" ).Move( sf::Vector2f( moveAmount, 0 ) );
    chalo::MenuManager::GetLabel( "levelend", "lblRank3" ).Move( sf::Vector2f( moveAmount, 0 ) );

    Logger::StackPop();
}

void GameState::Cleanup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    chalo::MenuManager::Cleanup();
    m_allObjects.clear();
    m_map.Clear();
    while ( !m_turnStack.empty() )
    {
        m_turnStack.pop();
    }
    Logger::StackPop();
}

void GameState::Update()
{
    Logger::StackPush( s_className + "::" + __func__ );

    if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Escape ) )
    {
        SetGotoState( "levelSelectState" );
    }

    // Object updates
    for ( auto& obj : m_allObjects )
    {
        obj.Update();

        // TODO: THis doesn't have to be calculated every cycle, could just be done at beginning
        // and ending of update turn transition time?
        obj.UpdateVision( m_allObjects, m_map.GetSolidTileRegions(), m_map.GetMapDimensions() );
    }

    HandleInput();
    CheckForLevelEndState();

    if ( m_inputCooldown > 0 )
    {
        m_inputCooldown--;
    }

    chalo::MenuManager::GetLabel( "main", "lblEggCount" ).SetText( "x " + Helper::ToString( m_allObjects[m_playerIndex].prop_player.totalEggs ) );
    chalo::MenuManager::GetLabel( "main", "lblBombCount" ).SetText( "x " + Helper::ToString( m_allObjects[m_playerIndex].prop_player.totalBombs ) );

    HandleMouse();

    Logger::StackPop();
}

void GameState::Draw( sf::RenderTexture& window )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Draw_Map( window );
    Draw_Objects( window );
    chalo::MenuManager::Draw( window );

    if ( m_playerDead )
    {
        window.draw( m_deadScreen );
    }

    Logger::StackPop();
}

void GameState::Draw_Map( sf::RenderTexture& window )
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_map.Draw( window );
    Logger::StackPop();
}

void GameState::Draw_Objects( sf::RenderTexture& window )
{
    for ( auto& obj : m_allObjects )
    {
//        obj.Draw( window );
    }

    for ( int i = m_allObjects.size() - 1; i >= 0; i-- )
    {
        m_allObjects[i].Draw( window );
    }
}

Object& GameState::GetPlayer()
{
    // TODO: Make this more efficient
    for ( auto& obj : m_allObjects )
    {
        if ( Helper::Contains( obj.name, "rawr" ) )
        {
            return obj;
        }
    }

    throw runtime_error( "No player object found!" );
}

void GameState::HandleInput()
{
    Logger::StackPush( s_className + "::" + __func__ );

    if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::InputAction::INPUT_ACTION2 ) ) )
    {
        PopStackGoBack();
    }

    if ( m_playerDead )
    {
        Logger::StackPop();
        return;
    }
    if ( m_levelEnd )
    {
        Logger::StackPop();
        return;
    }

    Object& playerRef = GetPlayer();

    if ( playerRef.prop_movable.IsTransitioning() )
    {
        // Can't move while currently moving
        Logger::StackPop();
        return;
    }

    Direction wantToMove = Direction::NONE;

    if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::InputAction::INPUT_NORTH ) ) )
    {
        wantToMove = Direction::NORTH;
    }
    else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::InputAction::INPUT_SOUTH ) ) )
    {
        wantToMove = Direction::SOUTH;
    }
    else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::InputAction::INPUT_WEST ) ) )
    {
        wantToMove = Direction::WEST;
    }
    else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::InputAction::INPUT_EAST ) ) )
    {
        wantToMove = Direction::EAST;
    }

    if ( wantToMove != Direction::NONE )
    {
        playerRef.prop_animatable.facingDirection = wantToMove;
    }

    if ( wantToMove == Direction::NONE )
    {
        // Player doesn't want to move
        Logger::StackPop();
        return;
    }

    if ( playerRef.CanObjectMoveInDirection( m_allObjects, m_map.GetSolidTileRegions(), wantToMove ) )
    {
        PushStateToStack();
        playerRef.prop_movable.BeginMove( wantToMove );
        for ( auto& obj : m_allObjects )
        {
            obj.UpdateTurn( m_allObjects, m_map.GetSolidTileRegions() );
        }
    }

    Logger::StackPop();
}

void GameState::HandleMouse()
{
    Logger::StackPush( s_className + "::" + __func__ );

    chalo::MenuManager::Update();
    std::string clickedButton = chalo::MenuManager::GetClickedButton();

    if ( clickedButton == "btnNextLevel" )
    {
        GotoNextLevel();
    }
    else if ( clickedButton == "btnRestartLevel" )
    {
        RestartLevel();
    }
    else if ( clickedButton == "btnBackToLevelSelect" )
    {
        SetGotoState( "levelSelectState" );
    }
    Logger::StackPop();
}

void GameState::LevelOver()
{
    Logger::StackPush( s_className + "::" + __func__ );

    m_levelEnd = true;

    // Move the end menu back on screen
    int moveAmount = 1000;
    chalo::MenuManager::GetImage( "levelend", "backgroundframe" ).Move( sf::Vector2f( moveAmount, 0 ) );
    chalo::MenuManager::GetLabel( "levelend", "lblGrats" ).Move( sf::Vector2f( moveAmount, 0 ) );
    chalo::MenuManager::GetImage( "levelend", "starScore1" ).Move( sf::Vector2f( moveAmount, 0 ) );
    chalo::MenuManager::GetImage( "levelend", "starScore2" ).Move( sf::Vector2f( moveAmount, 0 ) );
    chalo::MenuManager::GetImage( "levelend", "starScore3" ).Move( sf::Vector2f( moveAmount, 0 ) );
    chalo::MenuManager::GetButton( "levelend", "btnNextLevel" ).Move( sf::Vector2f( moveAmount, 0 ) );
    chalo::MenuManager::GetButton( "levelend", "btnRestartLevel" ).Move( sf::Vector2f( moveAmount, 0 ) );
    chalo::MenuManager::GetButton( "levelend", "btnBackToLevelSelect" ).Move( sf::Vector2f( moveAmount, 0 ) );
    chalo::MenuManager::GetLabel( "levelend", "lblRank1" ).Move( sf::Vector2f( moveAmount, 0 ) );
    chalo::MenuManager::GetLabel( "levelend", "lblRank2" ).Move( sf::Vector2f( moveAmount, 0 ) );
    chalo::MenuManager::GetLabel( "levelend", "lblRank3" ).Move( sf::Vector2f( moveAmount, 0 ) );

    // Update the stars - first star is always filled (level is completed)
    int totalStars = 1;

    sf::Color successColor = sf::Color( 255, 255, 0 );
    sf::Color failColor = sf::Color( 100, 100, 100 );

    // Any rewinds?
    if ( m_rewindCount == 0 )
    {
        totalStars++;
        chalo::MenuManager::GetLabel( "levelend", "lblRank3" ).SetText( chalo::LanguageManager::Text( "no_rewinds" ) );
        chalo::MenuManager::GetLabel( "levelend", "lblRank3" ).SetColor( successColor );
    }
    else
    {
        chalo::MenuManager::GetLabel( "levelend", "lblRank3" ).SetText( chalo::LanguageManager::Text( "rewinds_occurred" ) );
        chalo::MenuManager::GetLabel( "levelend", "lblRank3" ).SetColor( failColor );
    }

    // Got all eggs
    if ( m_allObjects[m_playerIndex].prop_player.totalEggs == m_totalEggs )
    {
        totalStars++;
        chalo::MenuManager::GetLabel( "levelend", "lblRank2" ).SetText( chalo::LanguageManager::Text( "got_all_eggs" ) );
        chalo::MenuManager::GetLabel( "levelend", "lblRank2" ).SetColor( successColor );
    }
    else
    {
        chalo::MenuManager::GetLabel( "levelend", "lblRank2" ).SetText( chalo::LanguageManager::Text( "didnt_get_all_eggs" ) );
        chalo::MenuManager::GetLabel( "levelend", "lblRank2" ).SetColor( failColor );
    }

    Logger::Out( "Total stars: " + Helper::ToString( totalStars ), s_className + "::" + __func__ );

    if ( totalStars >= 2 )
    {
        chalo::MenuManager::GetImage( "levelend", "starScore2" ).SetImageClipRect( sf::IntRect( 2*56, 0, 56, 56 ) );
    }
    else
    {
        chalo::MenuManager::GetImage( "levelend", "starScore2" ).SetImageClipRect( sf::IntRect( 1*56, 0, 56, 56 ) );
    }

    if ( totalStars >= 3 )
    {
        chalo::MenuManager::GetImage( "levelend", "starScore3" ).SetImageClipRect( sf::IntRect( 2*56, 0, 56, 56 ) );
    }
    else
    {
        chalo::MenuManager::GetImage( "levelend", "starScore3" ).SetImageClipRect( sf::IntRect( 1*56, 0, 56, 56 ) );
    }

    Logger::StackPop();
}

void GameState::RestartLevel()
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Restart level", s_className + "::" + __func__ );
    SetGotoState( "gameState" );
    Logger::StackPop();
}

void GameState::GotoNextLevel()
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Go to next level", s_className + "::" + __func__ );

    int currentLevel = Helper::StringToInt( chalo::Messager::Get( "LEVEL" ) );
    if ( currentLevel >= 5 )
    {
        SetGotoState( "levelSelectState" );
    }
    else
    {
        chalo::Messager::Set( "LEVEL", currentLevel+1 );
        SetGotoState( "gameState" );
    }

    SetGotoState( "gameState" );
    Logger::StackPop();
}

void GameState::CheckForLevelEndState()
{
    Logger::StackPush( s_className + "::" + __func__ );

    // Lose state:
    m_playerDead = false;
    chalo::MenuManager::GetLabel( "main", "lblReset" ).SetPosition( sf::Vector2f( -1000, 5 ) );
    for ( auto& obj : m_allObjects )
    {
        if ( obj.prop_character.objectType == ObjectType::ENEMY && obj.prop_character.canSeePlayer )
        {
            // ded
            m_playerDead = true;
            chalo::MenuManager::GetLabel( "main", "lblReset" ).SetPosition( sf::Vector2f( 100, 5 ) );
        }
    }

    // Win state:
    if ( m_levelEnd == false &&
        m_allObjects[m_playerIndex].prop_movable.position == m_allObjects[m_exitIndex].prop_movable.position )
    {
        LevelOver();
    }

    Logger::StackPop();
}

void GameState::PushStateToStack()
{
    std::vector<ObjectState> objectStates;
    for ( size_t i = 0; i < m_allObjects.size(); i++ )
    {
        ObjectState currentState;
        currentState.position   = m_allObjects[i].prop_movable.position;
        currentState.direction  = m_allObjects[i].prop_animatable.facingDirection;
        currentState.frameRect  = m_allObjects[i].prop_animatable.frameRect;
        currentState.collisionBehavior  = m_allObjects[i].prop_character.collisionBehavior;
        currentState.totalBombs = m_allObjects[i].prop_player.totalBombs;
        currentState.totalEggs = m_allObjects[i].prop_player.totalEggs;
        currentState.index      = i;
        objectStates.push_back( currentState );
    }
    m_turnStack.push( objectStates );
}

void GameState::PopStackGoBack()
{
    if ( m_turnStack.size() == 0 ) { return; }
    if ( m_inputCooldown > 0 ) { return; }

    m_rewindCount++;

    for ( size_t i = 0; i < m_allObjects.size(); i++ )
    {
        ObjectState previousState = m_turnStack.top()[i];
        m_allObjects[i].prop_movable.position = previousState.position;
        m_allObjects[i].prop_animatable.facingDirection = previousState.direction;
        m_allObjects[i].prop_animatable.frameRect = previousState.frameRect;
        m_allObjects[i].prop_character.collisionBehavior = previousState.collisionBehavior;
        m_allObjects[i].prop_player.totalBombs = previousState.totalBombs;
        m_allObjects[i].prop_player.totalEggs = previousState.totalEggs;
        m_inputCooldown = m_inputCooldownMax;
    }
    m_turnStack.pop();
}

