#include "Object.hpp"

#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"
#include "../chalo-engine/Utilities/Helper.hpp"
#include "../chalo-engine/Utilities_SFML/SFMLHelper.hpp"

std::string Object::s_className = "Object";

Object::Object()
{
    debug_text.Setup( "txt", "main", 8, sf::Color::Red, sf::Vector2f( 0, 0 ), "hi" );
    prop_character.specialId = -1;
    prop_player.totalBombs = 0;
    prop_player.totalEggs = 0;
    prop_player.totalEggs = 0;
}

void Object::Update()
{
    prop_movable.Update();
    #ifdef DEBUG
    if ( prop_character.specialId != -1 )
    {
        debug_text.SetText( Helper::ToString( prop_character.specialId ) );
        debug_text.SetPosition( sf::Vector2f( prop_movable.position.x, prop_movable.position.y ) );
    }
    #endif
}

void Object::UpdateTurn( std::vector<Object>& allObjects, std::vector<sf::IntRect>& solidTiles )
{
    prop_animatable.IncrementFrame();

    if      ( Helper::Contains( prop_character.type, "static robot" ) )
    {
    }
    else if ( Helper::Contains( prop_character.type, "rotating robot" ) )
    {
        // Turner rotates each 2 turns
        if ( prop_animatable.IsUpdateFrame() )
        {
            prop_animatable.Rotate( RotationDirection::CLOCKWISE );
        }
    }
    else if ( Helper::Contains( prop_character.type, "patrol robot" ) )
    {
        // 2dir moves forward (left/right or up/down) and turns around when hitting a wall
        // Map h as to handle this, for collision.
        if ( prop_animatable.IsUpdateFrame() )
        {
            if ( CanObjectMoveInDirection( allObjects, solidTiles, prop_animatable.facingDirection ) )
            {
                prop_movable.BeginMove( prop_animatable.facingDirection );
            }
            else
            {
                // Flip direction
                prop_animatable.FlipDirection();
            }
        }
    }
}

void Object::UpdateVision( std::vector<Object>& allObjects, std::vector<sf::IntRect>& solidTiles, sf::Vector2i mapDimensions )
{
    prop_character.debugSeenPoints.clear();
    prop_character.seenObjectIndices.clear();
    prop_character.seenObjectIndices = CanSeeObjectIndices( allObjects, mapDimensions );

    // TEMPORARY: FOR DEBUG
    for ( auto& index : prop_character.seenObjectIndices )
    {
        Object& obj = allObjects[ index ];
        prop_character.debugSeenPoints.push_back( sf::Vector2f( obj.prop_movable.position.x + obj.prop_movable.dimensions.width / 2, obj.prop_movable.position.y + obj.prop_movable.dimensions.height / 2 ) );
    }
}

void Object::Draw( sf::RenderTexture& window )
{
    Logger::StackPush( s_className + "::" + __func__ );

    if ( prop_character.objectType != ObjectType::WALL )
    {
        prop_animatable.UpdateSprite( prop_movable.position, prop_movable.dimensions );
        prop_animatable.Draw( window );
    }

    #ifdef DEBUG
    DrawDebug( window );
    #endif

    Logger::StackPop();
}

void Object::Debug()
{
    Logger::StackPush( s_className + "::" + __func__ );

    std::string text = "OBJECTINFO FOR " + name + "...<br>";
    text += "ANIMATABLE -- frame: " + Helper::ToString( prop_animatable.frame );
    text += ", frameMax: " + Helper::ToString( prop_animatable.frameMax );
    text += ", direction: " + Helper::ToString( static_cast<int>(prop_animatable.facingDirection) );
    text += ", frameRect: " + SFMLHelper::RectangleToString( prop_animatable.frameRect ) + "<br>";
    text += "CHARACTER -- type: " + prop_character.type + "<br>";
    text += "POSITIONABLE -- position: " + SFMLHelper::CoordinateToString( prop_movable.position );
    text += ", dimensions: " + SFMLHelper::DimensionsToString( prop_movable.dimensions );
    text += "<br>";
    Logger::Debug( text, s_className + "::" + __func__ );

    Logger::StackPop();
}

void Object::DrawDebug( sf::RenderTexture& window )
{
    sf::RectangleShape rect;
    rect.setFillColor( sf::Color::Transparent );
    rect.setOutlineThickness( 1 );
    rect.setPosition( prop_movable.position );
    rect.setSize( sf::Vector2f( prop_movable.dimensions.width, prop_movable.dimensions.height ) );

    if ( prop_character.objectType == ObjectType::ENEMY )
    {
        rect.setOutlineColor( sf::Color( 255, 0, 0, 100 ) );
    }
    else if ( prop_character.objectType == ObjectType::PLAYER )
    {
        rect.setOutlineColor( sf::Color( 0, 255, 0, 100 ) );
    }
    else if ( prop_character.objectType == ObjectType::WALL )
    {
        rect.setOutlineColor( sf::Color( 255, 255, 0, 100 ) );
    }
    else if ( prop_character.objectType == ObjectType::OBJECT )
    {
        rect.setOutlineColor( sf::Color( 0, 0, 255, 100 ) );
    }

    sf::Vertex linepts[2];
    if ( prop_character.objectType == ObjectType::ENEMY )
    {
        linepts[0] = sf::Vertex( sf::Vector2f( prop_movable.position.x + prop_movable.dimensions.width/2+1, prop_movable.position.y + prop_movable.dimensions.height/2+1 ), sf::Color::Red );
    }
    else if ( prop_character.objectType == ObjectType::PLAYER )
    {
        linepts[0] = sf::Vertex( sf::Vector2f( prop_movable.position.x + prop_movable.dimensions.width/2-1, prop_movable.position.y + prop_movable.dimensions.height/2-1 ), sf::Color::Green );
    }
    for ( auto& point : prop_character.debugSeenPoints )
    {
        linepts[1] = sf::Vertex( point, sf::Color::Black );
        window.draw( linepts, 2, sf::Lines );
    }

    window.draw( rect );
    window.draw( debug_text.GetText() );
}

sf::IntRect Object::GetCollisionRect()
{
    return sf::IntRect( prop_movable.position.x, prop_movable.position.y, prop_movable.dimensions.width, prop_movable.dimensions.height );
}

bool Object::CanObjectMoveInDirection( std::vector<Object>& allObjects, std::vector<sf::IntRect>& solidTiles, Direction direction )
{
    Logger::StackPush( s_className + "::" + __func__ + " (" + name + ")" );

    if ( direction == Direction::NONE )
    {
        Logger::StackPop();
        return false;
    }

    sf::IntRect objectRect = GetCollisionRect();

    switch( direction )
    {
        case Direction::NORTH: objectRect.top  -= 20; break;
        case Direction::SOUTH: objectRect.top  += 20; break;
        case Direction::WEST:  objectRect.left -= 20; break;
        case Direction::EAST:  objectRect.left += 20; break;
    }

    bool canMove = true;

    // Check for collision with OBJECTS
    for ( auto& otherObject : allObjects )
    {
        if ( name == otherObject.name ) { continue; } // Skip if this is itself

        if ( SFMLHelper::BoundingBoxCollision( objectRect, otherObject.GetCollisionRect() ) )
        {
            if ( otherObject.prop_character.collisionBehavior == CollisionBehavior::BLOCK )
            {
                canMove = false;
            }
            else if ( otherObject.prop_character.collisionBehavior == CollisionBehavior::SHOVE )
            {
                // Enemies can't shove things
                if ( Helper::Contains( prop_character.type, "enemy" ) )
                {
                    canMove = false;
                }
                else if ( !otherObject.CanObjectMoveInDirection( allObjects, solidTiles, direction ) )
                {
                    canMove = false;
                }
                else
                {
                    // can move
                    otherObject.prop_movable.BeginMove( direction );
                }
            }
            else if ( otherObject.prop_character.collisionBehavior == CollisionBehavior::PICKUP )
            {
                // can move
                if ( prop_character.objectType == ObjectType::PLAYER )
                {
                    Collect( otherObject );
                }
            }
            else if ( otherObject.prop_character.collisionBehavior == CollisionBehavior::DAMAGE )
            {
                // Can't move into enemies
                canMove = false;
            }
            else if ( otherObject.prop_character.collisionBehavior == CollisionBehavior::TOGGLE )
            {
                canMove = true;

                if ( Helper::Contains( otherObject.name, "exit toggle", false ) && prop_character.objectType != ObjectType::PLAYER )
                {
                    // No toggle
                }
                else
                {
                    Collect( otherObject );
                    // Trigger the other object's toggle
                    otherObject.Toggle( allObjects );
                }
            }
        }
    }

    // Check for collision with WALLS/SOLID tiles
    for ( auto& solidTile : solidTiles )
    {
        if ( SFMLHelper::BoundingBoxCollision( objectRect, solidTile ) )
        {
            canMove = false;
        }
    }

    Logger::StackPop();
    return canMove;
}

/**
Meant for player, but could be used for other things.
Check to see what objects this object can see based on its line of sight.
Return a vector of these objects.
*/
std::vector<int> Object::CanSeeObjectIndices( std::vector<Object>& allObjects, sf::Vector2i mapDimensions )
{
    Logger::StackPush( s_className + "::" + __func__ + " (" + name + ")" );

    std::vector<int> seenObjectIndices;
    prop_character.canSeePlayer = false;

    if ( prop_character.sightType == SightType::NONE )
    {
//        Logger::Out( "Object \"" + name + "\" has no sight.", s_className + "::" + __func__ + " (" + name + ")" );
        Logger::StackPop();
        return seenObjectIndices;
    }

    // Tile-based checking, not pixel-based.
    sf::Vector2f objPos = sf::Vector2f( prop_movable.position.x / prop_movable.dimensions.width, prop_movable.position.y / prop_movable.dimensions.height );
    sf::Vector2f startSight = objPos;
    sf::Vector2f endSight = objPos;

    if ( prop_character.sightType == SightType::AHEAD )
    {
        if ( prop_animatable.facingDirection == Direction::NORTH )
        {
            endSight.y = 0;
        }
        else if ( prop_animatable.facingDirection == Direction::SOUTH )
        {
            endSight.y = mapDimensions.y;
        }
        else if ( prop_animatable.facingDirection == Direction::WEST )
        {
            endSight.x = 0;
        }
        else if ( prop_animatable.facingDirection == Direction::EAST )
        {
            endSight.x = mapDimensions.x;
        }
    }
    else if ( prop_character.sightType == SightType::FOUR_DIRECTION )
    {
        // These are two independent lines, not a box. The object can see (0,400) in an X range on its own Y axis
        // and (0,240) on the Y range at its own X axis.
        startSight.x = 0;
        endSight.x = mapDimensions.x;
        startSight.y = 0;
        endSight.y = mapDimensions.y;
    }

    int closestLeftIndex = -1;
    int closestRightIndex = -1;
    int closestUpIndex = -1;
    int closestDownIndex = -1;
    for ( size_t i = 0; i < allObjects.size(); i++ )
    {
        if ( name == allObjects[i].name )                          { continue; } // Skip if this is itself

        // Two objects aligned on the Y axis (same vertical location)
        if ( prop_movable.position.y == allObjects[i].prop_movable.position.y )
        {

            if ( allObjects[i].prop_movable.position.x < prop_movable.position.x        // OBJECT 2 is to the left of OBJECT 1
                && prop_character.sightType == SightType::AHEAD
                && prop_animatable.facingDirection == Direction::WEST
            )
            {
                // If you don't already have a closest index set, OR you are closer than the previous closest index, update this.
                if ( closestLeftIndex == -1 ||
                    allObjects[i].prop_movable.position.x > allObjects[closestLeftIndex].prop_movable.position.x )
                {
                    closestLeftIndex = i;
                }
            }
            else if ( allObjects[i].prop_movable.position.x > prop_movable.position.x        // OBJECT 2 is to the right of OBJECT 1
                && prop_character.sightType == SightType::AHEAD
                && prop_animatable.facingDirection == Direction::EAST
            )
            {
                // If you don't already have a closest index set, OR you are closer than the previous closest index, update this.
                if ( closestRightIndex == -1 ||
                    allObjects[i].prop_movable.position.x < allObjects[closestRightIndex].prop_movable.position.x )
                {
                    closestRightIndex = i;
                }
            }
        }

        // Two objects aligned on the X axis (same horizontal location)
        if ( prop_movable.position.x == allObjects[i].prop_movable.position.x )
        {
            if ( name == allObjects[i].name )                          { continue; } // Skip if this is itself

            if ( allObjects[i].prop_movable.position.y < prop_movable.position.y        // OBJECT 2 is above OBJECT 1
                && prop_character.sightType == SightType::AHEAD
                && prop_animatable.facingDirection == Direction::NORTH
            )
            {
                // If you don't already have a closest index set, OR you are closer than the previous closest index, update this.
                if ( closestUpIndex == -1 ||
                allObjects[i].prop_movable.position.y > allObjects[closestUpIndex].prop_movable.position.y )
                {
                    closestUpIndex = i;
                }
            }
            else if ( allObjects[i].prop_movable.position.y > prop_movable.position.y        // OBJECT 2 is below OBJECT 1
                && prop_character.sightType == SightType::AHEAD
                && prop_animatable.facingDirection == Direction::SOUTH
            )
            {
                // If you don't already have a closest index set, OR you are closer than the previous closest index, update this.
                if ( closestDownIndex == -1 ||
                allObjects[i].prop_movable.position.y < allObjects[closestDownIndex].prop_movable.position.y )
                {
                    closestDownIndex = i;
                }
            }
        }
    }

    if ( closestDownIndex != -1 )
    {
        if ( Helper::Contains( allObjects[closestDownIndex].name, "rawr", false ) ) { prop_character.canSeePlayer = true; }
        seenObjectIndices.push_back( closestDownIndex );
    }
    if ( closestUpIndex != -1 )
    {
        if ( Helper::Contains( allObjects[closestUpIndex].name, "rawr", false ) ) { prop_character.canSeePlayer = true; }
        seenObjectIndices.push_back( closestUpIndex );
    }
    if ( closestLeftIndex != -1 )
    {
        if ( Helper::Contains( allObjects[closestLeftIndex].name, "rawr", false ) ) { prop_character.canSeePlayer = true; }
        seenObjectIndices.push_back( closestLeftIndex );
    }
    if ( closestRightIndex != -1 )
    {
        if ( Helper::Contains( allObjects[closestRightIndex].name, "rawr", false ) ) { prop_character.canSeePlayer = true; }
        seenObjectIndices.push_back( closestRightIndex );
    }

    Logger::StackPop();
    return seenObjectIndices;
}

void Object::Toggle( std::vector<Object>& allObjects )
{
    Logger::StackPush( s_className + "::" + __func__ + " (" + name + ")" );

    prop_animatable.frame = 2;
    prop_animatable.frameRect = sf::IntRect( 20*2, 20*1, 20, 20 );
    prop_character.collisionBehavior = CollisionBehavior::NOTHING;

    for ( auto& obj : allObjects )
    {
        if ( obj.prop_character.specialId == prop_character.specialId )
        {
            obj.prop_animatable.frame = 2;
            obj.prop_animatable.frameRect = sf::IntRect( 20*2, 20*1, 20, 20 );
            obj.prop_character.collisionBehavior = CollisionBehavior::NOTHING;
        }
    }

    Logger::StackPop();
}

void Object::Collect( Object& otherObject )
{
    if ( Helper::Contains( otherObject.name, "egg", false ) )
    {
        prop_player.totalEggs++;
    }
    else if (  Helper::Contains( otherObject.name, "bomb", false ) )
    {
        prop_player.totalBombs++;
    }
    otherObject.prop_movable.position.x = -100; // easy I guess :P
}
