#ifndef _ENUMS_HPP
#define _ENUMS_HPP

#include <string>


enum class Direction {
    NORTH = 0,
    EAST  = 1,
    SOUTH = 2,
    WEST  = 3,
    NONE,
    ALL
};

inline std::string EnumToString( Direction enumVal )
{
    switch( enumVal )
    {
        case Direction::NORTH:      return "NORTH"; break;
        case Direction::EAST:       return "EAST"; break;
        case Direction::SOUTH:      return "SOUTH"; break;
        case Direction::WEST:       return "WEST"; break;
        case Direction::NONE:       return "NONE"; break;
        case Direction::ALL:        return "ALL"; break;
        default: return "unknown";
    }
}


enum class RotationDirection {
    CLOCKWISE,
    COUNTERCLOCKWISE
};

inline std::string EnumToString( RotationDirection enumVal )
{
    switch( enumVal )
    {
        case RotationDirection::CLOCKWISE:          return "CLOCKWISE"; break;
        case RotationDirection::COUNTERCLOCKWISE:   return "COUNTERCLOCKWISE"; break;
        default: return "unknown";
    }
}


enum class CollisionBehavior {
    NOTHING,
    BLOCK,
    SHOVE,
    PICKUP,
    DAMAGE,
    TOGGLE
};

inline std::string EnumToString( CollisionBehavior enumVal )
{
    switch( enumVal )
    {
        case CollisionBehavior::NOTHING: return "NOTHING"; break;
        case CollisionBehavior::BLOCK:   return "BLOCK"; break;
        case CollisionBehavior::SHOVE:   return "SHOVE"; break;
        case CollisionBehavior::PICKUP:  return "PICKUP"; break;
        case CollisionBehavior::DAMAGE:  return "DAMAGE"; break;
        case CollisionBehavior::TOGGLE:  return "TOGGLE"; break;
        default: return "unknown";
    }
}


enum class ObjectType {
    NOTHING,
    PLAYER,
    ENEMY,
    OBJECT,
    WALL,
    TOGGLE_SWITCH,
    TOGGLE_GATE,
    DECOR
};

inline std::string EnumToString( ObjectType enumVal )
{
    switch( enumVal )
    {
        case ObjectType::NOTHING: return "NOTHING"; break;
        case ObjectType::PLAYER:  return "PLAYER"; break;
        case ObjectType::ENEMY:   return "ENEMY"; break;
        case ObjectType::OBJECT:  return "OBJECT"; break;
        case ObjectType::WALL:    return "WALL"; break;
        case ObjectType::DECOR:   return "DECOR"; break;
        default: return "unknown";
    }
}


enum class SightType {
    NONE,
    AHEAD,
    AHEAD_BEHIND,
    FOUR_DIRECTION
};

inline std::string EnumToString( SightType enumVal )
{
    switch( enumVal )
    {
        case SightType::NONE:           return "NONE"; break;
        case SightType::AHEAD:          return "AHEAD"; break;
        case SightType::AHEAD_BEHIND:   return "AHEAD_BEHIND"; break;
        case SightType::FOUR_DIRECTION:  return "FOUR_DIRECTION"; break;
        default: return "unknown";
    }
}

#endif
