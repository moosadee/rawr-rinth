#ifndef _MOVABLE_HPP
#define _MOVABLE_HPP

#include <SFML/Graphics.hpp>

#include "../Enums.hpp"

struct Movable
{
    sf::IntRect     dimensions;
    sf::Vector2f    position;
    float           speed;
    Direction       movementDirection;

    bool BeginMove( Direction dir );
    void Update();
    void Move();
    bool IsTransitioning();
};

#endif
