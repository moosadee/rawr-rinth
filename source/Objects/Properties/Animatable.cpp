#include "Animatable.hpp"

#include "../../chalo-engine/Utilities/Logger.hpp"

std::string Animatable::s_className = "Animatable";

Animatable::Animatable()
{
    frame = 0;
    frameMax = 2;
    animates = false;
}

void Animatable::IncrementFrame()
{
    Logger::StackPush( s_className + "::" + __func__ );

    if ( !animates )
    {
        Logger::StackPop();
        return;
    }

    frame += 1;
    if ( frame >= frameMax )
    {
        frame = 0;
    }

    Logger::StackPop();
}

void Animatable::UpdateSprite( sf::Vector2f position, sf::IntRect dimensions )
{
    if ( animates )
    {
        sprite.setTextureRect( sf::IntRect( int( frame ) * dimensions.width,
                                         static_cast<int>( facingDirection ) * dimensions.height,
                                         dimensions.width,
                                         dimensions.height
                                         ) );
    }
    else
    {
        sprite.setTextureRect( frameRect );
    }

    sprite.setPosition( position );
}

void Animatable::Draw( sf::RenderTexture& window )
{
    window.draw( sprite );
}

void Animatable::Rotate( RotationDirection rotDirection )
{
    // TODO: Add other directions?
    if      ( facingDirection == Direction::NORTH ) { facingDirection = Direction::EAST; }
    else if ( facingDirection == Direction::EAST  ) { facingDirection = Direction::SOUTH; }
    else if ( facingDirection == Direction::SOUTH ) { facingDirection = Direction::WEST; }
    else if ( facingDirection == Direction::WEST  ) { facingDirection = Direction::NORTH; }
    int rotationInt = static_cast<int>( facingDirection );
}

void Animatable::FlipDirection()
{
    if      ( facingDirection == Direction::NORTH ) { facingDirection = Direction::SOUTH; }
    else if ( facingDirection == Direction::EAST )  { facingDirection = Direction::WEST; }
    else if ( facingDirection == Direction::SOUTH ) { facingDirection = Direction::NORTH; }
    else if ( facingDirection == Direction::WEST )  { facingDirection = Direction::EAST; }
}

bool Animatable::IsUpdateFrame()
{
    return ( frame < 1 );
}


