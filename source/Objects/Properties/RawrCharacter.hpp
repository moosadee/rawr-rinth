#ifndef _RAWR_CHARACTER_HPP
#define _RAWR_CHARACTER_HPP

#include "../Enums.hpp"

#include <string>
#include <vector>

struct RawrCharacter
{
    std::string         type;
    CollisionBehavior   collisionBehavior;
    SightType           sightType;
    ObjectType          objectType;
    bool                canSeePlayer;
    int                 specialId; // For toggles
    std::vector<int>    seenObjectIndices;
    std::vector<sf::Vector2f>    debugSeenPoints;
};

#endif
