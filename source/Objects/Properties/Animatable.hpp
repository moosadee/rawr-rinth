#ifndef _ANIMATABLE_HPP
#define _ANIMATABLE_HPP

#include <SFML/Graphics.hpp>
#include <string>

#include "../Enums.hpp"

struct Animatable
{
    sf::Sprite  sprite;
    float       frame;
    float       frameMax;
    Direction   facingDirection;
    sf::IntRect frameRect;
    bool        animates;

    Animatable();
    void IncrementFrame();
    void UpdateSprite( sf::Vector2f position, sf::IntRect dimensions );
    void Draw( sf::RenderTexture& window );
    void Rotate( RotationDirection rotDirection );
    void FlipDirection();
    bool IsUpdateFrame();

private:
    static std::string s_className;
};

#endif
