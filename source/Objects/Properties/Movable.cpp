#include "Movable.hpp"

#include "../../chalo-engine/Utilities/Logger.hpp"
#include "../../chalo-engine/Utilities/Helper.hpp"
#include "../../chalo-engine/Utilities_SFML/SFMLHelper.hpp"

bool Movable::BeginMove( Direction dir )
{
    if ( IsTransitioning() )
    {
        return false;
    }

    movementDirection = dir;
    Move();

    return true;
}

void Movable::Update()
{
    if ( movementDirection == Direction::NONE )
    {
        return;
    }

    // Keep moving until %20 is 0
    if ( IsTransitioning() )
    {
        Move();
    }
    else
    {
        // Done moving
        movementDirection = Direction::NONE;
    }
}

void Movable::Move()
{
    if ( movementDirection == Direction::NORTH )
    {
        position.y -= speed;
    }
    else if ( movementDirection == Direction::SOUTH )
    {
        position.y += speed;
    }
    else if ( movementDirection == Direction::WEST )
    {
        position.x -= speed;
    }
    else if ( movementDirection == Direction::EAST )
    {
        position.x += speed;
    }
}

bool Movable::IsTransitioning()
{
    return ( int(position.x) % 20 != 0 || int(position.y) % 20 != 0 );
}
