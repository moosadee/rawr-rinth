#ifndef _OBJECT_HPP
#define _OBJECT_HPP

#include <SFML/Graphics.hpp>

#include <string>
#include <stack>

#include "../chalo-engine/UI/UILabel.hpp"

#include "Enums.hpp"
#include "Properties/Animatable.hpp"
#include "Properties/RawrCharacter.hpp"
#include "Properties/Movable.hpp"
#include "Properties/Player.hpp"

struct ObjectState
{
    sf::Vector2f        position;
    Direction           direction;
    int                 index;
    CollisionBehavior   collisionBehavior;
    sf::IntRect         frameRect;
    int                 totalEggs;
    int                 totalBombs;
};

class Object
{
public:
    Object();
    void Draw( sf::RenderTexture& window );
    void Update();
    void UpdateTurn( std::vector<Object>& allObjects, std::vector<sf::IntRect>& solidTiles );
    void UpdateVision( std::vector<Object>& allObjects, std::vector<sf::IntRect>& solidTiles, sf::Vector2i mapDimensions );
    void Move();
    void Rotate( RotationDirection rotDirection );
    void Debug();
    void DrawDebug( sf::RenderTexture& window );
    sf::IntRect GetCollisionRect();
    bool CanObjectMoveInDirection( std::vector<Object>& allObjects, std::vector<sf::IntRect>& solidTiles, Direction direction );
    std::vector<int> CanSeeObjectIndices( std::vector<Object>& allObjects, sf::Vector2i mapDimensions );
    void Toggle( std::vector<Object>& allObjects );
    void Collect( Object& otherObject );

    Animatable    prop_animatable;
    RawrCharacter prop_character;
    Movable       prop_movable;
    Player        prop_player;
    std::string   name;

    chalo::UILabel debug_text;

private:
    static std::string s_className;
};

#endif
