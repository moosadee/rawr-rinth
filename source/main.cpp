#include "Application/ChaloEngineProgram.hpp"
#include "chalo-engine/Utilities/Logger.hpp"

int main()
{
    Logger::Setup();
    Logger::StackPush( __func__ );

    {
        ChaloEngineProgram program;
        program.Run();
    }

    Logger::StackPop();
    Logger::Cleanup();
    return 0;
}
